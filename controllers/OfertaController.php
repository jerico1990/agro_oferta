<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;
use app\models\Oferta;
use app\models\OfeTcmOfertaCambio;
use yii\helpers\Json;
class OfertaController extends Controller
{
    /**
     * {@inheritdoc}
     */
   
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='intranet';
        return $this->render('index');
    }

    public function actionHistorial()
    {
        $this->layout='intranet';
        return $this->render('historial');
    }

    public function actionUpdate($oferta_id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Oferta::findOne($oferta_id);
        
        if($request->isAjax){
            if ($model->load($request->post())) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                /*$agro_cambio = new OfeTcmOfertaCambio;
                $agro_cambio->cod_cultivo   = $model->cod_cultivo;
                $agro_cambio->imp_precio    = $model->imp_precio;
                $agro_cambio->can_toneladas = $model->can_toneladas;
                $agro_cambio->fec_cosecha   = $model->fec_cosecha;
                $agro_cambio->cod_agri      = $model->cod_agri;
                $agro_cambio->ide_oferta    = $oferta_id;
                
                $agro_cambio->accion        = 'actualizar';
                $agro_cambio->fec_registro  = date('Y-m-d H:i:s');
                $agro_cambio->flg_estado    = 0 ;*/
                $model->fec_registro  = date('Y-m-d H:i:s');

                if($model->save()){
                    return ['success'=>true];
                }else{
                    return ['success'=>false];
                }
            } else {
                return $this->render('create',['model'=>$model]);
            }
        }
    }


    public function actionView($oferta_id){
        $this->layout = 'vacio';
        $request = Yii::$app->request;
        $model = Oferta::findOne($oferta_id);
        
        if($request->isAjax){
            return $this->render('view',['model'=>$model]);
        }
    }

    
    public function actionGetListaOfertas(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $features = [];

            $ofertas = (new \yii\db\Query())
                ->select(['ofe_tcm_oferta.*,to_char( ofe_tcm_oferta.fec_cosecha, \'DD-MM-YYYY\') as fecha,ofe_tcm_agricultor.txt_celular,ST_AsGeoJSON(cod_geom) as geojson,ofe_tcm_agricultor.txt_direccion,ofe_tcm_agricultor.txt_centro_poblado'])
                ->from('ofe_tcm_oferta')
                ->innerJoin('ofe_tcm_agricultor','ofe_tcm_agricultor.ide_agricultor=ofe_tcm_oferta.cod_agri');

            if(isset($_POST['region_id']) && $_POST['region_id']!=''){
                $ofertas = $ofertas->andWhere(['=', "cod_region",(int) $_POST['region_id']]);
            }

            if(isset($_POST['provincia_id']) && $_POST['provincia_id']!=''){
                $ofertas = $ofertas->andWhere(['=', "cod_prov",(int) substr($_POST['provincia_id'],3,2)]);
            }

            if(isset($_POST['distrito_id']) && $_POST['distrito_id']!=''){
                $ofertas = $ofertas->andWhere(['=', "cod_dist",(int) substr($_POST['distrito_id'],5,2)]);
            }
            
            if(isset($_POST['cod_agri']) && $_POST['cod_agri']!=''){
                $ofertas = $ofertas->andWhere(['=', "cod_agri",$_POST['cod_agri']]);
            }


            if(isset($_POST['cultivo_id']) && $_POST['cultivo_id']!=''){
                $ofertas = $ofertas->andWhere(['=', "cod_cultivo",$_POST['cultivo_id']]);
            }

            if(isset($_POST['fecha_inicio']) && $_POST['fecha_inicio']!='' && isset($_POST['fecha_fin']) && $_POST['fecha_fin']!=''){
                $ofertas = $ofertas->andWhere(['between', "fec_cosecha",$_POST['fecha_inicio'],$_POST['fecha_fin']]);
            }

            $ofertas = $ofertas->andWhere(['=', "flg_estado","1"]);

            $ofertas = $ofertas->andWhere(['>=', "fec_cosecha",date("Y-m-d")]);



            $ofertas = $ofertas->all();

            foreach($ofertas as $oferta){
                unset($oferta['cod_geom']);
                $geometry = $oferta['geojson'] = json_decode($oferta['geojson']);
                unset($oferta['geojson']);
                $feature = ['type'=>'Feature','geometry'=>$geometry,'properties'=>$oferta];
                array_push($features,$feature);
            }
            $featureCollection = ['type'=>'FeatureCollection','features'=>$features];

            return ['success'=>true,'ofertas'=>$ofertas,'featureCollection'=>$featureCollection];
        }
    }

    public function actionGetListaOfertasCambios(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){

            $ofertas = (new \yii\db\Query())
                ->select(['ofe_tcm_oferta_cambio.*,to_char( ofe_tcm_oferta_cambio.fec_cosecha, \'DD-MM-YYYY\') as fecha'])
                ->from('ofe_tcm_oferta_cambio');

            $ofertas = $ofertas->andWhere(['=', "flg_estado","0"]);
            $ofertas = $ofertas->andWhere(['>=', "fec_cosecha",date("Y-m-d")]);



            $ofertas = $ofertas->all();
            return ['success'=>true,'ofertas'=>$ofertas];
        }
    }

    public function actionGetListaOfertasHistorial(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){

            $ofertas = (new \yii\db\Query())
                ->select(['ofe_tcm_oferta_cambio.*,to_char( ofe_tcm_oferta_cambio.fec_cosecha, \'DD-MM-YYYY\') as fecha'])
                ->from('ofe_tcm_oferta_cambio');

            $ofertas = $ofertas->andWhere(['in', "flg_estado",['1','2']]);
            $ofertas = $ofertas->andWhere(['>=', "fec_cosecha",date("Y-m-d")]);



            $ofertas = $ofertas->all();
            return ['success'=>true,'ofertas'=>$ofertas];
        }
    }

    public function actionGetOferta(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){

            $oferta = (new \yii\db\Query())
                ->select('ofe_tcm_oferta.ide_oferta,ofe_tcm_oferta.cod_cultivo,ofe_tcm_oferta.imp_precio,ofe_tcm_oferta.can_toneladas,ofe_tcm_oferta.fec_cosecha')
                ->from('ofe_tcm_oferta');

            if(isset($_POST['oferta_id']) && $_POST['oferta_id']!=''){
                $oferta = $oferta->andWhere(['=', "ide_oferta",(int) $_POST['oferta_id']]);
            }

            $oferta = $oferta->one();
            return ['success'=>true,'oferta'=>$oferta];
        }
    }


    public function actionGetListaOfertasProductor(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){

            $ofertas = (new \yii\db\Query())
                ->select(['ofe_tcm_oferta.*,to_char( ofe_tcm_oferta.fec_cosecha, \'DD-MM-YYYY\') as fecha'])
                ->from('ofe_tcm_oferta');

            if(isset($_POST['dni']) && $_POST['dni']!=''){
                $ofertas = $ofertas->andWhere(['=', "txt_dni",$_POST['dni']]);
            }
            $ofertas = $ofertas->andWhere(['>=', "fec_cosecha",date("Y-m-d")]);
            $ofertas = $ofertas->andWhere(['=', "flg_estado","1"]);


            $ofertas = $ofertas->all();
            return ['success'=>true,'ofertas'=>$ofertas];
        }
    }

    public function actionDelete(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $oferta_id = $_POST['oferta_id'];
            $oferta = Oferta::findOne($oferta_id);
            $oferta->flg_estado = 0 ;
            if($oferta->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }

        }
    }


    public function actionOfertaPendiente(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $oferta_id = $_POST['oferta_id'];
            $accion = $_POST['accion'];
            $oferta = Oferta::findOne($oferta_id);

            if($accion=='eliminar'){
                $model = new OfeTcmOfertaCambio;
                $model->ide_oferta = $oferta_id;
                $model->accion = 'eliminar';
                $model->cod_agri = $oferta->cod_agri;
                $model->fec_registro = date('Y-m-d H:i:s');
                $model->flg_estado = 0 ;
                $model->cod_cultivo   = $oferta->cod_cultivo;
                $model->imp_precio    = $oferta->imp_precio;
                $model->can_toneladas = $oferta->can_toneladas;
                $model->fec_cosecha   = $oferta->fec_cosecha;
            }

            

            if($model->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }

        }
    }


    public function actionOfertaEliminarPendiente(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $ide_oferta_cambio = $_POST['ide_oferta_cambio'];
            $oferta_cambio = OfeTcmOfertaCambio::findOne($ide_oferta_cambio);
            $oferta_cambio->flg_estado = 2 ;
            //$oferta = Oferta::findOne($oferta_cambio->ide_oferta);
            //$oferta->flg_estado = 2 ;
            //$oferta->update();
            if($oferta_cambio->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }

        }
    }

    public function actionOfertaConfirmarPendiente(){
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $ide_oferta_cambio = $_POST['ide_oferta_cambio'];
            $oferta_cambio = OfeTcmOfertaCambio::findOne($ide_oferta_cambio);
            $oferta_cambio->flg_estado = 1 ;
            $oferta = Oferta::findOne($oferta_cambio->ide_oferta);

            if($oferta_cambio->accion=='eliminar'){
                $oferta->flg_estado = 2 ;
            }else if($oferta_cambio->accion=='actualizar'){
                $oferta->cod_cultivo   = $oferta_cambio->cod_cultivo;
                $oferta->imp_precio    = $oferta_cambio->imp_precio;
                $oferta->can_toneladas = $oferta_cambio->can_toneladas;
                $oferta->fec_cosecha   = $oferta_cambio->fec_cosecha;
            }
            $oferta->update();

            if($oferta_cambio->save()){
                return ['success'=>true];
            }else{
                return ['success'=>false];
            }

        }
    }

}
