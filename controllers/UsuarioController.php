<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;
use app\models\Oferta;
use yii\helpers\Json;
use app\models\Agricultor;
class UsuarioController extends Controller
{
    /**
     * {@inheritdoc}
     */
   
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='main';
        return $this->render('index');
    }

    public function actionActualizar(){
        $session = Yii::$app->session;
        if($session->get('dni')){
            $dni = $session->get('dni');
            $session->remove('dni');

            $agricultor = Agricultor::find()->where(['txt_dni'=>$dni])->one();
            if($agricultor){
                return $this->render('actualizar',['dni'=>$dni,'agricultor'=>$agricultor]);
            }
            $agricultor = new Agricultor;

            /*
            if(Yii::$app->request->post()){
                
                $agricultor = Agricultor::find()->where(['nro_documento'=>$dni])->one();
                $agricultor->telefono   = $_POST['telefono'];
                $agricultor->celular    = $_POST['celular'];
                $agricultor->email      = $_POST['correo_electronico'];
                $agricultor->update();

                return $this->render('../site/informar',['json'=>$json,'dni'=>$dni]);
            }*/
            return $this->render('actualizar',['dni'=>$dni,'agricultor'=>$agricultor]);
        }
        return $this->redirect(['site/index']);
        
    }

    public function actionLogin()
    {   
        $this->layout='vacio';
        $request = Yii::$app->request;
        if($request->isAjax){
            return $this->render('login');
        }
    }

    public function actionValidar(){
        
        if($_POST){
            $request = Yii::$app->request;
            $model = new LoginForm();
            //var_dump($request->post());
          
            if ($model->load($request->post()) && $model->login()) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ['success'=>True];
            }else{
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ['success'=>False];
            }
        }

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['visor/index']);
    }

}
