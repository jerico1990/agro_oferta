<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['options' => ['id' => 'formOfertaCambio']]); ?>

    <div class="modal-header">
        <h4 class="modal-title"><?= $model->titulo ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="cultivo">Seleccione el producto</label>
                    <select class="form-control oferta-cod_cultivo" id="oferta-cod_cultivo" name="Oferta[cod_cultivo]" data-live-search="true">
                        <option value>Seleccionar producto</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="precio">Ingrese el precio del producto por (S/. x Kg)</label>
                    <input type="number" class="form-control" id="oferta-imp_precio" name="Oferta[imp_precio]" maxlength = "7"  oninput="javascript: if (this.value.length > this.maxLength || (this.value.split('.')[1] && this.value.split('.')[1].length > 2)) {   this.value = parseFloat(this.value.slice(0, this.maxLength)).toFixed(2);   }" value="0.00">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="cantidad">Ingrese la cantidad total a ofertar (Kg)</label>
                    <input type="number"  class="form-control" id="oferta-can_toneladas" name="Oferta[can_toneladas]" maxlength = "7"  oninput="javascript: if (this.value.length > this.maxLength || (this.value.split('.')[1] && this.value.split('.')[1].length > 2)) {   this.value = parseFloat(this.value.slice(0, this.maxLength)).toFixed(2);   }" value="0.00">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="fecha_cosecha">Indique la fecha de la cosecha del producto ofertado</label>
                    <input type="date" class="form-control" id="oferta-fec_cosecha" min="<?php echo date('Y-m-d'); ?>"  name="Oferta[fec_cosecha]">
                </div>
            </div>
        </div>
        
    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary btn-grabar-oferta-pendiente">Guardar</button>
    </div>

<?php ActiveForm::end(); ?>

