<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;
use app\models\Oferta;
use yii\helpers\Json;
class VisorController extends Controller
{
    /**
     * {@inheritdoc}
     */
   
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='visor';
        return $this->render('index');
    }

    public function actionIndexVisor()
    {
        $this->layout='main3';
        return $this->render('index-visor');
    }

    public function actionIndex2()
    {
        $this->layout='main2';
        return $this->render('indexbk2');
    }

}
