<style>
.content-header{
    padding:4px .5rem;
}
</style>
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"> </h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="row">
        <div class="col-12 col-sm-4 col-md-2 col-lg-2">
            <div class="form-group">
                <div class="input-group botones"> 
                    <div class="input-group-prepend">  
                        <span class="input-group-text"> <i class="icon fa-apple" aria-hidden="true"></i> </span> 
                    </div> 
                    <select class="form-control cultivo_id" id="cultivo_id" name="cultivo_id"><option value>Seleccionar Cultivo</option></select> 
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-2 col-lg-2">
            <div class="form-group">
                <div class="input-group botones"> 
                    <div class="input-group-prepend">  
                        <span class="input-group-text"> <i class="icon fa-globe" aria-hidden="true"></i> </span> 
                    </div> 
                    <select class="form-control" data-plugin="selectpicker" id="region" name="region_id"><option value>Seleccionar Región</option></select> 
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-2 col-lg-2">
            <div class="form-group">
                <div class="input-group botones"> 
                    <div class="input-group-prepend">  
                        <span class="input-group-text"> <i class="icon fa-globe" aria-hidden="true"></i> </span> 
                    </div> 
                    <select class="form-control" data-plugin="selectpicker" id="provincia" name="provincia_id"><option value>Seleccionar Provincia</option></select>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-2 col-lg-2">
            <div class="form-group">
                <div class="input-group botones"> 
                    <div class="input-group-prepend">  
                        <span class="input-group-text"> <i class="icon fa-globe" aria-hidden="true"></i> </span> 
                    </div> 
                    <select class="form-control" data-plugin="selectpicker" id="distrito" name="distrito_id"><option value>Seleccionar Distrito</option></select>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-2 col-lg-2">
            <div class="form-group">
                <div class="input-group botones"> 
                    <div class="input-group-prepend">  
                        <span class="input-group-text"> <i class="icon fa-calendar" aria-hidden="true"></i> </span> 
                    </div> 
                    <input type="date" id="fecha_inicio" placeholder="Fecha inicio" class="form-control" data-plugin="datepicker">
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-2 col-lg-2">
            <div class="form-group">
                <div class="input-group botones"> 
                    <div class="input-group-prepend">  
                        <span class="input-group-text"> <i class="icon fa-calendar" aria-hidden="true"></i> </span> 
                    </div> 
                    <input type="date" id="fecha_fin" placeholder="Fecha fin" class="form-control" data-plugin="datepicker">
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-12 col-sm-4 col-md-4 col-lg-4">
            <div class="small-box bg-info">
                <div class="inner">
                    <h3><i class="icon fa-shopping-basket" aria-hidden="true"></i> <span class="cantidad_ofertas">0</span></h3>
                    <h2>Oferta Publicadas</h2>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="#" class="small-box-footer"> <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-4 col-lg-4">
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3><i class="icon fa-balance-scale" aria-hidden="true"></i> <span class="kilogramos_productos">0.00</span></h3>
                    <h2>Peso Total (Kg)</h2>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="#" class="small-box-footer"> <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-12 col-sm-4 col-md-4 col-lg-4">
            <div class="small-box bg-success">
                <div class="inner">
                    <h3><i class="icon fa-money" aria-hidden="true"></i> (S/.) <span class="montos_productos">0.00</span></h3>
                    <h2>Costo Total</h2>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="#" class="small-box-footer"> <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-12 col-sm-12 col-md-6 col-lg-6">
            <div id="cultivos_kilogramos" class="cultivos_kilogramos " style="font-size:10px !important"></div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-6">
            <div  id="map" class="map" style="height:400px;"></div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Lista de Ofertas publicadas</h3>
                    <!-- tools card -->
                    <div class="card-tools">
                        <!-- button with a dropdown -->
                        <button type="button" class="btn btn-sm" data-card-widget="collapse">
                            <i class="fas fa-plus"></i>
                        </button>
                    </div>
                    <!-- /. tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body" style="display:none">
                    <table class="ofertas table dt-responsive nowrap" >
                        <thead>
                            <th data-priority="1">Agricultor</th>
                            <th data-priority="2">Peso total (kg)</th>
                            <th data-priority="3">Costo total (S/.)</th>
                            <th data-priority="10000">Fecha de cosecha</th>
                            <th data-priority="10001">Cultivo</th>
                        </thead>
                        <tbody>
                        
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.content -->

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";

var map = L.map('map', {
    zoomControl: false,
    scrollWheelZoom: true,
    fullscreenControl: false,
    dragging: true,
    layers:[],
    minZoom: 3,
    maxNativeZoom: 18,
    maxZoom:18,
    zoom:5,
    center: [-9.1899672, -75.015152],
});

/* Mapa Base */

L.esri.basemapLayer('Imagery').addTo(map);

/* Controladores de ubigeo */
var regiones = new L.esri.featureLayer({
	url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/0/',
	minZoom: 5,
	style : function (feature) {
		return { color: 'white', weight: 2 };
	},
});
regiones.addTo(map);

var provincias;
var distritos;
var region;
var provincia;

/* Cargando valores a selector*/
var boundsRegiones = L.latLngBounds([]);
regiones.on("load", function(evt) {
    var regiones_options = "<option value>Seleccionar Región</option>";
    regiones.eachFeature(function (layer) {
        regiones_options = regiones_options + "<option value='" + layer.feature.properties.iddpto + "'>" + layer.feature.properties.nombdep + "</option>";
    })
    $('#region').html(regiones_options);

    boundsRegiones = L.latLngBounds([]);
    // loop through the features returned by the server
    regiones.eachFeature(function (layer) {
        // get the bounds of an individual feature
        var layerBounds = layer.getBounds();
        // extend the bounds of the collection to fit the bounds of the new feature
        boundsRegiones.extend(layerBounds);
    });

    //$('.ubigeo').html('Nivel Nacional');

    // once we've looped through all the features, zoom the map to the extent of the collection
    //map.fitBounds(boundsRegiones);
});

var bandera=0;
var iddpto;
var boundsRegion = L.latLngBounds([]);
$('body').on('change', '#region', function (e) {
    iddpto = $(this).val();
    if(bandera==0){
        bandera=1;
    }
    //
    if(GrupoOfertasGeneral != undefined){
        GrupoOfertasGeneral.clearLayers();
    }
    Ofertas();
    ListaOfertas();
    //map.setView([-9.1899672, -75.015152],6);
    map.fitBounds(boundsRegiones);
    map.removeLayer(regiones);
    if (region != undefined) {
        map.removeLayer(region);
    }
    
    if (provincias != undefined) {
        map.removeLayer(provincias);
        if(distritos){
            map.removeLayer(distritos);
        }
        

        var distritos_options = "<option value>Seleccionar Región</option>";
        $('#distrito').html(distritos_options);
        iddist='';
    }
    /* quitando tabla de agricultores */
    //map.removeControl(lista_agricultores)

    region = new L.esri.featureLayer({
        url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/0/',
        where: "iddpto='" + iddpto + "'",
        style : function (feature) {
            return { color: 'white', weight: 2 };
        },
    }).addTo(map);
    
    region.once('load', function (evt) {
        // create a new empty Leaflet bounds object
        boundsRegion = L.latLngBounds([]);
        // loop through the features returned by the server
        region.eachFeature(function (layer) {
            // get the bounds of an individual feature
            var layerBounds = layer.getBounds();
            // extend the bounds of the collection to fit the bounds of the new feature
            boundsRegion.extend(layerBounds);
        });

        // once we've looped through all the features, zoom the map to the extent of the collection
        map.fitBounds(boundsRegion);
    });

    provincias = new L.esri.featureLayer({
        url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/1/',
        where: "iddpto='" + iddpto + "'",
        style : function (feature) {
            return { color: 'white', weight: 2 };
        },
    }).addTo(map);

    provincias.on("load", function(evt) {
        var provincias_options = "<option value>Seleccionar Provincia</option>";
        provincias.eachFeature(function (layer) {
            provincias_options = provincias_options + "<option value='" + layer.feature.properties.idprov + "'>" + layer.feature.properties.nombprov + "</option>";
        })
        $('#provincia').html(provincias_options);
    });

    //$('.ubigeo').html('Nivel Regional');

});


var idprov;
var boundsProvincia = L.latLngBounds([]);

$('body').on('change', '#provincia', function (e) {
    idprov = $(this).val();
    
    if(GrupoOfertasGeneral != undefined){
        GrupoOfertasGeneral.clearLayers();
    }
    Ofertas();
    ListaOfertas();

    map.fitBounds(boundsRegion);
    map.removeLayer(region);
    map.removeLayer(provincias);
    if (distritos != undefined) {
        map.removeLayer(distritos);
        iddist='';
    }

    /* quitando tabla de agricultores */
    //map.removeControl(lista_agricultores)
        

    provincias = new L.esri.featureLayer({
        url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/1/',
        where: "idprov='" + idprov + "'",
        style : function (feature) {
            return { color: 'white', weight: 2 };
        },
    }).addTo(map);
    
    provincias.once('load', function (evt) {
        // create a new empty Leaflet bounds object
        boundsProvincia = L.latLngBounds([]);
        // loop through the features returned by the server
        provincias.eachFeature(function (layer) {
            // get the bounds of an individual feature
            var layerBounds = layer.getBounds();
            // extend the bounds of the collection to fit the bounds of the new feature
            boundsProvincia.extend(layerBounds);
        });

        // once we've looped through all the features, zoom the map to the extent of the collection
        map.fitBounds(boundsProvincia);
    });

    distritos = new L.esri.featureLayer({
        url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/2/',
        where: "idprov='" + idprov + "'",
        style : function (feature) {
            return { color: 'white', weight: 2 };
        },
    }).addTo(map);

    distritos.on("load", function(evt) {
        var distritos_options = "<option value>Seleccionar Distritos</option>";
        distritos.eachFeature(function (layer) {
            distritos_options = distritos_options + "<option value='" + layer.feature.properties.iddist + "'>" + layer.feature.properties.nombdist + "</option>";
        })
        $('#distrito').html(distritos_options);
    });
    //$('.ubigeo').html('Nivel Distrital');
});

var iddist;
$('body').on('change', '#distrito', function (e) {
    iddist = $(this).val();
    if(GrupoOfertasGeneral != undefined){
        GrupoOfertasGeneral.clearLayers();
    }
    /* quitando tabla de agricultores */
    //map.removeControl(lista_agricultores);
    /* agregando tabla de agricultores */
	//lista_agricultores.addTo(map);
    
    Ofertas();
    ListaOfertas();
    map.fitBounds(boundsProvincia);

    map.removeLayer(provincias);
    map.removeLayer(distritos);
	
    

    distritos = new L.esri.featureLayer({
        url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/2/',
        where: "iddist='" + iddist + "'",
        style : function (feature) {
            return { color: 'white', weight: 2 };
        },
    }).addTo(map);
    
    distritos.once('load', function (evt) {
        // create a new empty Leaflet bounds object
        var bounds = L.latLngBounds([]);
        // loop through the features returned by the server
        distritos.eachFeature(function (layer) {
            // get the bounds of an individual feature
            var layerBounds = layer.getBounds();
            // extend the bounds of the collection to fit the bounds of the new feature
            bounds.extend(layerBounds);
        });

        // once we've looped through all the features, zoom the map to the extent of the collection
        
        map.fitBounds(bounds);
        
    });

    //$('.ubigeo').html('Nivel Provincial');
});

var cultivo_id;
$('body').on('change', '#cultivo_id', function (e) {
    cultivo_id = $(this).val();
    if(bandera==0){
        bandera=2;
    }
    Ofertas();
    ListaOfertas();
});

var fecha_inicio;
var fecha_fin;
$('body').on('change', '#fecha_inicio', function (e) {
    fecha_inicio = $(this).val();
    if(fecha_inicio && fecha_fin){
        Ofertas();
        ListaOfertas();
    }
    
});

$('body').on('change', '#fecha_fin', function (e) {
    fecha_fin = $(this).val();
    if(fecha_inicio && fecha_fin){
        Ofertas();
        ListaOfertas();
    }
});

var GeoJsonOfertas = [];
var GrupoOfertasGeneral = L.layerGroup();
var icon_oferta = L.divIcon({className: 'fas fa-map-marker text-success fa-2x'});
async function Ofertas(){
    GrupoOfertasGeneral.clearLayers();
    //"http://190.12.92.169:8081/geoserver/centro_poblado/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=centro_poblado:centro_poblado_padron_agricultores&CQL_FILTER=ubigeo='" + iddist +"'&outputFormat=application/json",
    if(iddpto || cultivo_id){

    
        var candera_cqfilter= "";

        if(bandera==1){
            candera_cqfilter = candera_cqfilter + "cod_region='" + parseInt(iddpto) +"'";
        }
        else if(bandera==2){
            candera_cqfilter = candera_cqfilter + "cod_cultivo='" + cultivo_id +"'";
        }

        if(iddpto && bandera==2){
            candera_cqfilter = candera_cqfilter + " + AND + cod_region='" + parseInt(iddpto) +"'";
        }

        if(cultivo_id && bandera==1){
            candera_cqfilter = candera_cqfilter + " + AND + cod_cultivo='" + cultivo_id +"'";
        }


        if(idprov){
            candera_cqfilter = candera_cqfilter + " + AND + cod_prov='" + parseInt(idprov.substring(2, 4)) +"'";
        }

        if(iddist){
            candera_cqfilter = candera_cqfilter + " + AND + cod_dist='" + parseInt(iddist.substring(4, 6)) +"'";
        }

        

        if(fecha_inicio && fecha_fin){
            candera_cqfilter = candera_cqfilter + " + AND + fec_cosecha + BETWEEN + '" + fecha_inicio +"' AND '" + fecha_fin +"'";
        }

        candera_cqfilter = candera_cqfilter + " + AND + fec_cosecha + >= + '" + moment().format("YYYY-MM-DD") +"'";

        /*
        urls =  [
            "http://sisagro.minagri.gob.pe:8080/geoserver/AgroOferta/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=AgroOferta:ofe_tcm_oferta&CQL_FILTER=" + candera_cqfilter + "&outputFormat=application/json",
        ];*/
        
        /*
        
        urls =  [
            "http://200.48.31.150:8080/geoserver/AgroOferta/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=AgroOferta:ofe_tcm_oferta&CQL_FILTER=" + candera_cqfilter + "&outputFormat=application/json",
        ];
        */

        urls =  [
            "http://190.12.92.169:8081/geoserver/oferta_cultivo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=oferta_cultivo:ofe_tcm_oferta&CQL_FILTER=" + candera_cqfilter + "&outputFormat=application/json",
        ];
        

        let results =await Promise.all(urls.map((url) => fetch(url).then((r) => r.json())));

        let lista_ofertas = "";
        jQuery.each(results, function(i, result) {
            GeoJsonOfertas[i] = L.geoJson(result, {
                onEachFeature: function (feature, layer) {
                    layer.bindPopup('<p> Nombres y apellidos : <strong>' + feature.properties.txt_nombres_apellidos + '</strong> <br> Cultivo : <strong>' + lista_cultivos[feature.properties.cod_cultivo]  + '</strong> <br> Cantidad (Kg) : <strong>' + feature.properties.can_toneladas + '</strong> <br> Precio por Kilos : <strong> S/. ' + number_format(feature.properties.imp_precio,2) + '</strong> <br>  Fecha de cosecha : <strong>' + feature.properties.fec_cosecha + '</strong> <br> </p>');
                    //DNI : <strong>' + feature.properties.txt_dni + '</strong> <br>
                    /*
                    layer.bindPopup(Object.keys(feature.properties).map(function(k) {
                        return k + ": " + feature.properties[k];
                    }).join("<br />") , {
                        maxHeight: 200
                    });*/
                },
                pointToLayer: function (feature, latlng) {
                    if(feature.properties.can_toneladas>=0 && feature.properties.can_toneladas<5){
                        icon_oferta = L.divIcon({className: 'fas fa-map-marker text-info fa-2x'});
                    } else if(feature.properties.can_toneladas>=5 && feature.properties.can_toneladas<10){
                        icon_oferta = L.divIcon({className: 'fas fa-map-marker text-warning fa-2x'});
                    } else if(feature.properties.can_toneladas>=10){
                        icon_oferta = L.divIcon({className: 'fas fa-map-marker text-danger fa-2x'});
                    }

                    return L.marker(latlng, {
                        icon: icon_oferta,
                    });
                }
            });
            GrupoOfertasGeneral.addLayer(GeoJsonOfertas[i]);
        });
        GrupoOfertasGeneral.addTo(map);
    }
}


var cantidad_ofertas=0;
var kilogramos_productos=0;
var montos_productos=0;

var lista_agricultores = L.control({position: 'bottomright'});
lista_agricultores.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'info legend col-md-12');
    div.innerHTML = ``;
    div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    return div;
};


Cultivos();
var lista_cultivos =new Array();
async function Cultivos(){

    urls =  [
        "http://fspreset.minagri.gob.pe:3000/cat_cultivos2",
    ];
    let results =await Promise.all(urls.map((url) => fetch(url,{method:'POST'}).then((r) => r.json())));
    var cultivo_options = "<option value>Seleccionar Cultivo</option>";
    jQuery.each(results, function(i, result) {
        
        $.map( result, function( val, i ) {
            let cod = parseInt(val.COD);
            lista_cultivos[cod]= val.NOMCUL;
            cultivo_options = cultivo_options + "<option value='" + val.COD + "'>" + val.NOMCUL + "</option>";
        });
    });
    $('.cultivo_id').html(cultivo_options);
    ListaOfertas();
    //$('.cultivo_id').selectpicker();
}



var icon_mayorista = L.icon({
    iconUrl: '<?= \Yii::$app->request->BaseUrl ?>/img/mercado_mayorista.png',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11],
});

var icon_minorista = L.icon({
    iconUrl: '<?= \Yii::$app->request->BaseUrl ?>/img/mercado_minorista.png',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11],
});

var icon_supermercados = L.icon({
    iconUrl: '<?= \Yii::$app->request->BaseUrl ?>/img/super_mercados.png',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11],
});

var icon_mercados_itinerantes = L.icon({
    iconUrl: '<?= \Yii::$app->request->BaseUrl ?>/img/mercado_itinerante.svg',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11],
});

var baseMaps = {};
var capas = {};
var layerControl = L.control.layers(baseMaps,capas, {position: 'topleft'}).addTo(map);
var mayorista,minorista,supermercados,mercados_itinerantes;

Capas();
async function Capas(){
    /*mayoristas */
    mayoristas = new L.esri.featureLayer({
        url: 'https://swp64arcg.minagri.gob.pe/server/rest/services/AmbitosAdministrativosAgrarios/MercadosAbastecimiento/MapServer/1/',
        //where: "idprov='" + idprov + "'",
        style : function (feature) {
            //return { color: 'white', weight: 2 };
        },
        pointToLayer: function (geojson, latlng) {
            return L.marker(latlng, {
                icon: icon_mayorista
            });
        }
    });

    mayoristas.bindPopup(function (layer) {
        return L.Util.template('<p>NOMBDEP: <strong>{NOMBDEP}</strong> <br> NOMBPROV: <strong>{NOMBPROV}</strong> <br> NOMBDIST: <strong>{NOMBDIST} </strong> <br> LOCAL: <strong>{LOCAL} </strong><br> NOMBRE: <strong>{NOMBRE} </strong> <br> DIRECCION: <strong>{DIRECCION} </strong>  </p>', layer.feature.properties);
    });

    layerControl.addOverlay(mayoristas,"Mayoristas");
    
    /*minoristas */
    minoristas = new L.esri.featureLayer({
        url: 'https://swp64arcg.minagri.gob.pe/server/rest/services/AmbitosAdministrativosAgrarios/MercadosAbastecimiento/MapServer/2/',
        //where: "idprov='" + idprov + "'",
        style : function (feature) {
            //return { color: 'white', weight: 2 };
        },
        pointToLayer: function (geojson, latlng) {
            return L.marker(latlng, {
                icon: icon_minorista
            });
        }
    });
    minoristas.bindPopup(function (layer) {
        return L.Util.template('<p>NOMBDEP: <strong>{NOMBDEP}</strong> <br> NOMBPROV: <strong>{NOMBPROV}</strong> <br> NOMBDIST: <strong>{NOMBDIST} </strong> <br> LOCAL: <strong>{LOCAL} </strong> <br> NOMBRE: <strong>{NOMBRE} </strong> <br> DIRECCION: <strong>{DIRECCION} </strong>  </p>', layer.feature.properties);
    });

    layerControl.addOverlay(minoristas,"Minorista");

    /*supermercados */
    supermercados = new L.esri.featureLayer({
        url: 'https://swp64arcg.minagri.gob.pe/server/rest/services/AmbitosAdministrativosAgrarios/MercadosAbastecimiento/MapServer/3/',
        //where: "idprov='" + idprov + "'",
        style : function (feature) {
            //return { color: 'white', weight: 2 };
        },
        pointToLayer: function (geojson, latlng) {
            return L.marker(latlng, {
                icon: icon_supermercados
            });
        }
    });
    supermercados.bindPopup(function (layer) {
        return L.Util.template('<p>NOMBDEP: <strong>{NOMBDEP}</strong> <br> NOMBPROV: <strong>{NOMBPROV}</strong> <br> NOMBDIST: <strong>{NOMBDIST} </strong> <br> LOCAL: <strong>{LOCAL}</strong> <br> NOMBRE: <strong>{NOMBRE} </strong> <br> DIRECCION: <strong>{DIRECCION} </strong>  </p>', layer.feature.properties);
    });

    layerControl.addOverlay(supermercados,"Supermercados");

    /*mercados_itinerantes */
    mercados_itinerantes = new L.esri.featureLayer({
        url: 'https://swp64arcg.minagri.gob.pe/server/rest/services/AmbitosAdministrativosAgrarios/MercadosAbastecimiento/MapServer/4/',
        //where: "idprov='" + idprov + "'",
        style : function (feature) {
            //return { color: 'white', weight: 2 };
        },
        pointToLayer: function (geojson, latlng) {
            return L.marker(latlng, {
                icon: icon_mercados_itinerantes
            });
        }
    });

    mercados_itinerantes.bindPopup(function (layer) {
        return L.Util.template('<p>NOMBDEP: <strong>{NOMBDEP}</strong> <br> NOMBPROV: <strong>{NOMBPROV}</strong> <br> NOMBDIST: <strong>{NOMBDIST} </strong> <br> CAPITAL: <strong>{CAPITAL}</strong> <br> LUGAR: <strong>{LUGAR} </strong>  </p>', layer.feature.properties);
    });

    layerControl.addOverlay(mercados_itinerantes,"Mercados Itinerantes");

    /*
    var capa_options = "";
    capa_options = capa_options + "<option value='0'>Mayorista</option>";
    capa_options = capa_options + "<option value='1'>Minorista</option>";
    capa_options = capa_options + "<option value='2'>Supermercados</option>";
    capa_options = capa_options + "<option value='3'>Mercados Itinerantes</option>";
    capa_options = capa_options + "<option value='4'>Área calculada por zona de predominancia distrital</option>";*/

    //$('.capas').html(capa_options);
}

/*Control*/
map.on('overlayadd', function(eventlayer){
    console.log(eventlayer);
    if(eventlayer.name=='Mayoristas'){
        $('.mayorista').html('<p class="card-text"><img style="vertical-align:initial" width="15px" src="<?= \Yii::$app->request->BaseUrl ?>/img/mercado_mayorista.png"></img> M. mayorista</p>');
    }else if(eventlayer.name=='Minorista'){
        $('.minorista').html('<p class="card-text"><img style="vertical-align:initial" width="15px" src="<?= \Yii::$app->request->BaseUrl ?>/img/mercado_minorista.png"></img> M. minorista</p>');
    }else if(eventlayer.name=='Supermercados'){
        $('.supermercados').html('<p class="card-text"><img style="vertical-align:initial" width="15px" src="<?= \Yii::$app->request->BaseUrl ?>/img/super_mercados.png"></img> Super mercados</p>');
    }else if(eventlayer.name=='Mercados Itinerantes'){
        $('.mercados_itinerantes').html('<p class="card-text"><img style="vertical-align:initial" width="15px" src="<?= \Yii::$app->request->BaseUrl ?>/img/mercado_itinerante.svg"></img> M. Itinerantes</p>');
    }
});

map.on('overlayremove', function(eventlayer){
    if(eventlayer.name=='Mayoristas'){
        $('.mayorista').html('');
    }else if(eventlayer.name=='Minorista'){
        $('.minorista').html('');
    }else if(eventlayer.name=='Supermercados'){
        $('.supermercados').html('');
    }else if(eventlayer.name=='Mercados Itinerantes'){
        $('.mercados_itinerantes').html('');
    }
});


async function ListaOfertas(){
    data_cultivos_grafico = new Array();

    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/oferta/get-lista-ofertas',
        method: 'POST',
        data:{_csrf:csrf,region_id:iddpto,provincia_id:idprov,distrito_id:iddist,cultivo_id:cultivo_id,fecha_inicio:fecha_inicio,fecha_fin:fecha_fin},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                cantidad_ofertas = 0 ;
                kilogramos_productos = 0 ;
                montos_productos = 0 ;
                var ofertas ="";
                $('.ofertas').DataTable().destroy();
                $.each(results.ofertas, function( index, value ) {
                    var cultivo = "";
                    if(lista_cultivos[value.cod_cultivo]){
                        cultivo = lista_cultivos[value.cod_cultivo]
                    }
                    

                    var fecha_hoy = moment(new Date());
                    var fecha_cosecha = moment(value.fec_cosecha);
                    var dias = fecha_cosecha.diff(fecha_hoy, 'days');
                    var alerta="";
                    if(dias<=10){
                        alerta='<span class="badge badge-pill badge-danger">' + dias + ' d</span>';
                    }else if(dias<=20){
                        alerta='<span class="badge badge-pill badge-warning">' + dias + ' d</span>';
                    }else if(dias<=31){
                        alerta='<span class="badge badge-pill badge-success">' + dias + ' d</span>';
                    }

                   

                    if (cultivo in data_cultivos_grafico){
                        data_cultivos_grafico[cultivo] = parseFloat(data_cultivos_grafico[cultivo]) + parseFloat(value.can_toneladas);
                    }else{
                        data_cultivos_grafico[cultivo] = parseFloat(value.can_toneladas);
                    }

                    var tonelada = ((value.can_toneladas)?value.can_toneladas:0);
                    var precio = ((value.imp_precio)?value.imp_precio:0);
                    ofertas = ofertas + "<tr>";
                        ofertas = ofertas + "<td>" + ((value.txt_nombres_apellidos)?value.txt_nombres_apellidos:'') + "</td>";
                        ofertas = ofertas + "<td>" + ((value.can_toneladas)?number_format(value.can_toneladas,2):'') + "</td>";
                        ofertas = ofertas + "<td>" + ((value.imp_precio)?number_format(value.imp_precio,2):'') + "</td>";
                        ofertas = ofertas + "<td>" + ((value.fecha)?value.fecha + ' ' + alerta:'') + "</td>";
                        ofertas = ofertas + "<td>" + ((cultivo)?cultivo:'') + "</td>";
                    ofertas = ofertas + "</tr>";
                    kilogramos_productos = kilogramos_productos + parseFloat(tonelada);
                    montos_productos = montos_productos + parseFloat(precio)*parseFloat(tonelada);

                    cantidad_ofertas++;
                });

                graficoBarras(Object.entries(data_cultivos_grafico));
                console.log(Object.entries(data_cultivos_grafico));
                $('.ofertas tbody').html(ofertas);
                $('.cantidad_ofertas').html(number_format(cantidad_ofertas,0));
                $('.kilogramos_productos').html(number_format(kilogramos_productos,2));
                $('.montos_productos').html(number_format(montos_productos,2));
                
                $('.ofertas').DataTable({
					"responsive": true,
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "pageLength" : 3,
                    "language": {
                        "sProcessing":    "Procesando...",
                        "sLengthMenu":    "Mostrar _MENU_ registros",
                        "sZeroRecords":   "No se encontraron resultados",
                        "sEmptyTable":    "Ningun dato disponible en esta lista",
                        "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":   "",
                        "sSearch":        "Buscar:",
                        "sUrl":           "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":    "Último",
                            "sNext":    "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                });
               
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
}
var chart;
function graficoBarras(data){
    chart = Highcharts.chart('cultivos_kilogramos', {
        chart: {
            style: {
                fontSize: '0.9em'
            },
            type: 'column',
            height:  '48%'
        },
        title: {
            text: 'Cultivo por Kilogramos',
            style: {
                fontSize: '2em'
            }
        },
        subtitle: {
            text: 'Fuente: MINAGRI'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '0.9em',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            labels: {
                style: {
                    fontSize: '0.9em'
                }
            },
            min: 0,
            title: {
                text: 'Kilogramos'
            }
        },
        legend: {
            itemStyle: {
                fontSize: '0.9em'
            },
            enabled: false
        },
        tooltip: {
            pointFormat: 'Kilogramos: <b>{point.y:.1f}</b>',
            style: {
                fontSize: '0.9em'
            }
        },
        series: [{
            name: 'Population',
            data: data,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}


function number_format(amount, decimals) {
    amount += '';
    amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
    decimals = decimals || 0;
    if (isNaN(amount) || amount === 0)
            return parseFloat(0).toFixed(decimals);
    amount = '' + amount.toFixed(decimals);
    var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
    while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
    return amount_parts.join('.');
}


/*leyenda de kilogramos*/

var legend_kilogramos = L.control({position: 'bottomleft'});
legend_kilogramos.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'col-md-12');
    var html = "";
    html += `<div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <dl>
                        <dt><h5>Leyenda</h5></dt>
                        <dd>
                            <p class="card-text"><span class='fas fa-map-marker text-info fa-1x'></span> 1 a 5 kilogramos</p>
                            <p class="card-text"><span class='fas fa-map-marker text-warning fa-1x'></span> 5 a 10 kilogramos</p>
                            <p class="card-text"><span class='fas fa-map-marker text-danger fa-1x'></span> 10 a mas kilogramos</p>
                            <p class="card-text"><div class="mayorista"></div></p>
                            <p class="card-text"><div class="minorista"></div></p>
                            <p class="card-text"><div class="supermercados"></div></p>
                            <p class="card-text"><div class="mercados_itinerantes"></div></p>
                        </dd>
                    </dl>
                </div>
                <!-- /.card-body -->
            </div>
            `;
    div.innerHTML = html;

    return div;
};
legend_kilogramos.addTo(map);

function pad_with_zeroes(number, length) {

    var my_string = '' + number;
    while (my_string.length < length) {
        my_string = '0' + my_string;
    }

    return my_string;

}


</script>