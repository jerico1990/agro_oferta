<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>MIDAGRI | Agro Oferta</title>
        <!-- App favicon -->
        <link rel="shortcut icon" href="<?= \Yii::$app->request->BaseUrl ?>/img/agroOferta.png">
        
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/jquery/jquery.js"></script>
        <link href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/fonts/font-awesome/font-awesome.css" rel="stylesheet">
        
        


        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.25.1/moment.min.js"  crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet@1.5.1/dist/leaflet.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.fullscreen/1.4.2/Control.FullScreen.min.css" />
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/leaflet@1.5.1/dist/leaflet.js" crossorigin=""></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.fullscreen/1.4.2/Control.FullScreen.min.js"></script>
        <script src="https://unpkg.com/esri-leaflet@2.3.3/dist/esri-leaflet.js" integrity="sha512-cMQ5e58BDuu1pr9BQ/eGRn6HaR6Olh0ofcHFWe5XesdCITVuSBiBZZbhCijBe5ya238f/zMMRYIMIIg1jxv4sQ==" crossorigin=""></script>
        <script src="https://consbio.github.io/Leaflet.Basemaps/L.Control.Basemaps.js" crossorigin="anonymous"></script>

        <script src="<?= \Yii::$app->request->BaseUrl ?>/leaflet-Basemaps/L.Control.Basemaps.js"></script>
        <link rel="<?= \Yii::$app->request->BaseUrl ?>/leaflet-Basemaps/L.Control.Basemaps.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.js"></script>
        <script src="https://bowercdn.net/c/es6-promise-3.2.2/es6-promise.min.js"  crossorigin="anonymous"></script>
        <script src="https://bowercdn.net/c/fetch-1.0.0/fetch.js"  crossorigin="anonymous"></script>
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/leaflet-control-topcenter-master/dist/leaflet-control-topcenter.css" />
        <script src="<?= \Yii::$app->request->BaseUrl ?>/leaflet-control-topcenter-master/dist/leaflet-control-topcenter.js" crossorigin="anonymous"></script>

        <!-- DataTables -->
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-3.0.5/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-3.0.5/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">


        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-3.0.5/plugins/fontawesome-free/css/all.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-3.0.5/dist/css/adminlte.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src="https://code.highcharts.com/modules/accessibility.js"></script>
        <!-- jQuery -->
        <script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-3.0.5/plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-3.0.5/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- DataTables -->
        <script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-3.0.5/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-3.0.5/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-3.0.5/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-3.0.5/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

        <!-- AdminLTE App -->
        <script src="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-3.0.5/dist/js/adminlte.min.js"></script>
        <style>
        .CheckConn{
            display:none
        }

        .icono_check{
            content: url("<?= \Yii::$app->request->BaseUrl ?>/img/rss-solid.svg");
            width: 2%;
        }

        .icono_check_desconectado {
            color:red;
        }
        </style>
    </head>
    <body class="hold-transition layout-top-nav ">
        

        <div class="wrapper">

            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
                <a href="https://www.gob.pe/minagri" class="navbar-brand">
                    <img src="<?= \Yii::$app->request->BaseUrl ?>/img/LogoMinagri.jpg" alt="Minagri" class="brand-image"
                        style="opacity: .8">
                    <span class="brand-text font-weight-light">SISAGRO - Visor de Ofertas publicadas</span> - 
                    <i id="icono_check" class="fas fa-rss icono_check_conectado"></i> <span class="CheckConn icono_check_desconectado"> No hay conectividad con el sistema </span>
                </a>
            </nav>
            <!-- /.navbar -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?= $content ?>
            </div>
            <!-- /.content-wrapper -->


            <!-- Main Footer -->
            <footer class="main-footer">
                <!-- Default to the left -->
                <strong>Copyright &copy; 2020 <a href="https://www.gob.pe/minagri">MINAGRI</a>.</strong> Todos los derechos reservados.
            </footer>
        </div>
    <!-- ./wrapper -->
    <script>
    var icono_check = $('#icono_check');
    var msg_check = $('#msg_check');
    function checkconnection() {
        var status = navigator.onLine;
        
        if (status) {
            $('.CheckConn').hide();
            /*
            $('.CheckConn').html(' Su ordenador cuenta con internet actualmente');

            msg_check.removeClass('icono_check_desconectado');
            msg_check.addClass('icono_check_conectado');

            icono_check.removeClass('icono_check_desconectado');
            icono_check.addClass('icono_check_conectado');*/
        }else{
            $('.CheckConn').show();
            
            /*.html('<i class="fas fa-rss"></i> Su ordenador no cuenta con internet actualmente');

            msg_check.removeClass('icono_check_conectado');
            msg_check.addClass('icono_check_desconectado');

            icono_check.removeClass('icono_check_conectado');
            icono_check.addClass('icono_check_desconectado');*/

        
        }
    }

    setInterval(function(){ 
        checkconnection();
    }, 1000);

    </script>
    <!-- REQUIRED SCRIPTS -->
    </body>
    <!-- Modal -->
    

</html>
