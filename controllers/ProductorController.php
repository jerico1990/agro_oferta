<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;
use app\models\Oferta;
use app\models\Agricultor;
use yii\helpers\Json;
class ProductorController extends Controller
{
    /**
     * {@inheritdoc}
     */
   
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='intranet';
        return $this->render('index');
    }

    
    public function actionGetListaProductores(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){

            $productores = (new \yii\db\Query())
                ->select('*')
                ->from('ofe_tcm_agricultor');
            

            $productores = $productores->all();
            return ['success'=>true,'productores'=>$productores];
        }
    }

}
