<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
///AppAsset::register($this);
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>MIDAGRI | Agro Oferta</title>
    <link rel="shortcut icon" href="<?= \Yii::$app->request->BaseUrl ?>/img/agroOferta.png">
    
    <script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet@1.5.1/dist/leaflet.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.fullscreen/1.4.2/Control.FullScreen.min.css" />
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/leaflet@1.5.1/dist/leaflet.js" crossorigin=""></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.fullscreen/1.4.2/Control.FullScreen.min.js"></script>
    <script src="https://unpkg.com/esri-leaflet@2.3.3/dist/esri-leaflet.js" integrity="sha512-cMQ5e58BDuu1pr9BQ/eGRn6HaR6Olh0ofcHFWe5XesdCITVuSBiBZZbhCijBe5ya238f/zMMRYIMIIg1jxv4sQ==" crossorigin=""></script>
    <script src="https://consbio.github.io/Leaflet.Basemaps/L.Control.Basemaps.js" crossorigin="anonymous"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/leaflet-Basemaps/L.Control.Basemaps.js"></script>
    <link rel="<?= \Yii::$app->request->BaseUrl ?>/leaflet-Basemaps/L.Control.Basemaps.css">

    <script src="<?= \Yii::$app->request->BaseUrl ?>/leaflet-turf/turf.min.js"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.js"></script>
    <script src="https://bowercdn.net/c/es6-promise-3.2.2/es6-promise.min.js"  crossorigin="anonymous"></script>
    <script src="https://bowercdn.net/c/fetch-1.0.0/fetch.js"  crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css" integrity="sha384-Bfad6CLCknfcloXFOyFnlgtENryhrpZCe29RTifKEixXQZ38WheV+i/6YWSzkz3V" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<!-- 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"/> -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css"/>
    

    
    
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"  crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"  crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"  crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"  crossorigin="anonymous"></script>
    <link href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/fonts/font-awesome/font-awesome.css" rel="stylesheet">
    <link href="http://fspreset.minagri.gob.pe:3000/Content/capcha/css/captcha.css" rel="stylesheet">
    <script src="http://fspreset.minagri.gob.pe:3000/Content/capcha/js/captcha.js"  crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js"  crossorigin="anonymous"></script>
    
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"/>
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-3.0.5/plugins/fontawesome-free/css/all.min.css">
    
    <style>
    .CheckConn{
        display:none
    }

    .icono_check{
        content: url("<?= \Yii::$app->request->BaseUrl ?>/img/rss-solid.svg");
        width: 2%;
    }

    .icono_check_desconectado {
        color:red;
    }
    </style>
</head>
<body style="background-color:#f8f9fa">


<div class="wrap">
   
    <!-- Just an image -->
    <nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="#">
        <img src="<?= \Yii::$app->request->BaseUrl ?>/img/LogoMinagri.jpg" width="250" height="50" alt="">  <span class="CheckConn icono_check_desconectado"> No hay conectividad con el sistema </span>
    </a>
    </nav>
    <div class="container" style="max-width:1300px">
        <?= $content ?>
    </div>
</div>

<script>
var icono_check = $('#icono_check');
var msg_check = $('#msg_check');
function checkconnection() {
    var status = navigator.onLine;
    
    if (status) {
        $('.CheckConn').hide();
        /*
        $('.CheckConn').html(' Su ordenador cuenta con internet actualmente');

        msg_check.removeClass('icono_check_desconectado');
        msg_check.addClass('icono_check_conectado');

        icono_check.removeClass('icono_check_desconectado');
        icono_check.addClass('icono_check_conectado');*/
    }else{
        $('.CheckConn').show();
        
        /*.html('<i class="fas fa-rss"></i> Su ordenador no cuenta con internet actualmente');

        msg_check.removeClass('icono_check_conectado');
        msg_check.addClass('icono_check_desconectado');

        icono_check.removeClass('icono_check_conectado');
        icono_check.addClass('icono_check_desconectado');*/

       
    }
}

setInterval(function(){ 
    checkconnection();
}, 1000);

</script>
</body>
</html>
