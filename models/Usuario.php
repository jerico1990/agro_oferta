<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\helpers\Json;
/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string|null $usuario
 * @property string|null $clave
 * @property int|null $estado
 * @property string|null $fecha_registro
 */
class Usuario extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public $auth_key;
    public $verifyCode;
    public static function tableName()
    {
        return 'ofe_tcm_usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_estado'], 'integer'],
            [['fec_created_at'], 'safe'],
            [['txt_usuario', 'txt_clave'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'txt_usuario' => 'Usuario',
            'txt_clave' => 'Clave',
            'cod_estado' => 'Estado',
            'fec_created_at' => 'Fecha Registro',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getUsername(){
        return $this->txt_usuario;
    }

    public static function findByUsername($password,$username)
    {
      return static::find()->where('txt_usuario=:txt_usuario and cod_estado=:cod_estado',[':txt_usuario' => $username,':cod_estado'=>1])->one();
    }

    public function validatePassword($password,$username){
        $model=static::find()->where('txt_usuario=:txt_usuario and cod_estado=:cod_estado',[':txt_usuario' => $username,':cod_estado'=>1])->one();
        if ($password == $model->txt_clave)
        {
            return $model;
        }
        return false;
    }

    public static function findIdentityByAccessToken($token, $type = null){
        if ($user['accessToken'] === $token) {
           return new static($user);
        }
        return null;
    }

    public function getAuthKey(){
        return $this->auth_key;
    }

    public function validateAuthKey($authKey){
        return $this->getAuthKey() === $authKey;
    }

    public function validateDNI($dni){
        $consultadni = curl_init();
        //curl_setopt($consultadni_sosen, CURLOPT_URL,"http://fspreset.minagri.gob.pe:5000/consultadni/");
        curl_setopt($consultadni, CURLOPT_URL,"http://fspreset.minagri.gob.pe:5000/consultadni/");
        curl_setopt($consultadni, CURLOPT_POST, TRUE);
        curl_setopt($consultadni, CURLOPT_POSTFIELDS, "dni=".$dni."");
        curl_setopt($consultadni, CURLOPT_RETURNTRANSFER, true);
        $remote_server_consultadni = curl_exec ($consultadni);

        if(!$remote_server_consultadni){
            return [
                'ok'        => false,
                'bandera'   => '3',
                'msg'       => 'No se encuentra ningun servicio activo.',
            ];

            /*
            curl_close ($consultadni);

            $consultadni_sosen = curl_init();
            curl_setopt($consultadni_sosen, CURLOPT_URL,"http://fspreset.minagri.gob.pe:5000/consultadni_sosen/");
            curl_setopt($consultadni_sosen, CURLOPT_POST, TRUE);
            curl_setopt($consultadni_sosen, CURLOPT_POSTFIELDS, "dni=".$dni."");
            curl_setopt($consultadni_sosen, CURLOPT_RETURNTRANSFER, true);
            $remote_server_consultadni_sosen = curl_exec($consultadni_sosen);

           
            if(!$remote_server_consultadni_sosen){
                return [
                    'ok'        => false,
                    'bandera'   => '3',
                    'msg'       => 'No se encuentra ningun servicio activo.',
                ];
            }

            // cerramos la sesión cURL
            curl_close ($consultadni_sosen);

            $json_consultadni_sosen = Json::decode($json = $remote_server_consultadni_sosen);
            
            if(!$json_consultadni_sosen["ok"]){
                return [
                    'ok'        => false,
                    'bandera'   => '1',
                    'msg'       => 'No se encuentra la información del DNI.',
                ];
            }

            
            
            $json_consultadni_sosen = Json::decode($json = $json_consultadni_sosen["message"]);
            if(!$json_consultadni_sosen["ok"]){
                return [
                    'ok'        => false,
                    'bandera'   => '1',
                    'msg'       => 'No se encuentra la información del DNI.',
                ];
            }
            return $json_consultadni_sosen;
            /*

            return [
                'ok'        => true,
                'apellidos' => $json_xconsultadni["apPrimer"] . " " . $json_xconsultadni["apSegundo"],
                'nombres'   => $json_xconsultadni["prenombres"],
                'dni'       => $dni
            ];*/
        }
        
        $httpCode = curl_getinfo($consultadni, CURLINFO_HTTP_CODE);
        if($httpCode == 404){
            return [
                'ok'        => false,
                'bandera'   => '3',
                'msg'       => 'No se encuentra ningun servicio activo.',
            ];
        }
        //var_dump($httpCode);die;
        // cerramos la sesión cURL
        
        $json_consultadni = Json::decode($json = $remote_server_consultadni);
        //var_dump($json_consultadni);die;
        //$json_consultadni = Json::decode($json = $remote_server_consultadni);
        if(!$json_consultadni["ok"]){
            return [
                'ok'        => false,
                'bandera'   => '1',
                'msg'       => 'No se encuentra la información del DNI.',
            ];
        }
        $json_consultadni = Json::decode($json = $json_consultadni["message"]);

        curl_close ($consultadni);

        //return Json::decode($json_validate_exists["message"]);
        return [
            'ok'        => true,
            'apellidos' => $json_consultadni["apPrimer"] . " " . $json_consultadni["apSegundo"],
            'nombres'   => $json_consultadni["prenombres"],
            'dni'       => $dni
        ];
    }
}
