<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Agro Oferta</title>

    <link rel="apple-touch-icon" href="<?= \Yii::$app->request->BaseUrl ?>/remark/topicon/assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="<?= \Yii::$app->request->BaseUrl ?>/remark/topicon/assets/images/favicon.ico">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/topicon/assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/jquery-wizard/jquery-wizard.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/formvalidation/formValidation.css">

    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/topicon/assets/examples/css/tables/datatable.css">
    
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/bootstrap-maxlength/bootstrap-maxlength.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/timepicker/jquery-timepicker.css">


    <!-- Fonts -->
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/fonts/web-icons/web-icons.min.css">
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/jquery/jquery.js"></script>
    <!-- Core  -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/animsition/animsition.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    
    <!-- Plugins -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/switchery/switchery.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/intro-js/intro.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/screenfull/screenfull.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/slidepanel/jquery-slidePanel.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/formvalidation/formValidation.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/formvalidation/framework/bootstrap.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/jquery-wizard/jquery-wizard.js"></script>

    <!-- Plugins -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/switchery/switchery.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/intro-js/intro.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/screenfull/screenfull.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/slidepanel/jquery-slidePanel.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/asprogress/jquery-asProgress.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/draggabilly/draggabilly.pkgd.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/raty/jquery.raty.js"></script>
    
    <!-- Scripts -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Component.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Base.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Config.js"></script>
    
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/topicon/assets/js/Section/Menubar.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/topicon/assets/js/Section/Sidebar.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/topicon/assets/js/Section/PageAside.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/topicon/assets/js/Plugin/menu.js"></script>
    
    <!-- Page -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/topicon/assets/js/Site.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/asscrollable.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/slidepanel.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/switchery.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/responsive-tabs.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/tabs.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/asprogress.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/panel.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/asscrollable.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/raty.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
        Breakpoints();
    </script>


    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net/jquery.dataTables.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-rowgroup/dataTables.rowGroup.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-scroller/dataTables.scroller.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-buttons/dataTables.buttons.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-buttons/buttons.html5.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-buttons/buttons.flash.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-buttons/buttons.print.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-buttons/buttons.colVis.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/datatables.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datepair/datepair.min.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datepair/jquery.datepair.min.js"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/bootstrap-datepicker.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/jt-timepicker.js"></script>

    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet@1.5.1/dist/leaflet.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.fullscreen/1.4.2/Control.FullScreen.min.css" />
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/leaflet@1.5.1/dist/leaflet.js" crossorigin=""></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.fullscreen/1.4.2/Control.FullScreen.min.js"></script>
    <script src="https://unpkg.com/esri-leaflet@2.3.3/dist/esri-leaflet.js" integrity="sha512-cMQ5e58BDuu1pr9BQ/eGRn6HaR6Olh0ofcHFWe5XesdCITVuSBiBZZbhCijBe5ya238f/zMMRYIMIIg1jxv4sQ==" crossorigin=""></script>
    <script src="https://consbio.github.io/Leaflet.Basemaps/L.Control.Basemaps.js" crossorigin="anonymous"></script>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/leaflet-Basemaps/L.Control.Basemaps.js"></script>
    <link rel="<?= \Yii::$app->request->BaseUrl ?>/leaflet-Basemaps/L.Control.Basemaps.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.js"></script>
    <script src="https://bowercdn.net/c/es6-promise-3.2.2/es6-promise.min.js"  crossorigin="anonymous"></script>
    <script src="https://bowercdn.net/c/fetch-1.0.0/fetch.js"  crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/all.css" integrity="sha384-Bfad6CLCknfcloXFOyFnlgtENryhrpZCe29RTifKEixXQZ38WheV+i/6YWSzkz3V" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.25.1/moment.min.js"  crossorigin="anonymous"></script>
    <link href="<?= \Yii::$app->request->BaseUrl ?>/c3/c3.css" rel="stylesheet">
    <script src="https://d3js.org/d3.v5.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/c3/c3.min.js"></script>
    <link rel="stylesheet"  href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/toastr/toastr.css"/>

    <link rel="stylesheet"  href="<?= \Yii::$app->request->BaseUrl ?>/remark/topicon/assets/examples/css/advanced/toastr.css"/>

    <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/toastr.js"></script>
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/leaflet-control-topcenter-master/dist/leaflet-control-topcenter.css" />
    <script src="<?= \Yii::$app->request->BaseUrl ?>/leaflet-control-topcenter-master/dist/leaflet-control-topcenter.js" crossorigin="anonymous"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.17/dist/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.17/js/bootstrap-select.min.js"></script>

    <link href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/fonts/font-awesome/font-awesome.css" rel="stylesheet">

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
	<style>
	html,body{
		height:100%;
		width:100%;
		margin:0px;
        padding:0px;
	}
	</style>
</head>
<body>
  	<?= $content ?>
</body>
</html>