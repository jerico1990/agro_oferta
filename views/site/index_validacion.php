<?php

/* @var $this yii\web\View */

$this->title = 'Agro - Ofertas';
use yii\bootstrap\ActiveForm;
?>

<div class="row">
    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-4"></div>
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-4">
        <?php if($message == "2"){ ?>
            <?php $form = ActiveForm::begin(['action'=>\Yii::$app->request->BaseUrl.'/site/informar']); ?>
            <div class="card">
                <div class="card-header">
                    Agro Ofertas
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <input type="hidden" name="dni" value="<?= $dni; ?>">
                        <input type="number" id="dni" disabled value="<?= $dni; ?>" class="form-control" minlength="8" maxlength="8"  placeholder="Ingrese DNI" >
                    </div>
                    <div class="form-group">
                        <input type="text" name="prenombres" id="prenombres" disabled value="<?= $json["nombres"]; ?>" class="form-control" >
                    </div>
                    <div class="form-group">
                        <input type="text" name="apSegundo" id="apSegundo" disabled value="<?= $json["apellidos"]; ?>" class="form-control" >
                    </div>

                    <div class="form-group">
                        <a href="<?= \Yii::$app->request->BaseUrl ?>/usuario/actualizar" class="btn btn-block btn-info">Datos de contacto</a>  
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-primary ">Registra tus productos</button>
                    </div>
                </div>
            </div>

                
            <?php ActiveForm::end(); ?>
        <?php }else if($message == "1"){ ?>
            <div class="card">
                <div class="card-header">
                    Agro Ofertas
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <input type="number" name="dni" id="dni" disabled value="<?= $dni; ?>" class="form-control" minlength="8" maxlength="8"  placeholder="Ingrese DNI" >
                    </div>
                    <div class="form-group">
                        No hay conectividad con el servicio de consultas de DNI.
                    </div>
                    <div class="form-group">
                        <a href="<?= \Yii::$app->request->BaseUrl ?>">Regresar</a>
                    </div>
                </div>
            </div>

        <?php }else if($message == "3"){ ?>
            <div class="card">
                <div class="card-header">
                    Agro Ofertas
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <input type="number" name="dni" id="dni" disabled value="<?= $dni; ?>" class="form-control" minlength="8" maxlength="8"  placeholder="Ingrese DNI" >
                    </div>
                    <div class="form-group">
                        No hay conectividad con el servicio de consultas de DNI.
                    </div>
                    <div class="form-group">
                        <a href="<?= \Yii::$app->request->BaseUrl ?>">Regresar</a>
                    </div>
                </div>
            </div>

                
        <?php } ?>
    </div>
    <div class="col-md-4"></div>
</div>

<script>
if (window.history.replaceState) { // verificamos disponibilidad
    window.history.replaceState(null, null, window.location.href);
}
</script>