<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Galpon */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(['action'=>\Yii::$app->request->BaseUrl.'/usuario/validar','options' => ['id' => 'formLogin','class' => 'form-horizontal']]); ?>
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Iniciar sesión</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>Usuario</label>
            <input type="text" id="usuario-username" class="form-control" name="LoginForm[username]" >
        </div>
        <div class="form-group">
            <label>Clave</label>
            <input type="password" id="usuario-password" class="form-control" name="LoginForm[password]" >
        </div>
        <div class="form-group alerta_login">
            
        </div>
    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary btn-iniciar-sesion">Ingresar</button>
    </div>
</div>
<?php ActiveForm::end(); ?>