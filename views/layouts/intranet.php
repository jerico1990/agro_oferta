
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="bootstrap admin template">
        <meta name="author" content="">
        
        <title>MIDAGRI | Agro Oferta</title>
        
        <link rel="apple-touch-icon" href="<?= \Yii::$app->request->BaseUrl ?>/remark/base/assets/images/apple-touch-icon.png">
        <link rel="shortcut icon" href="<?= \Yii::$app->request->BaseUrl ?>/remark/base/assets/images/favicon.ico">
        
        <!-- Stylesheets -->
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/css/bootstrap-extend.min.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/base/assets/css/site.min.css">
        
        <!-- Plugins -->
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/animsition/animsition.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/asscrollable/asScrollable.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/switchery/switchery.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/intro-js/introjs.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/slidepanel/slidePanel.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/flag-icon-css/flag-icon.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/base/assets/examples/css/dashboard/v1.css">
        
        
        <!-- Fonts -->
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/fonts/weather-icons/weather-icons.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/fonts/web-icons/web-icons.min.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/fonts/brand-icons/brand-icons.min.css">
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.css">
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/remark/base/assets/examples/css/tables/datatable.css">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js"  crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"/>
        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/AdminLTE-3.0.5/plugins/fontawesome-free/css/all.min.css">

        
        <!--[if lt IE 9]>
        <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
        <![endif]-->
        
        <!--[if lt IE 10]>
        <script src="../../global/vendor/media-match/media.match.min.js"></script>
        <script src="../../global/vendor/respond/respond.min.js"></script>
        <![endif]-->
        
        <!-- Scripts -->
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/breakpoints/breakpoints.js"></script>
        <script>
        Breakpoints();
        </script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/jquery/jquery.js"></script>

        
        <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.17/dist/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.17/js/bootstrap-select.min.js"></script>
    </head>
    <body class="animsition dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

        <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
        
            <div class="navbar-header">
                <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided" data-toggle="menubar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="hamburger-bar"></span>
                </button>
                <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse" data-toggle="collapse">
                    <i class="icon wb-more-horizontal" aria-hidden="true"></i>
                </button>
                <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
                    <img class="navbar-brand-logo" src="<?= \Yii::$app->request->BaseUrl ?>/img/LogoMinagri.png" title="Monitoreo">
                    <span class="navbar-brand-text hidden-xs-down"> </span>
                </div>
            </div>
            
            <div class="navbar-container container-fluid">
                <!-- Navbar Collapse -->
                <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
                    <!-- Navbar Toolbar -->
                    <ul class="nav navbar-toolbar">
                        <li class="nav-item hidden-float" id="toggleMenubar">
                            <a class="nav-link" data-toggle="menubar" href="#" role="button">
                                <i class="icon hamburger hamburger-arrow-left">
                                    <span class="sr-only">Toggle menubar</span>
                                    <span class="hamburger-bar"></span>
                                </i>
                            </a>
                        </li>
                        <li class="nav-item hidden-sm-down" id="toggleFullscreen">
                            <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                                <span class="sr-only">Toggle fullscreen</span>
                            </a>
                        </li>
                    </ul>
                    <!-- End Navbar Toolbar -->
                
                    <!-- Navbar Toolbar Right -->
                    <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                        <li class="nav-item dropdown">
                            <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false" data-animation="scale-up" role="button">
                                <span class="avatar avatar-online">
                                <img src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/portraits/5.jpg" alt="...">
                                <i></i>
                                </span>
                            </a>
                            <div class="dropdown-menu" role="menu">
                                <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon wb-settings" aria-hidden="true"></i> Configuración</a>
                                <div class="dropdown-divider" role="presentation"></div>
                                <a class="dropdown-item" href="<?= \Yii::$app->request->BaseUrl ?>/login/logout" role="menuitem"><i class="icon wb-power" aria-hidden="true"></i> Cerrar sesión</a>
                            </div>
                        </li>
                    </ul>
                    <!-- End Navbar Toolbar Right -->
                </div>
                <!-- End Navbar Collapse -->
            </div>
        </nav>    
        
        <div class="site-menubar">
            <div class="site-menubar-body">
                <div>
                    <div>
                        <ul class="site-menu" data-plugin="menu">
                            
                            <li class="site-menu-category">General</li>
                            <li class="site-menu-item">
                                <a href="<?= \Yii::$app->request->BaseUrl ?>/oferta/index">
                                    <i class="site-menu-icon wb-dashboard" aria-hidden="true"></i>
                                    <span class="site-menu-title">Ofertas - Modificaciones</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a href="<?= \Yii::$app->request->BaseUrl ?>/oferta/historial">
                                    <i class="site-menu-icon wb-dashboard" aria-hidden="true"></i>
                                    <span class="site-menu-title">Ofertas - Historial</span>
                                </a>
                            </li>
                        </ul>     
                    </div>
                </div>
            </div>
            <div class="site-menubar-footer">
                <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip" data-original-title="Configuración">
                    <span class="icon wb-settings" aria-hidden="true"></span>
                </a>
                <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Bloquear">
                    <span class="icon wb-eye-close" aria-hidden="true"></span>
                </a>
                <a href="<?= \Yii::$app->request->BaseUrl ?>/login/logout" data-placement="top" data-toggle="tooltip" data-original-title="Cerrar sesión">
                    <span class="icon wb-power" aria-hidden="true"></span>
                </a>
            </div>
        </div>   

        <!-- Page -->
        <div class="page">
            <?= $content ?>
        </div>
        <!-- End Page -->


        <!-- Footer -->
        <footer class="site-footer">
            <div class="site-footer-legal">© 2020 <a href="#">MINAGRI - IDE</a></div>
            
        </footer>
        <!-- Core  -->
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
        
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/popper-js/umd/popper.min.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/bootstrap/bootstrap.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/animsition/animsition.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/mousewheel/jquery.mousewheel.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/asscrollable/jquery-asScrollable.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
        
        <!-- Plugins -->
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/switchery/switchery.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/intro-js/intro.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/screenfull/screenfull.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/skycons/skycons.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/chartist/chartist.min.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/aspieprogress/jquery-asPieProgress.min.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/jvectormap/maps/jquery-jvectormap-au-mill-en.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        
        <!-- Scripts -->
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Component.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Base.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Config.js"></script>
        
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/base/assets/js/Section/Menubar.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/base/assets/js/Section/GridMenu.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/base/assets/js/Section/Sidebar.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/base/assets/js/Section/PageAside.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/base/assets/js/Plugin/menu.js"></script>
        
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/config/colors.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/base/assets/js/config/tour.js"></script>
        <script>Config.set('assets', '../assets');</script>
        
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net/jquery.dataTables.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-rowgroup/dataTables.rowGroup.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-scroller/dataTables.scroller.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-buttons/dataTables.buttons.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-buttons/buttons.html5.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-buttons/buttons.flash.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-buttons/buttons.print.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-buttons/buttons.colVis.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/datatables.js"></script>

        <!-- Page -->
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/base/assets/js/Site.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/asscrollable.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/slidepanel.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/switchery.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/matchheight.js"></script>
        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/global/js/Plugin/jvectormap.js"></script>

        <script src="<?= \Yii::$app->request->BaseUrl ?>/remark/base/assets/examples/js/dashboard/v1.js"></script>
    </body>
</html>
