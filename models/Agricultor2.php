<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agricultor".
 *
 * @property int $id
 * @property string|null $nombres
 * @property string|null $ap_paterno
 * @property string|null $ap_materno
 * @property string|null $telefono
 * @property string|null $celular
 * @property string|null $email
 * @property string|null $direccion
 * @property int|null $usuario_id
 */
class Agricultor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ofe_tcm_agricultor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_id'], 'default', 'value' => null],
            [['usuario_id'], 'integer'],
            [['nombres', 'ap_paterno', 'ap_materno', 'direccion'], 'string', 'max' => 100],
            [['telefono', 'celular'], 'string', 'max' => 10],
            [['email'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombres' => 'Nombres',
            'ap_paterno' => 'Ap Paterno',
            'ap_materno' => 'Ap Materno',
            'telefono' => 'Telefono',
            'celular' => 'Celular',
            'email' => 'Email',
            'direccion' => 'Direccion',
            'usuario_id' => 'Usuario ID',
        ];
    }
}
