<?php

/* @var $this yii\web\View */

$this->title = 'Agro - Ofertas';
use yii\bootstrap\ActiveForm;
?>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.17/dist/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.17/js/bootstrap-select.min.js"></script>


<style>
.btn-light{
    background-color:#fff;
    border:1px solid #ced4da;
    color:#495057;
}
.bootstrap-select>.dropdown-hide.bs-placeholder{
    color:#495057;
}
</style>

<?php $form = ActiveForm::begin(['action'=>\Yii::$app->request->BaseUrl.'/site/create-oferta','options' => ['id' => 'formOferta','class' => 'form-horizontal']]); ?>

<!--<input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />-->
<div class="site-index">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header">
                    Agro Oferta - Informa de tus cultivos - <?= $json["nombres"]. " " .$json["apellidos"]  ; ?>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label for="region">Seleccione la Región</label>
                                <select class="form-control" id="region" name="region_id">
                                    <option value>Seleccionar región</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="provincia">Seleccione la Provincia</label>
                                <select class="form-control" id="provincia" name="provincia_id">
                                    <option value>Seleccionar provincia</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="distrito">Seleccione la Distrito</label>
                                <select class="form-control" id="distrito" name="distrito_id">
                                    <option value>Seleccionar distrito</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="cultivo">Seleccione el producto</label>
                                <select class="cultivo_id form-control " id="cultivo_id" name="cultivo_id" data-live-search="true" data-size="10">
                                    <option value>Seleccionar producto</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label for="precio">Ingrese el precio del producto por (S/ x kg /L /Lb)</label>
                                <input type="number" class="form-control" id="precio" name="precio" maxlength = "7"  oninput="javascript: if (this.value.length > this.maxLength || (this.value.split('.')[1] && this.value.split('.')[1].length > 2)) {   this.value = parseFloat(this.value.slice(0, this.maxLength)).toFixed(2);   }" value="0.00">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="cantidad">Ingrese la cantidad total a ofertar (Kg / L / Lb)</label>
                                <input type="number"  class="form-control" id="cantidad" name="cantidad" maxlength = "7"  oninput="javascript: if (this.value.length > this.maxLength || (this.value.split('.')[1] && this.value.split('.')[1].length > 0)) {   this.value = parseFloat(this.value.slice(0, this.maxLength)).toFixed(0);   }" value="0">
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="fecha_cosecha">Indique la fecha de la cosecha/venta del producto ofertado</label>
                                <input type="date" class="form-control" id="fecha_cosecha" min="<?php echo date('Y-m-d'); ?>"  name="fecha_cosecha">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md" >
                            <div class="form-group">
                                <label for="ubicacion">Ubica tu parcela, donde se cosechará el producto ofertado dandole clic al <i class="icon fa-map-marker"></i></label>
                                <div  id="map" class="map sidebar-map" style="height:500px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <input type="hidden" name="dni" value="<?= $dni; ?>">
                                <input type="hidden" name="nombres_apellidos" value="<?= $json["nombres"]. " , " .$json["apellidos"] ; ?>">
                                <input type="hidden" id="latitud" name="latitud">
                                <input type="hidden" id="longitud" name="longitud">
                                <button class="btn btn-primary btn-block btn-grabar-oferta">Registrar</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group alerta">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <div class="clearfix"></div>
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card">
            <div class="card-header">
                    Lista de Ofertas
                </div>
                <div class="card-body overflow-auto">
                    <table class=" ofertas table dt-responsive nowrap" >
                        <thead>
                            <th>Departamento</th>
                            <th>Provincia</th>
                            <th>Distrito</th>
                            <th>Producto</th>
                            <th>Precio (S/. x Kg)</th>
                            <th>Cantidad total ofertada</th>
                            <th>Fecha de cosecha</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                        
                        </tbody>
                    </table>
                </div>
            </div>
            <br>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

<!-- Modal -->
<div class="modal fade" id="myModal" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-center modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Mensaje informativo</h4>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-salir" data-dismiss="modal">Salir</button>
                <button type="button" class="btn btn-success btn-registrar-misma-ubicacion">Registrar otro producto en la misma ubicación</button>
                <button type="button" class="btn btn-success btn-registrar-otra-zona">Registrar otro producto en otra zona</button>
            </div>
        </div>
    </div>
</div>

<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog text-center" role="document">
        <i class="fa fa-refresh fa-spin"></i>
        Cargando
    </div>
</div>

<script>
var loading =   $('#staticBackdrop');
loading.modal("show");

//$.fn.selectpicker.Constructor.BootstrapVersion = '4';
var dni = '<?= $dni; ?>';

var boundsNacional = L.latLngBounds([]);
var boundsRegion = L.latLngBounds([]);
var boundsProvincia = L.latLngBounds([]);
var boundsDistrito = L.latLngBounds([]);


var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var map = L.map('map', {
    zoomControl: false,
    scrollWheelZoom: true,
    fullscreenControl: false,
    attributionControl:false,
    dragging: true,
    layers:[],
    minZoom: 3,
    maxNativeZoom: 17,
    maxZoom:17,
    zoom: 6,
    center: [-9.1899672, -75.015152],
});

/* Mapa Base */

L.esri.basemapLayer('Imagery').addTo(map);

// Controles de dibujo
var drawnItems = new L.FeatureGroup();
map.addLayer(drawnItems);
var options = {
    position: 'topleft',
    draw: {
        marker: true,
        polyline: false,
        polygon: false,
        circle: false, // Turns off this drawing tool
        rectangle: false,
    },
    edit: {
        featureGroup: drawnItems, //REQUIRED!!
        remove: true
    }
};

var lat_lng;
var drawControl = new L.Control.Draw(options);
map.addControl(drawControl);

map.on('draw:created', function (e) {
    var type = e.layerType,
        layer = e.layer;
        console.log(layer);
        layer.options.showMeasurements=true;
    if (type === 'marker') {
        // Do marker specific actions

        //console.log(layer.getLatLng());
        //alert(layer.getLatLng());
        drawnItems.clearLayers();
        //lat_lng = layer.getLatLng();
        

        var point = turf.point([layer.getLatLng().lng,layer.getLatLng().lat]);
        
        if (!turf.booleanPointInPolygon(point, distrito_poligono)) {
            alert("Su marcado no se encuentra dentro de la zona del distrito");
            return false;
        }

        $('#latitud').val(layer.getLatLng().lat);
        $('#longitud').val(layer.getLatLng().lng);

    }else if(type === 'circle'){
        //console.log(layer);
        //console.log(layer.getLatLng());
        //console.log(layer.getRadius());
        
        //alert(layer.getLatLng()+ "\t" + layer.getRadius());
    }else if(type === 'rectangle'){
        //console.log(layer);
        //console.log(layer.getLatLngs());
        //alert(layer.getLatLngs());
    }else if(type === 'polygon'){
        //console.log(layer);
        //console.log(layer.getLatLngs());
        //alert(layer.getLatLngs());
    }
    // Do whatever else you need to. (save to db, add to map etc)
    drawnItems.addLayer(layer);
    
});

map.on('draw:edited', function () {
    // Update db to save latest changes.
    //console.log("bb");
});

map.on('draw:deleted', function () {
    // Update db to save latest changes.
    //console.log("ccc");
});

var regiones = new L.esri.featureLayer({
            url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/0/',
            minZoom: 5,
            style : function (feature) {
                return { color: 'white', weight: 2 };
            },
        });

regiones.addTo(map);

var provincias;
var distritos;
var region;
var provincia;
var regiones_options;
var provincias_options;
var distritos_options;


/* Cargando valores a selector*/
regiones.on("load",async function(evt) {
    regiones_options = "<option value>Seleccionar Región</option>";
    regiones.eachFeature(async function (layer) {
        regiones_options = regiones_options + "<option value='" + layer.feature.properties.iddpto + "'>" + layer.feature.properties.nombdep + "</option>";
    })
    $('#region').html(regiones_options);

    boundsNacional = L.latLngBounds([]);
    // loop through the features returned by the server
    regiones.eachFeature(async function (layer) {
        // get the bounds of an individual feature
        var layerBounds = layer.getBounds();
        // extend the bounds of the collection to fit the bounds of the new feature
        await boundsNacional.extend(layerBounds);
    });
    await loading.modal('hide');
    // once we've looped through all the features, zoom the map to the extent of the collection
    //map.fitBounds(boundsRegiones);

});

var iddpto;
$('body').on('change', '#region', function (e) {
    loading.modal("show");
    iddpto = $(this).val();
    
    provincias_options = "<option value>Seleccione provincia</option>";
    $('#cboConsProvinciasServicio').html(provincias_options);
    idprov = '';

    distritos_options = "<option value>Seleccione distrito</option>";
    $('#distrito').html(distritos_options);
    iddist = '';

    
    if(iddpto){
        CultivosInformar(iddpto);
        map.fitBounds(boundsNacional);
        map.removeLayer(regiones);
        if (region != undefined) {
            map.removeLayer(region);
        }
        
        
        if (provincias != undefined) {
            map.removeLayer(provincias);
            if(distritos){
                map.removeLayer(distritos);
            }
        }
        
        region = new L.esri.featureLayer({
            url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/0/',
            where: "iddpto='" + iddpto + "'",
            style : function (feature) {
                return { color: 'white', weight: 2 };
            },
        }).addTo(map);
        
        region.once('load',async function (evt) {
            // create a new empty Leaflet bounds object
            boundsRegion = L.latLngBounds([]);
            // loop through the features returned by the server
            region.eachFeature(async function (layer) {
                // get the bounds of an individual feature
                var layerBounds = layer.getBounds();
                // extend the bounds of the collection to fit the bounds of the new feature
                await boundsRegion.extend(layerBounds);
            });

            // once we've looped through all the features, zoom the map to the extent of the collection
            await map.fitBounds(boundsRegion);
        });

        provincias = new L.esri.featureLayer({
            url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/1/',
            where: "iddpto='" + iddpto + "'",
            style : function (feature) {
                return { color: 'white', weight: 2 };
            },
        }).addTo(map);

        provincias.on("load",async function(evt) {
            provincias_options = "<option value>Seleccionar provincia</option>";
            provincias.eachFeature(async function (layer) {
                provincias_options = provincias_options + "<option value='" + layer.feature.properties.idprov + "'>" + layer.feature.properties.nombprov + "</option>";
            })
            $('#provincia').html(provincias_options);
            await loading.modal('hide');
        });
    }else if(iddpto == ''){
        map.fitBounds(boundsNacional);

        if (region != undefined) {
            map.removeLayer(region);
        }

        if (provincias != undefined) {
            map.removeLayer(provincias);
        }

        if (distritos != undefined) {
            map.removeLayer(distritos);
        }

        regiones.addTo(map);

        
        /* Cargando valores a selector*/
        /*
        regiones.on("load", function (evt) {
            regiones_options = "<option value>Seleccione región</option>";
            regiones.eachFeature(function (layer) {
                regiones_options = regiones_options + "<option value='" + layer.feature.properties.iddpto + "'>" + layer.feature.properties.nombdep + "</option>";
            })
            $('#region').html(regiones_options);

            boundsNacional = L.latLngBounds([]);
            // loop through the features returned by the server
            regiones.eachFeature(function (layer) {
                // get the bounds of an individual feature
                var layerBounds = layer.getBounds();
                // extend the bounds of the collection to fit the bounds of the new feature
                boundsNacional.extend(layerBounds);
            });
        });*/
    }
    

});

var idprov;

$('body').on('change', '#provincia', function (e) {
    loading.modal("show");
    idprov = $(this).val();
    distritos_options = "<option value>Seleccione distrito</option>";
    $('#distrito').html(distritos_options);
    iddist = '';

    if (idprov) {
        map.fitBounds(boundsRegion);
        map.removeLayer(region);
        map.removeLayer(provincias);
        if (distritos != undefined) {
            map.removeLayer(distritos);
            iddist='';
        }
            

        provincias = new L.esri.featureLayer({
            url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/1/',
            where: "idprov='" + idprov + "'",
            style : function (feature) {
                return { color: 'white', weight: 2 };
            },
        }).addTo(map);
        
        provincias.once('load',async function (evt) {
            // create a new empty Leaflet bounds object
            boundsProvincia = L.latLngBounds([]);
            // loop through the features returned by the server
            provincias.eachFeature(async function (layer) {
                // get the bounds of an individual feature
                var layerBounds = layer.getBounds();
                // extend the bounds of the collection to fit the bounds of the new feature
                await boundsProvincia.extend(layerBounds);
            });

            // once we've looped through all the features, zoom the map to the extent of the collection
            await map.fitBounds(boundsProvincia);
        });

        distritos = new L.esri.featureLayer({
            url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/2/',
            where: "idprov='" + idprov + "'",
            style : function (feature) {
                return { color: 'white', weight: 2 };
            },
        }).addTo(map);

        distritos.on("load",async function(evt) {
            var distritos_options = "<option value>Seleccione distrito</option>";
            distritos.eachFeature(async function (layer) {
                distritos_options = distritos_options + "<option value='" + layer.feature.properties.iddist + "'>" + layer.feature.properties.nombdist + "</option>";
            })
            $('#distrito').html(distritos_options);
            await loading.modal('hide');
        });
    }else if(idprov == ''){
        map.fitBounds(boundsRegion);
            
        if (region != undefined) {
            map.removeLayer(region);
        }

        if (provincias != undefined) {
            map.removeLayer(provincias);
        }

        if (distritos != undefined) {
            map.removeLayer(distritos);
        }

        /* quitando tabla de agricultores */
        //map.removeControl(lista_agricultores)

        region = new L.esri.featureLayer({
            url: 'https://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/0/',
            where: "iddpto='" + iddpto + "'",
            style: function (feature) {
                return { color: 'white', weight: 2 };
            },
        });
        region.addTo(map);

        region.once('load', function (evt) {
            // create a new empty Leaflet bounds object
            boundsRegion = L.latLngBounds([]);
            // loop through the features returned by the server
            region.eachFeature(function (layer) {
                // get the bounds of an individual feature
                var layerBounds = layer.getBounds();
                // extend the bounds of the collection to fit the bounds of the new feature
                boundsRegion.extend(layerBounds);
            });
            // once we've looped through all the features, zoom the map to the extent of the collection
            map.fitBounds(boundsRegion);
        });

        provincias = new L.esri.featureLayer({
            url: 'https://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/1/',
            where: "iddpto='" + iddpto + "'",
            style: function (feature) {
                return { color: 'white', weight: 2 };
            },
        });
        provincias.addTo(map);

        provincias.on("load",async function (evt) {
            provincias_options = "<option value>Seleccione provincia</option>";
            provincias.eachFeature(async function (layer) {
                provincias_options = provincias_options + "<option value='" + layer.feature.properties.idprov + "'>" + layer.feature.properties.nombprov + "</option>";
            })
            $('#provincia').html(provincias_options);
            await loading.modal('hide');
        });
    }

    
});

var iddist;
$('body').on('change', '#distrito', function (e) {
    loading.modal("show");
    iddist = $(this).val();
    
    if(iddist){
        //console.log(map.fitBounds(boundsProvincia))
        map.fitBounds(boundsProvincia);

        map.removeLayer(provincias);
        map.removeLayer(distritos);


        distritos = new L.esri.featureLayer({
            url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/2/',
            where: "iddist='" + iddist + "'",
            style : function (feature) {
                return { color: 'white', weight: 2 };
            },
        });
        distritos.addTo(map);
        

        distritos.once('load',async  function (evt) {
            // create a new empty Leaflet bounds object
            var bounds = L.latLngBounds([]);
            // loop through the features returned by the server
            distritos.eachFeature(async function (layer) {
                // get the bounds of an individual feature

               

                distrito_poligono = turf.multiPolygon([layer.feature.geometry.coordinates]);
                
                var layerBounds = layer.getBounds();
                // extend the bounds of the collection to fit the bounds of the new feature
                await bounds.extend(layerBounds);
            });

            // once we've looped through all the features, zoom the map to the extent of the collection
            
            await map.fitBounds(bounds);
            
        });

        distritos.on("load",async function(evt) {
            await loading.modal('hide');
        });

    }else if(iddist == ''){
        map.fitBounds(boundsProvincia);
            //map.removeLayer(region);
            if (provincias != undefined) {
                map.removeLayer(provincias);
            }


            if (distritos != undefined) {
                map.removeLayer(distritos);
                iddist='';
            }


            provincias = new L.esri.featureLayer({
                url: 'https://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/1/',
                where: "idprov='" + idprov + "'",
                style: function (feature) {
                    return { color: 'white', weight: 2 };
                },
            });
            provincias.addTo(map);

            provincias.once('load',async function (evt) {
                // create a new empty Leaflet bounds object
                boundsProvincia = L.latLngBounds([]);
                // loop through the features returned by the server
                provincias.eachFeature(async function (layer) {
                    // get the bounds of an individual feature
                    var layerBounds = layer.getBounds();
                    // extend the bounds of the collection to fit the bounds of the new feature
                    await boundsProvincia.extend(layerBounds);
                });

                // once we've looped through all the features, zoom the map to the extent of the collection
                await map.fitBounds(boundsProvincia);
            });

            distritos = new L.esri.featureLayer({
                url: 'https://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/2/',
                where: "idprov='" + idprov + "'",
                style: function (feature) {
                    return { color: 'white', weight: 2 };
                },
            });
            distritos.addTo(map);

            distritos.on("load",async function (evt) {
                distritos_options = "<option value>Seleccione distrito</option>";
                distritos.eachFeature(async function (layer) {
                    distritos_options = distritos_options + "<option value='" + layer.feature.properties.iddist + "'>" + layer.feature.properties.nombdist + "</option>";
                })
                $('#distrito').html(distritos_options);
                await loading.modal('hide');
            });
    }
});


var lista_cultivos =new Array();
//Cultivos();
CultivosCarga();
async function CultivosCarga(){

    urls =  [
        "http://fspreset.minagri.gob.pe:3000/ofe_productos_todos",
    ];
    let results =await Promise.all(urls.map((url) => fetch(url,{method:'POST'}).then((r) => r.json()).catch(function(error) { alert('No hay conectividad con el sistema');console.log('Hubo un problema con la petición Fetch:' + error.message);})  ));
    jQuery.each(results, function(i, result) {
        $.map( result, function( val, i ) {
            let cod = parseInt(val.c2);
            lista_cultivos[cod]= val.c3;
        });
    });
    DistritosServicio();
}


async function CultivosInformar(region){
    /*$('.cultivo_id').html("<option value>Seleccionar producto</option>");
    $('.cultivo_id').val('');
    $('.cultivo_id').selectpicker();
    $('.cultivo_id').selectpicker('refresh');*/
    $('.cultivo_id').selectpicker('destroy');
    await $.ajax({
        url:'http://fspreset.minagri.gob.pe:3000/ofe_productos',
        method: 'POST',
        data:{dpto:parseInt(region)},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {
            var cultivo_options = "<option value>Seleccionar producto</option>";
            $.map( results, function( val, i ) {
                cultivo_options = cultivo_options + "<option value='" + val.c2 + "'>" + val.c3 + "</option>";
            });
            $('.cultivo_id').html(cultivo_options);
            $('.cultivo_id').selectpicker();
            $('.cultivo_id').selectpicker('render');

        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });


    /*
    urls =  [
        "http://fspreset.minagri.gob.pe:3000/ofe_productos",
    ];
    let results =await Promise.all(urls.map((url) => fetch(url,{method:'POST',body: JSON.stringify({'dpto':'1'})}).then((r) => r.json())));
    var cultivo_options = "<option value>Seleccionar producto</option>";
    jQuery.each(results, function(i, result) {
        console.log(result);
        $.map( result, function( val, i ) {
            cultivo_options = cultivo_options + "<option value='" + val.COD + "'>" + val.NOMCUL + "</option>";
        });
    });
    $('.cultivo_id').html(cultivo_options);
    $('.cultivo_id').selectpicker();*/
    //ListaOfertas();
}

async function CultivosActualizar(region,oferta_id){

    await $.ajax({
        url:'http://fspreset.minagri.gob.pe:3000/ofe_productos',
        method: 'POST',
        data:{dpto:parseInt(region)},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {
            console.log("Yo debo cargar primero");
            var cultivo_options = "<option value>Seleccionar producto</option>";
            $.map( results, function( val, i ) {
                cultivo_options = cultivo_options + "<option value='" + val.c2 + "'>" + val.c3 + "</option>";
            });
            $('.oferta-cod_cultivo').html(cultivo_options);
            $('.oferta-cod_cultivo').selectpicker();
            Oferta(oferta_id);
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });



}


$('body').on('click', '.btn-grabar-oferta', function (e) {
	e.preventDefault();
    

	var form = $('#formOferta');
	var formData = $('#formOferta').serializeArray();
	if (form.find('.has-error').length) {
		return false;
	}
	
    var v_region_id = $.trim($('#region').val());
    var v_provincia_id = $.trim($('#provincia').val());
    var v_distrito_id = $.trim($('#distrito').val());
    var v_cultivo_id = $.trim($('#cultivo_id').val());
    var v_precio = $.trim($('#precio').val());
    var v_cantidad = $.trim($('#cantidad').val());
    var v_fecha_cosecha = $.trim($('#fecha_cosecha').val());
    var v_latitud = $.trim($('#latitud').val());
    var v_longitud = $.trim($('#longitud').val());
    var error = "";

    if(v_region_id==""){
        error = error + "Debe seleccionar una región <br>";
    }
    if(v_provincia_id==""){
        error = error + "Debe seleccionar una provincia <br>";
    }
    if(v_distrito_id==""){
        error = error + "Debe seleccionar un distrito <br>";
    }
    if(v_cultivo_id==""){
        error = error + "Debe seleccionar un cultivo <br>";
    }
    if(v_precio==""){
        error = error + "Debe ingresar un precio <br>";
    }
    if(v_cantidad==""){
        error = error + "Debe ingresar una cantidad <br>";
    }
    if(v_fecha_cosecha==""){
        error = error + "Debe ingresar una fecha de cosecha <br>";
    }
    if(v_latitud=="" && v_longitud==""){
        error = error + "Debe indicar el punto de recojo <br>";
    }

    if(error!=""){
        $('.alerta').html('<div class="alert alert-danger" role="alert">' + error + '</div>');
        return false;
    }
    $(this).prop('disabled', true);



	$.ajax({
		url:form.attr("action"),
		type: form.attr("method"),
		data: formData,
		dataType: 'json',
        beforeSend:function()
        {
            
        },
		success: function (results) {
			if(results.success){
                $('#myModal .modal-body').html('<div class="alert alert-success" role="alert">Oferta publicada</div>');
                $("#myModal").modal({backdrop: 'static', keyboard: false});
				//$('.alerta').html('<div class="alert alert-success" role="alert">Oferta publicada</div>');
                $('.alerta').html('');
			}else{

                $('.alerta').html('<div class="alert alert-danger" role="alert">Oferta no publicada, por favor realizar de nuevo.</div>');
            }
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert('No hay conectividad con el sistema');
		},
	});
});


var icon_mayorista = L.icon({
    iconUrl: '<?= \Yii::$app->request->BaseUrl ?>/img/mercado_mayorista.png',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11],
});

var icon_minorista = L.icon({
    iconUrl: '<?= \Yii::$app->request->BaseUrl ?>/img/mercado_minorista.png',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11],
});

var icon_supermercados = L.icon({
    iconUrl: '<?= \Yii::$app->request->BaseUrl ?>/img/super_mercados.png',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11],
});

var icon_mercados_itinerantes = L.icon({
    iconUrl: '<?= \Yii::$app->request->BaseUrl ?>/img/mercado_itinerante.svg',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11],
});

var icon_centros_poblados = L.icon({
    iconUrl: '<?= \Yii::$app->request->BaseUrl ?>/img/ccpp.svg',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11],
});


var baseMaps = {};
var capas = {};
var layerControl = L.control.layers(baseMaps,capas, {position: 'topleft'}).addTo(map);
var mayorista,minorista,supermercados,mercados_itinerantes,centros_poblados;

Capas();
async function Capas(){
    /*mayoristas */
    mayoristas = new L.esri.featureLayer({
        url: 'https://swp64arcg.minagri.gob.pe/server/rest/services/AmbitosAdministrativosAgrarios/MercadosAbastecimiento/MapServer/1/',
        //where: "idprov='" + idprov + "'",
        style : function (feature) {
            //return { color: 'white', weight: 2 };
        },
        pointToLayer: function (geojson, latlng) {
            return L.marker(latlng, {
                icon: icon_mayorista
            });
        }
    });

    mayoristas.bindPopup(function (layer) {
        return L.Util.template('<p>NOMBDEP: <strong>{NOMBDEP}</strong> <br> NOMBPROV: <strong>{NOMBPROV}</strong> <br> NOMBDIST: <strong>{NOMBDIST} </strong> <br> LOCAL: <strong>{LOCAL} </strong><br> NOMBRE: <strong>{NOMBRE} </strong> <br> DIRECCION: <strong>{DIRECCION} </strong>  </p>', layer.feature.properties);
    });

    layerControl.addOverlay(mayoristas,"Mayoristas");
    
    /*minoristas */
    minoristas = new L.esri.featureLayer({
        url: 'https://swp64arcg.minagri.gob.pe/server/rest/services/AmbitosAdministrativosAgrarios/MercadosAbastecimiento/MapServer/2/',
        //where: "idprov='" + idprov + "'",
        style : function (feature) {
            //return { color: 'white', weight: 2 };
        },
        pointToLayer: function (geojson, latlng) {
            return L.marker(latlng, {
                icon: icon_minorista
            });
        }
    });
    minoristas.bindPopup(function (layer) {
        return L.Util.template('<p>NOMBDEP: <strong>{NOMBDEP}</strong> <br> NOMBPROV: <strong>{NOMBPROV}</strong> <br> NOMBDIST: <strong>{NOMBDIST} </strong> <br> LOCAL: <strong>{LOCAL} </strong> <br> NOMBRE: <strong>{NOMBRE} </strong> <br> DIRECCION: <strong>{DIRECCION} </strong>  </p>', layer.feature.properties);
    });

    layerControl.addOverlay(minoristas,"Minorista");

    /*supermercados */
    supermercados = new L.esri.featureLayer({
        url: 'https://swp64arcg.minagri.gob.pe/server/rest/services/AmbitosAdministrativosAgrarios/MercadosAbastecimiento/MapServer/3/',
        //where: "idprov='" + idprov + "'",
        style : function (feature) {
            //return { color: 'white', weight: 2 };
        },
        pointToLayer: function (geojson, latlng) {
            return L.marker(latlng, {
                icon: icon_supermercados
            });
        }
    });
    supermercados.bindPopup(function (layer) {
        return L.Util.template('<p>NOMBDEP: <strong>{NOMBDEP}</strong> <br> NOMBPROV: <strong>{NOMBPROV}</strong> <br> NOMBDIST: <strong>{NOMBDIST} </strong> <br> LOCAL: <strong>{LOCAL}</strong> <br> NOMBRE: <strong>{NOMBRE} </strong> <br> DIRECCION: <strong>{DIRECCION} </strong>  </p>', layer.feature.properties);
    });

    layerControl.addOverlay(supermercados,"Supermercados");

    /*mercados_itinerantes */
    mercados_itinerantes = new L.esri.featureLayer({
        url: 'https://swp64arcg.minagri.gob.pe/server/rest/services/AmbitosAdministrativosAgrarios/MercadosAbastecimiento/MapServer/4/',
        //where: "idprov='" + idprov + "'",
        style : function (feature) {
            //return { color: 'white', weight: 2 };
        },
        pointToLayer: function (geojson, latlng) {
            return L.marker(latlng, {
                icon: icon_mercados_itinerantes
            });
        }
    });

    mercados_itinerantes.bindPopup(function (layer) {
        return L.Util.template('<p>NOMBDEP: <strong>{NOMBDEP}</strong> <br> NOMBPROV: <strong>{NOMBPROV}</strong> <br> NOMBDIST: <strong>{NOMBDIST} </strong> <br> CAPITAL: <strong>{CAPITAL}</strong> <br> LUGAR: <strong>{LUGAR} </strong>  </p>', layer.feature.properties);
    });

    layerControl.addOverlay(mercados_itinerantes,"Mercados Itinerantes");


    /*centros_poblados */
    centros_poblados = new L.esri.featureLayer({
        url: 'https://swp64arcg.minagri.gob.pe/server/rest/services/CartografiaNacional/CentrosPoblados/MapServer/0/',
        //where: "idprov='" + idprov + "'",
        style : function (feature) {
            //return { color: 'white', weight: 2 };
        },
        pointToLayer: function (geojson, latlng) {
            return L.marker(latlng, {
                icon: icon_centros_poblados
            });
        },
        minZoom:11,
        maxZoom:17
    });

    centros_poblados.bindPopup(function (layer) {
        return L.Util.template('<p>NOMCCPP_17: <strong>{NOMCCPP_17}</strong> <br> UBIGEO: <strong>{UBIGEO}</strong> <br> CODCCPP: <strong>{CODCCPP} </strong> <br> DEPARTAMEN: <strong>{DEPARTAMEN}</strong> <br> PROVINCIA: <strong>{PROVINCIA} </strong> <br> DISTRITO: <strong>{DISTRITO} </strong> <br> CATEGORIA: <strong>{CATEGORIA} </strong>  </p>', layer.feature.properties);
    });

    layerControl.addOverlay(centros_poblados,"Centros poblados");
}

/*Control*/
map.on('overlayadd', function(eventlayer){
    console.log(iddpto);
    /*if(iddpto== undefined){
        alert("no hay region");
        return false;
    }*/

    //console.log(eventlayer);
    if(eventlayer.name=='Mayoristas'){
        //map.removeLayer(mayoristas);

        $('.mayorista').html('<p class="card-text"><img style="vertical-align:initial" width="15px" src="<?= \Yii::$app->request->BaseUrl ?>/img/mercado_mayorista.png"></img> M. mayorista</p>');
        
        if(iddpto && idprov && iddist){
            mayoristas.setWhere( "IDDIST = '" + iddist + "'");
        }else if(iddpto && idprov){
            mayoristas.setWhere( "IDPROV = '" + idprov + "'");
        }else if(iddpto){
            mayoristas.setWhere( "IDDPTO = '" + iddpto + "'");
        }

        
        //this.removeLayer(mayoristas);
    }else if(eventlayer.name=='Minorista'){
        $('.minorista').html('<p class="card-text"><img style="vertical-align:initial" width="15px" src="<?= \Yii::$app->request->BaseUrl ?>/img/mercado_minorista.png"></img> M. minorista</p>');

        if(iddpto && idprov && iddist){
            minoristas.setWhere( "IDDIST = '" + iddist + "'");
        }else if(iddpto && idprov){
            minoristas.setWhere( "IDPROV = '" + idprov + "'");
        }else if(iddpto){
            minoristas.setWhere( "IDDPTO = '" + iddpto + "'");
        }

    }else if(eventlayer.name=='Supermercados'){
        $('.supermercados').html('<p class="card-text"><img style="vertical-align:initial" width="15px" src="<?= \Yii::$app->request->BaseUrl ?>/img/super_mercados.png"></img> Super mercados</p>');
        if(iddpto && idprov && iddist){
            supermercados.setWhere( "IDDIST = '" + iddist + "'");
        }else if(iddpto && idprov){
            supermercados.setWhere( "IDPROV = '" + idprov + "'");
        }else if(iddpto){
            supermercados.setWhere( "IDDPTO = '" + iddpto + "'");
        }

    }else if(eventlayer.name=='Mercados Itinerantes'){
        $('.mercados_itinerantes').html('<p class="card-text"><img style="vertical-align:initial" width="15px" src="<?= \Yii::$app->request->BaseUrl ?>/img/mercado_itinerante.svg"></img> M. Itinerantes</p>');
        if(iddpto && idprov && iddist){
            mercados_itinerantes.setWhere( "IDDIST = '" + iddist + "'");
        }else if(iddpto && idprov){
            mercados_itinerantes.setWhere( "IDPROV = '" + idprov + "'");
        }else if(iddpto){
            mercados_itinerantes.setWhere( "IDDPTO = '" + iddpto + "'");
        }
    }else if(eventlayer.name=='Centros poblados'){
        $('.centros_poblados').html('<p class="card-text"><img style="vertical-align:initial" width="15px" src="<?= \Yii::$app->request->BaseUrl ?>/img/ccpp.svg"></img> Centros Poblados</p>');
        if(iddpto && idprov && iddist){
            centros_poblados.setWhere( "CODDIST = '" + iddist + "'");
        }else if(iddpto && idprov){
            centros_poblados.setWhere( "CODPROV = '" + idprov + "'");
        }else if(iddpto){
            centros_poblados.setWhere( "CODDPTO = '" + iddpto + "'");
        }
    }
});

map.on('overlayremove', function(eventlayer){
    if(eventlayer.name=='Mayoristas'){
        $('.mayorista').html('');
    }else if(eventlayer.name=='Minorista'){
        $('.minorista').html('');
    }else if(eventlayer.name=='Supermercados'){
        $('.supermercados').html('');
    }else if(eventlayer.name=='Mercados Itinerantes'){
        $('.mercados_itinerantes').html('');
    }else if(eventlayer.name=='Centros poblados'){
        $('.centros_poblados').html('');
    }
});


$('body').on('click', '.btn-salir', function (e) {
    window.location.href = "<?= \Yii::$app->request->BaseUrl ?>/site/";
});

$('body').on('click', '.btn-registrar-otra-zona', function (e) {
    location.reload();
});


$('body').on('click', '.btn-registrar-misma-ubicacion', function (e) {
    //location.reload();
    //$('#region').val('');
    //$('#provincia').val('');
    //$('#distrito').val('');
    ListaOfertas();
    $('.btn-grabar-oferta').prop('disabled', false);
    $('#precio').val('0.00');
    $('#cantidad').val('0.00');
    $('#fecha_cosecha').val('');
    $('#cultivo_id').val('');
    $('#cultivo_id').selectpicker('refresh')
    $('#myModal').modal('hide');
});

var lista_distritos =new Array();
async function DistritosServicio(){

    urls =  [
        "https://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/2/query?where=1%3D1&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=iddpto%2Cnombdep%2Cidprov%2Cnombprov%2Ciddist%2Cnombdist&returnGeometry=false&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentsOnly=false&datumTransformation=&parameterValues=&rangeValues=&f=pjson",
    ];
    let results =await Promise.all(urls.map((url) => fetch(url,{method:'GET'}).then((r) => r.json())));
    //var cultivo_options = "<option value>Por producto</option>";
    jQuery.each(results, function(i, result) {
        
        $.map( result.features, function( val, i ) {
            //console.log(val);
            lista_distritos.push({'iddpto':val.attributes.iddpto,'nombdep':val.attributes.nombdep,'idprov':val.attributes.idprov,'nombprov':val.attributes.nombprov,'iddist':val.attributes.iddist,'nombdist':val.attributes.nombdist});
        });
    });
    ListaOfertas();
}


async function ListaOfertas(){
   

    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/oferta/get-lista-ofertas-productor',
        method: 'POST',
        data:{_csrf:csrf,dni:dni},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {
            if(results && results.success){
                
                var ofertas ="";
                $('.ofertas').DataTable().destroy();
                $.each(results.ofertas, function( index, value ) {
                    var cultivo = "";
                
                    if(lista_cultivos[value.cod_cultivo]){
                        cultivo = lista_cultivos[value.cod_cultivo]
                    }

                    var ubigeo = pad_with_zeroes(value.cod_region,2) + '' + pad_with_zeroes(value.cod_prov,2) + '' + pad_with_zeroes(value.cod_dist,2)
                    
                    objeto_ubigeo = lista_distritos.filter(resp => resp["iddist"] ==ubigeo)[0];

                    if(objeto_ubigeo){
                        var fecha_hoy = moment(new Date());
                        var fecha_cosecha = moment(value.fec_cosecha);
                        var dias = fecha_cosecha.diff(fecha_hoy, 'days');
                        var alerta="";
                        if(dias<=10){
                            alerta='<span class="badge badge-pill badge-danger">' + dias + ' d</span>';
                        }else if(dias<=20){
                            alerta='<span class="badge badge-pill badge-warning">' + dias + ' d</span>';
                        }else if(dias<=31){
                            alerta='<span class="badge badge-pill badge-success">' + dias + ' d</span>';
                        }

                    
                        var tonelada = ((value.can_toneladas)?value.can_toneladas:0);
                        var precio = ((value.imp_precio)?value.imp_precio:0);
                        ofertas = ofertas + "<tr>";
                            ofertas = ofertas + "<td>" + ((objeto_ubigeo.nombdep)?objeto_ubigeo.nombdep:"") + "</td>";
                            ofertas = ofertas + "<td>" + ((objeto_ubigeo.nombprov)?objeto_ubigeo.nombprov:"") + "</td>";
                            ofertas = ofertas + "<td>" + ((objeto_ubigeo.nombdist)?objeto_ubigeo.nombdist:"") + "</td>";

                            ofertas = ofertas + "<td>" + ((cultivo)?cultivo:'') + "</td>";
                            ofertas = ofertas + "<td>" + ((value.imp_precio)?number_format(value.imp_precio,2):'') + "</td>";
                            ofertas = ofertas + "<td>" + ((value.can_toneladas)?number_format(value.can_toneladas,2):'') + "</td>";
                            ofertas = ofertas + "<td>" + ((value.fecha)?value.fecha + ' ' + alerta:'') + "</td>";
                            ofertas = ofertas + "<td>";
                                ofertas = ofertas + '<div class="btn-group">';
                                ofertas = ofertas + '<button type="button" data-id="' + value.ide_oferta + '" data-region="' +  parseInt(value.cod_region) + '" class="btn btn-default btn-sm btn-modificar-oferta"><i class="fas fa-edit"></i></button>';
                                ofertas = ofertas + '<button type="button" data-id="' + value.ide_oferta + '" data-region="' +  parseInt(value.cod_region) + '" class="btn btn-default btn-sm btn-eliminar-oferta"><i class="fas fa-trash-alt"></i></button>';
                                ofertas = ofertas + '</div>';
                            ofertas = ofertas + "</td>";
                        ofertas = ofertas + "</tr>";
                    }
                    

                    
                });

                $('.ofertas tbody').html(ofertas);
               
                $('.ofertas').DataTable({
					"responsive": true,
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "pageLength" : 3,
                    "language": {
                        "sProcessing":    "Procesando...",
                        "sLengthMenu":    "Mostrar _MENU_ registros",
                        "sZeroRecords":   "No se encontraron resultados",
                        "sEmptyTable":    "Ningun dato disponible en esta lista",
                        "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":   "",
                        "sSearch":        "Buscar:",
                        "sUrl":           "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":    "Último",
                            "sNext":    "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                });
               
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}


async function Oferta(oferta_id){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/oferta/get-oferta',
        method: 'POST',
        data:{_csrf:csrf,oferta_id:oferta_id},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                //console.log(results);
                console.log("Yo debo cargar segundo");
                $('#oferta-imp_precio').val(results.oferta.imp_precio);
                $('#oferta-can_toneladas').val(results.oferta.can_toneladas);
                $('#oferta-fec_cosecha').val(results.oferta.fec_cosecha);

                $('.oferta-cod_cultivo').val(results.oferta.cod_cultivo);
                $('.oferta-cod_cultivo').selectpicker('refresh');
            }
        },
        error:function(){
            alert('No hay conectividad con el sistema');
        }
    });
}

$('body').on('click', '.btn-eliminar-oferta', function (e) {
    e.preventDefault();
    var oferta_id = $(this).attr('data-id');
    var r = confirm("¿Esta seguro de eliminar la oferta ?");
    if (r == true) {
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/oferta/delete',
            method: 'POST',
            data:{_csrf:csrf,oferta_id:oferta_id},
            dataType:'Json',
            beforeSend:function()
            {
                
            },
            success:function(results)
            {   
                if(results && results.success){
                    ListaOfertas();
                }
            },
            error:function(){
                alert('Error al realizar el proceso de eliminación.');
            }
        });
    }
});


//modificar

$('body').on('click', '.btn-modificar-oferta', function (e) {
    e.preventDefault();
    var oferta_id = $(this).attr('data-id');
    var region = $(this).attr('data-region');
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/oferta/update?oferta_id=' + oferta_id,function(){
        CultivosActualizar(region,oferta_id);
    });
    $('#modal').modal('show');
});

$('body').on('click', '.btn-grabar-oferta-pendiente', function (e) {
    e.preventDefault();
    var form = $('#formOfertaCambio');
    var formData = $('#formOfertaCambio').serializeArray();
    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert('No hay conectividad con el sistema');
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        success: function (results) {
            if(results.success){
                $('.ofertas').DataTable().destroy();
                ListaOfertas();
                $('#modal').modal('hide');
            }
        },
    });
});


/*leyenda de kilogramos*/

var legenda = L.control({position: 'bottomleft'});
legenda.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'col-md-12');
    var html = "";
    html += `<div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <dl>
                        <dt><h5>Leyenda</h5></dt>
                        <dd>
                            <p class="card-text"><div class="mayorista"></div></p>
                            <p class="card-text"><div class="minorista"></div></p>
                            <p class="card-text"><div class="supermercados"></div></p>
                            <p class="card-text"><div class="mercados_itinerantes"></div></p>
                            <p class="card-text"><div class="centros_poblados"></div></p>
                        </dd>
                    </dl>
                </div>
                <!-- /.card-body -->
            </div>
            `;
    div.innerHTML = html;

    return div;
};
legenda.addTo(map);



function number_format(amount, decimals) {
    amount += '';
    amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
    decimals = decimals || 0;
    if (isNaN(amount) || amount === 0)
            return parseFloat(0).toFixed(decimals);
    amount = '' + amount.toFixed(decimals);
    var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
    while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
    return amount_parts.join('.');
}

function pad_with_zeroes(number, length) {

    var my_string = '' + number;
    while (my_string.length < length) {
        my_string = '0' + my_string;
    }

    return my_string;

}
</script>