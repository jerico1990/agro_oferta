<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lote_cultivo".
 *
 * @property int $id
 * @property float|null $cantidad
 * @property float|null $precio
 * @property string|null $fecha_cosecha
 * @property string|null $fecha_disponible
 * @property int|null $estado
 * @property int|null $lote_id
 * @property int|null $cultivo_id
 */
class LoteCultivo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lote_cultivo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cantidad', 'precio'], 'number'],
            [['fecha_cosecha', 'fecha_disponible'], 'safe'],
            [['estado', 'lote_id', 'cultivo_id'], 'default', 'value' => null],
            [['estado', 'lote_id', 'cultivo_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cantidad' => 'Cantidad',
            'precio' => 'Precio',
            'fecha_cosecha' => 'Fecha Cosecha',
            'fecha_disponible' => 'Fecha Disponible',
            'estado' => 'Estado',
            'lote_id' => 'Lote ID',
            'cultivo_id' => 'Cultivo ID',
        ];
    }
}
