<?php

/* @var $this yii\web\View */

$this->title = 'Agro - Ofertas';
use yii\bootstrap\ActiveForm;
use developit\captcha\Captcha;
?>
<div class="row" >
    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-4"></div>
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-4">
        <?php $form = ActiveForm::begin(['action'=>\Yii::$app->request->BaseUrl.'/site/index','options' => ['id' => 'formActualizarDatos']]); ?>
        <div class="card">
            <div class="card-header">
                Agro Ofertas - Actualizar datos
            </div>
            <div class="card-body">
                <input type="hidden" name="actualizando" value="1">
                <input type="hidden" name="dni" value="<?= $dni; ?>">
                <div class="form-group">
                    <label for="">Teléfono</label>
                    <input type="text" name="telefono" id="telefono" class="form-control"  placeholder="Telefono" value="<?= $agricultor->txt_telefono ?>">
                </div>
                <div class="form-group">
                    <label for="">Celular</label>
                    <input type="text" name="celular" id="celular" class="form-control"  placeholder="Celular" value="<?= $agricultor->txt_celular ?>">
                </div>
                <div class="form-group">
                    <label for="">Correo electrónico</label>
                    <input type="text" name="correo_electronico" id="correo_electronico" class="form-control"  placeholder="Correo electrónico" value="<?= $agricultor->txt_email ?>">
                </div>
                <div class="form-group">
                    <label for="">Dirección</label>
                    <input type="text" name="direccion" id="direccion" class="form-control"  placeholder="Dirección de referencia" value="<?= $agricultor->txt_direccion ?>">
                </div>

                <div class="form-group">
                    <label for="">Centro poblado</label>
                    <input type="text" name="centro_poblado" id="centro_poblado" class="form-control"  placeholder="Centro poblado mas cercano" value="<?= $agricultor->txt_centro_poblado ?>">
                </div>
                

                <div class="form-group error" style="display:none">
                    Su correo electrónico no es una dirección válida.
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-block btn-success btn-actualizar-datos">Actualizar</button>
                </div>
            </div>
        </div>

            
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md"></div>
</div>

<script>


$('body').on('click', '.btn-actualizar-datos', function (e) {
    e.preventDefault();
    $('.error').hide();
    var form = $('#formActualizarDatos');
    var email = $('#correo_electronico').val();
    console.log(caracteresCorreoValido(email));
    if(!caracteresCorreoValido(email)){
        $('.error').show();
        return false;
    }
    form.submit();
    $(this).prop('disabled', true);
});


function caracteresCorreoValido(email){
    //var email = $(email).val();
    var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);

    if (caract.test(email) == false){
        return false;
    }else{
        return true;
    }
}



</script>