<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ofe_tcm_agricultor".
 *
 * @property int $ide_agricultor
 * @property string|null $txt_dni
 * @property string|null $txt_paterno
 * @property string|null $txt_materno
 * @property string|null $txt_telefono
 * @property string|null $txt_celular
 * @property string|null $txt_direccion
 * @property float|null $cod_ideusu
 * @property float|null $flg_pagri
 * @property string|null $txt_nombres
 * @property string|null $txt_email
 * @property string|null $fec_registro
 * @property string|null $fec_act
 */
class Agricultor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ofe_tcm_agricultor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['txt_dni', 'txt_paterno', 'txt_materno', 'txt_telefono', 'txt_celular', 'txt_direccion', 'txt_nombres', 'txt_email','txt_centro_poblado'], 'string'],
            [['cod_ideusu', 'flg_pagri'], 'number'],
            [['fec_registro', 'fec_act'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ide_agricultor' => 'Ide Agricultor',
            'txt_dni' => 'Txt Dni',
            'txt_paterno' => 'Txt Paterno',
            'txt_materno' => 'Txt Materno',
            'txt_telefono' => 'Txt Telefono',
            'txt_celular' => 'Txt Celular',
            'txt_direccion' => 'Txt Direccion',
            'cod_ideusu' => 'Cod Ideusu',
            'flg_pagri' => 'Flg Pagri',
            'txt_nombres' => 'Txt Nombres',
            'txt_email' => 'Txt Email',
            'fec_registro' => 'Fec Registro',
            'fec_act' => 'Fec Act',
        ];
    }
}
