<?php

/* @var $this yii\web\View */

$this->title = 'Agro - Ofertas';
use yii\bootstrap\ActiveForm;
use developit\captcha\Captcha;
?>
<div class="row" >
    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-4"></div>
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-4">
        <?php $form = ActiveForm::begin(); ?>
        <div class="card">
        <!--
            <div class="card-header">
                Agro Ofertas
            </div>-->
            <div class="card-body">
                <div class="form-group text-center">
                    <img src="<?= \Yii::$app->request->BaseUrl ?>/img/agroOferta.png" width="180px" alt="">
                </div>
                <div class="form-group text-justify">
                    El Ministerio de Desarrollo Agrario y Riego pone a disposición de los pequeños productores <i><b>Agro Oferta</b></i>, un sistema que permitirá mostrar todos sus productos agropecuarios ofertados, indicando la fecha de cosecha/venta, toneladas y precios de venta, georeferenciando su parcela. <br>
                    Gracias a esta plataforma de información, los compradores interesados podrán contactarse directamente con ustedes y negociar el precio de sus productos. Generando mayores ingresos para el bienestar suyo y de su familia.
                </div>
                <div class="form-group">
                    <input type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" name="dni" id="dni" class="form-control" maxlength = "8"   placeholder="N° de DNI" >
                </div>
                <!--
                <div class="form-group">
                    <div id="Captcha_Panel" style="width: unset !important; padding-left:0px" class="CSSCaptcha_Panel  ">
                        <div style="margin-top:20px; margin-bottom:10px" id="Captcha_PanelTitulo" class="CSSCaptcha_PanelTitulo">

                            <span id="lblCaptcha_Titulo" style="display:inline-block;height:25px;width:95%;text-align: center">
                                <div class="row" style="color: #293826; margin-bottom:10px">
                                    Seleccione una imagen
                                </div>
                                <span style="" id="ImgSel" class='CSSCaptcha_NombreImagen'>    </span>
                                <i style='color: #00633E; background:transparent; border-color: transparent;' class='glyphicon glyphicon-refresh btn btn-default' onclick="cargar()"> </i>

                            </span>
                            <input readonly type="text" id="res" name="res" value="*" />
                        </div><br />
                        <div id="Captcha_PanelCuerpo" class="CSSCaptcha_PanelCuerpo ">
                            <table style="margin-top: 2px;" class="CSSCaptcha_Tabla">
                                <tr>
                                    <td>
                                        <img alt="img" id="imgCaptcha01" class=" " onclick="Captcha_ActivaOpcion(1)" />
                                    </td>
                                    <td><img alt="img" id="imgCaptcha02" class="CSSCaptcha_Imagen" onclick="Captcha_ActivaOpcion(2)" /></td>
                                    <td><img alt="img" id="imgCaptcha03" class="CSSCaptcha_Imagen" onclick="Captcha_ActivaOpcion(3)" /></td>
                                    <td><img alt="img" id="imgCaptcha04" class="CSSCaptcha_Imagen" onclick="Captcha_ActivaOpcion(4)" /></td>
                                </tr>
                            </table>
                            <input class="" type="hidden" name="hfCaptcha_Seleccion" id="hfCaptcha_Seleccion" value="0" />
                            <input type="hidden" name="hfCaptcha_ticket" id="hfCaptcha_ticket" value="0" />
                        </div>

                    </div>
                </div>
                -->
                <div class="form-group">
                    <button type="submit" class="btn btn-block btn-success">Validar</button>
                    <a href="<?= \Yii::$app->request->BaseUrl ?>/visor" target="_blank" class="btn btn-block btn-primary">Consulta Ofertas disponibles</a>
                </div>
            </div>
        </div>

            
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-12"></div>
</div>
<script>
$('.numerico').keypress(function (tecla) {
    var reg = /^[0-9.\s]+$/;
    if (!reg.test(String.fromCharCode(tecla.which))) {
        return false;
    }
    return true;
});

if (window.history.replaceState) { // verificamos disponibilidad
    window.history.replaceState(null, null, window.location.href);
}




</script>