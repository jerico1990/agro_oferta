
<style>
    body {
        padding-top:0px;
    }

    th { font-size: 10px; }
    td { font-size: 10px; }

    #cultivos_kilogramos{
        font-size:10px !important;
    }

    @media (max-width: 1920px){
        

        .cultivos_kilogramos{
            width:300px;
            float:right;
        }
        
    }

    @media (max-width: 1600px){
        .logo{
            width:160px;
        }
        
        .indicadores{
            font-size:0.9rem;
            padding:0.1rem 0.5rem;
        }

        .form-control{
            font-size:0.6rem;
            height:30px;
        }
        .dropdown-item{
            padding:.3rem 1rem;
        }
        .dropdown-menu{
            font-size:0.9rem;
        }

        .dropdown-toggle{
            font-size:0.6rem;
        }
        .input-group-text{
            height:30px;
        }
       
        .col-md-12{
            padding-right:0px;
        }
        .cultivos_kilogramos{
            width:250px;
            font-size:10px !important;
            float:right;
        }
        .highcharts-xaxis-labels {
            font-weight: bold;
            font-size: 10px;
        }
        .titulo{
            font-size:18px;
        }
        table{
            font-size:10px;
            width:-1px !important;
        }
    }


    @media (max-width: 1480px){
        
    }

    @media (max-width: 1370px){
        .logo{
            width:160px;
        }
        .botones{
            width:160px !important;
        }
        .indicadores{
            font-size:0.8rem;
            padding:0.1rem 0.5rem;
        }
        .form-control{
            font-size:0.5rem;
            height:25px;
        }

        .dropdown-item{
            padding:.3rem 1rem;
        }
        .dropdown-menu{
            font-size:0.9rem;
        }

        .dropdown-toggle{
            font-size:0.5rem;
        }
        .input-group-text{
            height:25px;
        }
       
        .col-md-12{
            padding-right:0px;
        }
        .cultivos_kilogramos{
            width:250px;
            font-size:10px !important;
            float:right;
        }
        .highcharts-xaxis-labels {
            font-weight: bold;
            font-size: 8px;
        }
        .titulo{
            font-size:15px;
        }
        table{
            font-size:8px;
            width:-1px !important;
        }
    }


    @media (max-width: 1280px){
        .logo{
            width:160px;
        }
        .botones{
            width:160px !important;
        }
        .indicadores{
            font-size:0.8rem;
            padding:0.1rem 0.5rem;
        }
        .form-control{
            font-size:0.5rem;
            height:25px;
        }

        .dropdown-item{
            padding:.3rem 1rem;
        }
        .dropdown-menu{
            font-size:0.9rem;
        }

        .dropdown-toggle{
            font-size:0.5rem;
        }
        .input-group-text{
            height:25px;
        }
       
        .col-md-12{
            padding-right:0px;
        }
        .cultivos_kilogramos{
            width:250px;
            font-size:10px !important;
        }
        .highcharts-xaxis-labels {
            font-weight: bold;
            font-size: 10px;
        }
        .titulo{
            font-size:15px;
        }
        table{
            font-size:8px;
            width:-1px !important;
        }
    }

    @media (max-width: 1024px){
        .logo{
            width:160px;
        }
        .botones{
            width:160px !important;
        }
        .indicadores{
            font-size:0.6rem;
            padding:0.1rem 0.5rem;
        }
        .form-control{
            font-size:0.5rem;
            height:25px;
        }

        .dropdown-item{
            padding:.3rem 1rem;
        }
        .dropdown-menu{
            font-size:0.9rem;
        }


        .dropdown-toggle{
            font-size:0.5rem;
        }
        .input-group-text{
            height:25px;
        }
       
        .col-md-12{
            padding-right:0px;
        }
        .cultivos_kilogramos{
            width:250px;
            font-size:10px !important;
        }
        .highcharts-xaxis-labels {
            font-weight: bold;
            font-size: 10px;
        }
        .titulo{
            font-size:15px;
        }
        table{
            font-size:8px;
            width:-1px !important;
        }
    }
    
    @media (max-width: 965px){
        /*.btn{
            font-size:0.6rem;
        }*/
        .logo{
            width:160px;
        }
        .botones{
            width:160px !important;
        }
        .indicadores{
            font-size:0.5rem;
            padding:0.1rem 0.5rem;
        }
        .form-control{
            font-size:0.5rem;
            height:25px;
        }

        .dropdown-item{
            padding:.3rem 1rem;
        }
        .dropdown-menu{
            font-size:0.9rem;
        }


        .dropdown-toggle{
            font-size:0.5rem;
        }
        .input-group-text{
            height:25px;
        }
       
        .col-md-12{
            padding-right:0px;
        }
        .cultivos_kilogramos{
            width:250px;
            font-size:10px !important;
        }
        .highcharts-xaxis-labels {
            font-weight: bold;
            font-size: 6px;
        }
        .titulo{
            font-size:15px;
        }
        table{
            font-size:8px;
            width:-1px !important;
        }
    }

    @media (max-width: 725px){
        
    }



    @media (max-width: 400px){
        
    }
</style>


<div  id="map" class="map" style="height:100%;"></div>

<div id="modal" class="fade modal" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
var user = "<?= (!Yii::$app->user->isGuest)?\Yii::$app->user->identity->username:''; ?>";
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";

var map = L.map('map', {
    zoomControl: false,
    scrollWheelZoom: true,
    fullscreenControl: false,
    dragging: true,
    layers:[],
    minZoom: 3,
    maxNativeZoom: 18,
    maxZoom:18,
    zoom:6,
    center: [-9.1899672, -75.015152],
});

/* Mapa Base */

L.esri.basemapLayer('Imagery').addTo(map);

/* Logo */
var logo = L.control({position: 'topleft'});
logo.onAdd = function (map) {
    var div = L.DomUtil.create('div', ' col-4 col-sm-4 col-md-12 col-lg-12  col-xl-12 ');
    div.innerHTML = '<img class="img-responsive logo" width="240px" src="<?= \Yii::$app->request->BaseUrl ?>/img/LogoMinagri.png">';
    div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    return div;
};
logo.addTo(map);


/* Agregando filtros */
var selector_cultivo = L.control({position: 'topleft'});
selector_cultivo.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'info legend col-4 col-sm-4 col-md-12 col-lg-12  col-xl-12 ');
    div.innerHTML = '<div class="input-group botones"> <div class="input-group-prepend"> <span class="input-group-text"> <i class="icon fa-apple" aria-hidden="true"></i> </span> </div> <select style="width:1" class="form-control cultivo_id" id="cultivo_id" name="cultivo_id"><option value>Seleccionar Cultivo</option></select> </div>';
    div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    return div;
};
selector_cultivo.addTo(map);


var selector_region = L.control({position: 'topleft'});
selector_region.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'info legend col-4 col-sm-4  col-md-12 col-lg-12  col-xl-12  ');
    div.innerHTML = '<div class="input-group botones"> <div class="input-group-prepend"> <span class="input-group-text"> <i class="icon fa-globe" aria-hidden="true"></i> </span> </div> <select class="form-control" data-plugin="selectpicker" id="region" name="region_id"><option value>Seleccionar Región</option></select> </div>';
    div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    return div;
};
selector_region.addTo(map);

var selector_provincia = L.control({position: 'topleft'});
selector_provincia.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'info legend col-4 col-sm-4  col-md-12 col-lg-12  col-xl-12 ');
    div.innerHTML = '<div class="input-group botones"> <div class="input-group-prepend"> <span class="input-group-text"> <i class="icon fa-globe" aria-hidden="true"></i> </span> </div> <select class="form-control" data-plugin="selectpicker" id="provincia" name="provincia_id"><option value>Seleccionar Provincia</option></select> </div>';
    div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    return div;
};
selector_provincia.addTo(map);

var selector_distrito = L.control({position: 'topleft'});
selector_distrito.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'info legend col-4 col-sm-4  col-md-12 col-lg-12  col-xl-12 ');
    div.innerHTML = '<div class="input-group botones"> <div class="input-group-prepend"> <span class="input-group-text"> <i class="icon fa-globe" aria-hidden="true"></i> </span> </div> <select class="form-control" data-plugin="selectpicker" id="distrito" name="distrito_id"><option value>Seleccionar Distrito</option></select> </div>';
    div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    return div;
};
selector_distrito.addTo(map);

var selector_fecha_inicio = L.control({position: 'topleft'});
selector_fecha_inicio.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'info legend col-4 col-sm-4  col-md-12 col-lg-12  col-xl-12 ');
    div.innerHTML = '<div class="input-group botones"> <div class="input-group-prepend"> <span class="input-group-text"> <i class="icon wb-calendar" aria-hidden="true"></i> </span> </div> <input type="text" id="fecha_inicio" placeholder="Fecha inicio" class="form-control" data-plugin="datepicker"> </div>';
    div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    return div;
};
selector_fecha_inicio.addTo(map);

var selector_fecha_fin = L.control({position: 'topleft'});
selector_fecha_fin.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'info legend col-4 col-sm-4  col-md-12 col-lg-12  col-xl-12 ');
    div.innerHTML = '<div class="input-group botones"> <div class="input-group-prepend"> <span class="input-group-text"> <i class="icon wb-calendar" aria-hidden="true"></i> </span> </div> <input type="text" id="fecha_fin" placeholder="Fecha fin" class="form-control" data-plugin="datepicker"> </div>';
    div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    return div;
};
selector_fecha_fin.addTo(map);

var selector_capas = L.control({position: 'topleft'});
selector_capas.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'info legend col-4 col-sm-4  col-md-12 col-lg-12  col-xl-12 ');
    div.innerHTML = '<div class="input-group botones"> <div class="input-group-prepend"> <span class="input-group-text"> <i class="icon fa-globe" aria-hidden="true"></i> </span> </div> <select class="form-control selectpicker capas" id="capas" multiple name="capas" data-selected-text-format="count" title="Seleccionar Capas"></select> </div>';
    div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    return div;
};
selector_capas.addTo(map);





$('#fecha_inicio').datepicker();
$('#fecha_fin').datepicker();


var titulo_general = L.control({position: 'topcenter'});
titulo_general.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'info legend ');
    div.innerHTML = 	`
                        <h3 class="text-white text-center titulo">Agro Oferta - Visor de tus cultivos</h3>
                        <h5 class="text-white ubigeo text-center">-</h5>`;
    div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    return div;
};
titulo_general.addTo(map);

if(user){
    html_user = `<div class="btn-group" role="group">
                    <a href="<?= \Yii::$app->request->BaseUrl ?>/usuario/logout" class="btn btn-info">
                        <i class="icon fa-user text-success" aria-hidden="true"></i> <br> <span> Cerrar sesión </span>
                    </a>
                </div>`
}else{
    html_user = `<div class="btn-group" role="group">
                    <button type="button" class="btn btn-info btn-login indicadores">
                        <i class="icon fa-user text-warning " aria-hidden="true"></i> <br> <span>Iniciar sesión</span>
                    </button>
                </div>`
}


var general_indicadores = L.control({position: 'topright'});
general_indicadores.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'info legend col-md-12');
    div.innerHTML = 	`
                        <div class="btn-group">
							<div class="btn-group" role="group">
                                <button type="button" class="btn btn-primary indicadores">
                                    <i class="icon fa-shopping-basket" aria-hidden="true"></i> <span class="cantidad_ofertas"></span>
                                    <br>
                                    <span class="text-uppercase hidden-sm-down">Ofertas publicadas</span>
                                </button>
                            </div>

                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-info indicadores">
                                    <i class="icon fa-balance-scale" aria-hidden="true"></i> <span class="kilogramos_productos"></span>
                                    <br>
                                    <span class="text-uppercase hidden-sm-down">Peso total (kg)</span>
                                </button>
                            </div>

                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-success indicadores">
                                    <i class="icon fa-money" aria-hidden="true"></i> <span class="montos_productos"></span>
                                    <br>
                                    <span class="text-uppercase hidden-sm-down">Costo total (S/.)</span>
                                </button>
							</div>` +
                            html_user +
                        `</div>`;
    div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    return div;
};
general_indicadores.addTo(map);

var cuadro_cultivos_kilogramos = L.control({position: 'topright'});
cuadro_cultivos_kilogramos.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'info legend col-md-12');
    div.innerHTML = 	`<div id="cultivos_kilogramos" class="cultivos_kilogramos " style="font-size:10px !important"></div>`;
    div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    return div;
};
cuadro_cultivos_kilogramos.addTo(map);
/* Controladores de ubigeo */
var regiones = new L.esri.featureLayer({
	url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/0/',
	minZoom: 5,
	style : function (feature) {
		return { color: 'white', weight: 2 };
	},
});
regiones.addTo(map);

var provincias;
var distritos;
var region;
var provincia;

/* Cargando valores a selector*/
var boundsRegiones = L.latLngBounds([]);
regiones.on("load", function(evt) {
    var regiones_options = "<option value>Seleccionar Región</option>";
    regiones.eachFeature(function (layer) {
        regiones_options = regiones_options + "<option value='" + layer.feature.properties.iddpto + "'>" + layer.feature.properties.nombdep + "</option>";
    })
    $('#region').html(regiones_options);

    boundsRegiones = L.latLngBounds([]);
    // loop through the features returned by the server
    regiones.eachFeature(function (layer) {
        // get the bounds of an individual feature
        var layerBounds = layer.getBounds();
        // extend the bounds of the collection to fit the bounds of the new feature
        boundsRegiones.extend(layerBounds);
    });

    $('.ubigeo').html('Nivel Nacional');

    // once we've looped through all the features, zoom the map to the extent of the collection
    //map.fitBounds(boundsRegiones);
});

var bandera=0;
var iddpto;
var boundsRegion = L.latLngBounds([]);
$('body').on('change', '#region', function (e) {
    iddpto = $(this).val();
    if(bandera==0){
        bandera=1;
    }
    //
    if(GrupoOfertasGeneral != undefined){
        GrupoOfertasGeneral.clearLayers();
    }
    Ofertas();
    ListaOfertas();
    //map.setView([-9.1899672, -75.015152],6);
    map.fitBounds(boundsRegiones);
    map.removeLayer(regiones);
    if (region != undefined) {
        map.removeLayer(region);
    }
    
    if (provincias != undefined) {
        map.removeLayer(provincias);
        if(distritos){
            map.removeLayer(distritos);
        }
        

        var distritos_options = "<option value>Seleccionar Región</option>";
        $('#distrito').html(distritos_options);
        iddist='';
    }
    /* quitando tabla de agricultores */
    map.removeControl(lista_agricultores)

    region = new L.esri.featureLayer({
        url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/0/',
        where: "iddpto='" + iddpto + "'",
        style : function (feature) {
            return { color: 'white', weight: 2 };
        },
    }).addTo(map);
    
    region.once('load', function (evt) {
        // create a new empty Leaflet bounds object
        boundsRegion = L.latLngBounds([]);
        // loop through the features returned by the server
        region.eachFeature(function (layer) {
            // get the bounds of an individual feature
            var layerBounds = layer.getBounds();
            // extend the bounds of the collection to fit the bounds of the new feature
            boundsRegion.extend(layerBounds);
        });

        // once we've looped through all the features, zoom the map to the extent of the collection
        map.fitBounds(boundsRegion);
    });

    provincias = new L.esri.featureLayer({
        url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/1/',
        where: "iddpto='" + iddpto + "'",
        style : function (feature) {
            return { color: 'white', weight: 2 };
        },
    }).addTo(map);

    provincias.on("load", function(evt) {
        var provincias_options = "<option value>Seleccionar Provincia</option>";
        provincias.eachFeature(function (layer) {
            provincias_options = provincias_options + "<option value='" + layer.feature.properties.idprov + "'>" + layer.feature.properties.nombprov + "</option>";
        })
        $('#provincia').html(provincias_options);
    });

    $('.ubigeo').html('Nivel Regional');

});


var idprov;
var boundsProvincia = L.latLngBounds([]);

$('body').on('change', '#provincia', function (e) {
    idprov = $(this).val();
    
    if(GrupoOfertasGeneral != undefined){
        GrupoOfertasGeneral.clearLayers();
    }
    Ofertas();
    ListaOfertas();

    map.fitBounds(boundsRegion);
    map.removeLayer(region);
    map.removeLayer(provincias);
    if (distritos != undefined) {
        map.removeLayer(distritos);
        iddist='';
    }

    
    

    /* quitando tabla de agricultores */
    map.removeControl(lista_agricultores)
        

    provincias = new L.esri.featureLayer({
        url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/1/',
        where: "idprov='" + idprov + "'",
        style : function (feature) {
            return { color: 'white', weight: 2 };
        },
    }).addTo(map);
    
    provincias.once('load', function (evt) {
        // create a new empty Leaflet bounds object
        boundsProvincia = L.latLngBounds([]);
        // loop through the features returned by the server
        provincias.eachFeature(function (layer) {
            // get the bounds of an individual feature
            var layerBounds = layer.getBounds();
            // extend the bounds of the collection to fit the bounds of the new feature
            boundsProvincia.extend(layerBounds);
        });

        // once we've looped through all the features, zoom the map to the extent of the collection
        map.fitBounds(boundsProvincia);
    });

    distritos = new L.esri.featureLayer({
        url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/2/',
        where: "idprov='" + idprov + "'",
        style : function (feature) {
            return { color: 'white', weight: 2 };
        },
    }).addTo(map);

    distritos.on("load", function(evt) {
        var distritos_options = "<option value>Seleccionar Distritos</option>";
        distritos.eachFeature(function (layer) {
            distritos_options = distritos_options + "<option value='" + layer.feature.properties.iddist + "'>" + layer.feature.properties.nombdist + "</option>";
        })
        $('#distrito').html(distritos_options);
    });
    $('.ubigeo').html('Nivel Distrital');
});

var iddist;
$('body').on('change', '#distrito', function (e) {
    iddist = $(this).val();
    if(GrupoOfertasGeneral != undefined){
        GrupoOfertasGeneral.clearLayers();
    }
    /* quitando tabla de agricultores */
    map.removeControl(lista_agricultores);
    /* agregando tabla de agricultores */
	lista_agricultores.addTo(map);
    
    Ofertas();
    ListaOfertas();
    map.fitBounds(boundsProvincia);

    map.removeLayer(provincias);
    map.removeLayer(distritos);
	
    

    distritos = new L.esri.featureLayer({
        url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/2/',
        where: "iddist='" + iddist + "'",
        style : function (feature) {
            return { color: 'white', weight: 2 };
        },
    }).addTo(map);
    
    distritos.once('load', function (evt) {
        // create a new empty Leaflet bounds object
        var bounds = L.latLngBounds([]);
        // loop through the features returned by the server
        distritos.eachFeature(function (layer) {
            // get the bounds of an individual feature
            var layerBounds = layer.getBounds();
            // extend the bounds of the collection to fit the bounds of the new feature
            bounds.extend(layerBounds);
        });

        // once we've looped through all the features, zoom the map to the extent of the collection
        
        map.fitBounds(bounds);
        
    });
    /*
    distritos.once('load', function (evt) {
        console.log(evt.bounds);
        var bounds = L.latLngBounds([]);
        bounds.extend(evt.bounds);

        map.fitBounds(bounds);
    });*/

    $('.ubigeo').html('Nivel Provincial');
});

var cultivo_id;
$('body').on('change', '#cultivo_id', function (e) {
    cultivo_id = $(this).val();
    if(bandera==0){
        bandera=2;
    }
    Ofertas();
    ListaOfertas();
});

var fecha_inicio;
var fecha_fin;
$('body').on('change', '#fecha_inicio', function (e) {
    fecha_inicio = $(this).val();
    if(fecha_inicio && fecha_fin){
        Ofertas();
        ListaOfertas();
    }
    
});

$('body').on('change', '#fecha_fin', function (e) {
    fecha_fin = $(this).val();
    if(fecha_inicio && fecha_fin){
        Ofertas();
        ListaOfertas();
    }
});

var GeoJsonOfertas = [];
var GrupoOfertasGeneral = L.layerGroup();
var icon_oferta = L.divIcon({className: 'fas fa-map-marker text-success fa-2x'});
async function Ofertas(){
    GrupoOfertasGeneral.clearLayers();
    //"http://190.12.92.169:8081/geoserver/centro_poblado/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=centro_poblado:centro_poblado_padron_agricultores&CQL_FILTER=ubigeo='" + iddist +"'&outputFormat=application/json",
    if(iddpto || cultivo_id){

    
        var candera_cqfilter= "";

        if(bandera==1){
            candera_cqfilter = candera_cqfilter + "cod_region='" + parseInt(iddpto) +"'";
        }
        else if(bandera==2){
            candera_cqfilter = candera_cqfilter + "cod_cultivo='" + cultivo_id +"'";
        }

        if(iddpto && bandera==2){
            candera_cqfilter = candera_cqfilter + " + AND + cod_region='" + parseInt(iddpto) +"'";
        }

        if(cultivo_id && bandera==1){
            candera_cqfilter = candera_cqfilter + " + AND + cod_cultivo='" + cultivo_id +"'";
        }


        if(idprov){
            candera_cqfilter = candera_cqfilter + " + AND + cod_prov='" + parseInt(idprov.substring(2, 4)) +"'";
        }

        if(iddist){
            candera_cqfilter = candera_cqfilter + " + AND + cod_dist='" + parseInt(iddist.substring(4, 6)) +"'";
        }

        

        if(fecha_inicio && fecha_fin){
            candera_cqfilter = candera_cqfilter + " + AND + fec_cosecha + BETWEEN + '" + fecha_inicio +"' AND '" + fecha_fin +"'";
        }

        candera_cqfilter = candera_cqfilter + " + AND + fec_cosecha + >= + '" + moment().format("YYYY-MM-DD") +"'";

        /*
        urls =  [
            "http://190.12.92.169:8081/geoserver/oferta_cultivo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=oferta_cultivo:ofe_tcm_oferta&CQL_FILTER=" + candera_cqfilter + "&outputFormat=application/json",
        ];
        */
        
        urls =  [
            "http://200.48.31.150:8080/geoserver/AgroOferta/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=AgroOferta:ofe_tcm_oferta&CQL_FILTER=" + candera_cqfilter + "&outputFormat=application/json",
        ];

        let results =await Promise.all(urls.map((url) => fetch(url).then((r) => r.json())));

        let lista_ofertas = "";
        jQuery.each(results, function(i, result) {
            GeoJsonOfertas[i] = L.geoJson(result, {
                onEachFeature: function (feature, layer) {
                    layer.bindPopup('<p> Nombres y apellidos : <strong>' + feature.properties.txt_nombres_apellidos + '</strong> <br> Cultivo : <strong>' + lista_cultivos[feature.properties.cod_cultivo]  + '</strong> <br> Cantidad (Kg) : <strong>' + feature.properties.can_toneladas + '</strong> <br> Precio por Kilos : <strong> S/. ' + number_format(feature.properties.imp_precio,2) + '</strong> <br>  Fecha de cosecha : <strong>' + feature.properties.fec_cosecha + '</strong> <br> </p>');
                    //DNI : <strong>' + feature.properties.txt_dni + '</strong> <br>
                    /*
                    layer.bindPopup(Object.keys(feature.properties).map(function(k) {
                        return k + ": " + feature.properties[k];
                    }).join("<br />") , {
                        maxHeight: 200
                    });*/
                },
                pointToLayer: function (feature, latlng) {
                    if(feature.properties.can_toneladas>=0 && feature.properties.can_toneladas<5){
                        icon_oferta = L.divIcon({className: 'fas fa-map-marker text-info fa-2x'});
                    } else if(feature.properties.can_toneladas>=5 && feature.properties.can_toneladas<10){
                        icon_oferta = L.divIcon({className: 'fas fa-map-marker text-warning fa-2x'});
                    } else if(feature.properties.can_toneladas>=10){
                        icon_oferta = L.divIcon({className: 'fas fa-map-marker text-danger fa-2x'});
                    }

                    return L.marker(latlng, {
                        icon: icon_oferta,
                    });
                }
            });
            GrupoOfertasGeneral.addLayer(GeoJsonOfertas[i]);
        });
        GrupoOfertasGeneral.addTo(map);
    }
}


var cantidad_ofertas=0;
var kilogramos_productos=0;
var montos_productos=0;

var lista_agricultores = L.control({position: 'bottomright'});
lista_agricultores.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'info legend col-md-12');
    div.innerHTML = `
                    
                    <div class="panel" style="margin-bottom:0px !important">
                        <div class="panel-heading">
                            <h3 class="panel-title">Lista</h3>
                            <div class="panel-actions">
                            <a class="panel-action icon wb-refresh" data-toggle="panel-refresh" data-load-type="round-circle"
                                data-load-callback="customRefreshCallback" aria-hidden="true"></a>
                            <a class="panel-action icon wb-minus" data-toggle="panel-collapse" aria-expanded="true"
                                aria-hidden="true"></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class=" ofertas table dt-responsive nowrap" >
								<thead>
									<th data-priority="1">Agricultor</th>
									<th data-priority="2">Peso total (kg)</th>
									<th data-priority="3">Costo total (S/.)</th>
									<th data-priority="10000">Fecha de cosecha</th>
									<th data-priority="10001">Cultivo</th>
								</thead>
								<tbody>
								
								</tbody>
							</table>
                        </div>
                    </div>`;
    div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    return div;
};


Cultivos();
var lista_cultivos =new Array();
async function Cultivos(){

    urls =  [
        "http://fspreset.minagri.gob.pe:3000/cat_cultivos2",
    ];
    let results =await Promise.all(urls.map((url) => fetch(url,{method:'POST'}).then((r) => r.json())));
    var cultivo_options = "<option value>Seleccionar Cultivo</option>";
    jQuery.each(results, function(i, result) {
        
        $.map( result, function( val, i ) {
            let cod = parseInt(val.COD);
            lista_cultivos[cod]= val.NOMCUL;
            cultivo_options = cultivo_options + "<option value='" + val.COD + "'>" + val.NOMCUL + "</option>";
        });
    });
    $('.cultivo_id').html(cultivo_options);
    ListaOfertas();
    //$('.cultivo_id').selectpicker();
}

Capas();
async function Capas(){

    var capa_options = "";
    capa_options = capa_options + "<option value='0'>Mayorista</option>";
    capa_options = capa_options + "<option value='1'>Minorista</option>";
    capa_options = capa_options + "<option value='2'>Supermercados</option>";
    capa_options = capa_options + "<option value='3'>Mercados Itinerantes</option>";
    capa_options = capa_options + "<option value='4'>Área calculada por zona de predominancia distrital</option>";
    $('.capas').html(capa_options);
}

var icon_mayorista = L.icon({
    iconUrl: '<?= \Yii::$app->request->BaseUrl ?>/img/mercado_mayorista.png',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11],
});

var icon_minorista = L.icon({
    iconUrl: '<?= \Yii::$app->request->BaseUrl ?>/img/mercado_minorista.png',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11],
});

var icon_supermercados = L.icon({
    iconUrl: '<?= \Yii::$app->request->BaseUrl ?>/img/super_mercados.png',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11],
});

var icon_mercados_itinerantes = L.icon({
    iconUrl: '<?= \Yii::$app->request->BaseUrl ?>/img/mercado_itinerante.svg',
    iconSize: [27, 31],
    iconAnchor: [13.5, 17.5],
    popupAnchor: [0, -11],
});

var mayorista,minorista,supermercados,mercados_itinerantes,area_calculada_zona_predominancia_distrital;
$('#capas').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    console.log(clickedIndex, isSelected, previousValue);
    /* mayorista */
    if(clickedIndex==0 && isSelected==true){
        mayorista = new L.esri.featureLayer({
            url: 'https://swp64arcg.minagri.gob.pe/server/rest/services/AmbitosAdministrativosAgrarios/MercadosAbastecimiento/MapServer/1/',
            //where: "idprov='" + idprov + "'",
            style : function (feature) {
                //return { color: 'white', weight: 2 };
            },
            pointToLayer: function (geojson, latlng) {
                return L.marker(latlng, {
                    icon: icon_mayorista
                });
            }
        }).addTo(map);

        mayorista.bindPopup(function (layer) {
            return L.Util.template('<p>NOMBDEP: <strong>{NOMBDEP}</strong> <br> NOMBPROV: <strong>{NOMBPROV}</strong> <br> NOMBDIST: <strong>{NOMBDIST} </strong> <br> LOCAL: <strong>{LOCAL} </strong><br> NOMBRE: <strong>{NOMBRE} </strong> <br> DIRECCION: <strong>{DIRECCION} </strong>  </p>', layer.feature.properties);
        });

        $('.mayorista').html('<p class="card-text"><img style="vertical-align:initial" width="15px" src="<?= \Yii::$app->request->BaseUrl ?>/img/mercado_mayorista.png"></img> M. mayorista</p>');

    }else if(clickedIndex==0 && isSelected==false){
        map.removeLayer(mayorista);
        $('.mayorista').html('');
    }

    /* minorista */
    if(clickedIndex==1 && isSelected==true){
        minorista = new L.esri.featureLayer({
            url: 'https://swp64arcg.minagri.gob.pe/server/rest/services/AmbitosAdministrativosAgrarios/MercadosAbastecimiento/MapServer/2/',
            //where: "idprov='" + idprov + "'",
            style : function (feature) {
                //return { color: 'white', weight: 2 };
            },
            pointToLayer: function (geojson, latlng) {
                return L.marker(latlng, {
                    icon: icon_minorista
                });
            }
        }).addTo(map);
        minorista.bindPopup(function (layer) {
            return L.Util.template('<p>NOMBDEP: <strong>{NOMBDEP}</strong> <br> NOMBPROV: <strong>{NOMBPROV}</strong> <br> NOMBDIST: <strong>{NOMBDIST} </strong> <br> LOCAL: <strong>{LOCAL} </strong> <br> NOMBRE: <strong>{NOMBRE} </strong> <br> DIRECCION: <strong>{DIRECCION} </strong>  </p>', layer.feature.properties);
        });
        $('.minorista').html('<p class="card-text"><img style="vertical-align:initial" width="15px" src="<?= \Yii::$app->request->BaseUrl ?>/img/mercado_minorista.png"></img> M. minorista</p>');

    }else if(clickedIndex==1 && isSelected==false){
        map.removeLayer(minorista);
        $('.minorista').html('');
    }

    /* supermercados */
    if(clickedIndex==2 && isSelected==true){
        supermercados = new L.esri.featureLayer({
            url: 'https://swp64arcg.minagri.gob.pe/server/rest/services/AmbitosAdministrativosAgrarios/MercadosAbastecimiento/MapServer/3/',
            //where: "idprov='" + idprov + "'",
            style : function (feature) {
                //return { color: 'white', weight: 2 };
            },
            pointToLayer: function (geojson, latlng) {
                return L.marker(latlng, {
                    icon: icon_supermercados
                });
            }
        }).addTo(map);
        supermercados.bindPopup(function (layer) {
            return L.Util.template('<p>NOMBDEP: <strong>{NOMBDEP}</strong> <br> NOMBPROV: <strong>{NOMBPROV}</strong> <br> NOMBDIST: <strong>{NOMBDIST} </strong> <br> LOCAL: <strong>{LOCAL}</strong> <br> NOMBRE: <strong>{NOMBRE} </strong> <br> DIRECCION: <strong>{DIRECCION} </strong>  </p>', layer.feature.properties);
        });
        $('.supermercados').html('<p class="card-text"><img style="vertical-align:initial" width="15px" src="<?= \Yii::$app->request->BaseUrl ?>/img/super_mercados.png"></img> Super mercados</p>');
    }else if(clickedIndex==2 && isSelected==false){
        map.removeLayer(supermercados);
        $('.supermercados').html('');
    }

    /* mercados_itinerantes */
    if(clickedIndex==3 && isSelected==true){
        mercados_itinerantes = new L.esri.featureLayer({
            url: 'https://swp64arcg.minagri.gob.pe/server/rest/services/AmbitosAdministrativosAgrarios/MercadosAbastecimiento/MapServer/4/',
            //where: "idprov='" + idprov + "'",
            style : function (feature) {
                //return { color: 'white', weight: 2 };
            },
            pointToLayer: function (geojson, latlng) {
                return L.marker(latlng, {
                    icon: icon_mercados_itinerantes
                });
            }
        }).addTo(map);

        mercados_itinerantes.bindPopup(function (layer) {
            return L.Util.template('<p>NOMBDEP: <strong>{NOMBDEP}</strong> <br> NOMBPROV: <strong>{NOMBPROV}</strong> <br> NOMBDIST: <strong>{NOMBDIST} </strong> <br> CAPITAL: <strong>{CAPITAL}</strong> <br> LUGAR: <strong>{LUGAR} </strong>  </p>', layer.feature.properties);
        });
        $('.mercados_itinerantes').html('<p class="card-text"><img style="vertical-align:initial" width="15px" src="<?= \Yii::$app->request->BaseUrl ?>/img/mercado_itinerante.svg"></img> M. Itinerantes</p>');

    }else if(clickedIndex==3 && isSelected==false){
        map.removeLayer(mercados_itinerantes);
        $('.mercados_itinerantes').html('');
    }


    /* Área calculada por zona de predominancia distrital */
    if(clickedIndex==4 && isSelected==true){
        area_calculada_zona_predominancia_distrital = new L.esri.featureLayer({
            url: 'http://swp64arcg.minagri.gob.pe/server/rest/services/Geosiea/SectoresEstad%C3%ADsticos2018_2019/MapServer/0/',
            style : function (feature) {
                return { color: 'blue', weight: 2 };
            }
        }).addTo(map);
    }else if(clickedIndex==4 && isSelected==false){
        
    }

    

});


async function ListaOfertas(){
    data_cultivos_grafico = new Array();

    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/oferta/get-lista-ofertas',
        method: 'POST',
        data:{_csrf:csrf,region_id:iddpto,provincia_id:idprov,distrito_id:iddist,cultivo_id:cultivo_id,fecha_inicio:fecha_inicio,fecha_fin:fecha_fin},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                cantidad_ofertas = 0 ;
                kilogramos_productos = 0 ;
                montos_productos = 0 ;
                var ofertas ="";
                $('.ofertas').DataTable().destroy();
                $.each(results.ofertas, function( index, value ) {
                    var cultivo = "";
                    if(lista_cultivos[value.cod_cultivo]){
                        cultivo = lista_cultivos[value.cod_cultivo]
                    }
                    

                    var fecha_hoy = moment(new Date());
                    var fecha_cosecha = moment(value.fec_cosecha);
                    var dias = fecha_cosecha.diff(fecha_hoy, 'days');
                    var alerta="";
                    if(dias<=10){
                        alerta='<span class="badge badge-pill badge-danger">' + dias + ' d</span>';
                    }else if(dias<=20){
                        alerta='<span class="badge badge-pill badge-warning">' + dias + ' d</span>';
                    }else if(dias<=31){
                        alerta='<span class="badge badge-pill badge-success">' + dias + ' d</span>';
                    }

                   

                    if (cultivo in data_cultivos_grafico){
                        data_cultivos_grafico[cultivo] = parseFloat(data_cultivos_grafico[cultivo]) + parseFloat(value.can_toneladas);
                    }else{
                        data_cultivos_grafico[cultivo] = parseFloat(value.can_toneladas);
                    }

                    

                    
                    

                    var tonelada = ((value.can_toneladas)?value.can_toneladas:0);
                    var precio = ((value.imp_precio)?value.imp_precio:0);
                    ofertas = ofertas + "<tr>";
                        ofertas = ofertas + "<td>" + ((value.txt_nombres_apellidos)?value.txt_nombres_apellidos:'') + "</td>";
                        ofertas = ofertas + "<td>" + ((value.can_toneladas)?number_format(value.can_toneladas,2):'') + "</td>";
                        ofertas = ofertas + "<td>" + ((value.imp_precio)?number_format(value.imp_precio,2):'') + "</td>";
                        ofertas = ofertas + "<td>" + ((value.fecha)?value.fecha + ' ' + alerta:'') + "</td>";
                        ofertas = ofertas + "<td>" + ((cultivo)?cultivo:'') + "</td>";
                    ofertas = ofertas + "</tr>";
                    kilogramos_productos = kilogramos_productos + parseFloat(tonelada);
                    montos_productos = montos_productos + parseFloat(precio);

                    cantidad_ofertas++;
                });

                /*
                console.log(data_cultivos_kilogramos_x);
                
                cultivos_kilogramos.load({
                    columns:
                        [Object.entries(data_cultivos_kilogramos_x),
                        Object.entries(data_pie_cultivo_peso)]
                    
                });*/
                graficoBarras(Object.entries(data_cultivos_grafico));
                console.log(Object.entries(data_cultivos_grafico));
                $('.ofertas tbody').html(ofertas);
                $('.cantidad_ofertas').html(number_format(cantidad_ofertas,0));
                $('.kilogramos_productos').html(number_format(kilogramos_productos,2));
                $('.montos_productos').html(number_format(montos_productos,2));
               
                $('.ofertas').DataTable({
					"responsive": true,
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "pageLength" : 3,
                    "language": {
                        "sProcessing":    "Procesando...",
                        "sLengthMenu":    "Mostrar _MENU_ registros",
                        "sZeroRecords":   "No se encontraron resultados",
                        "sEmptyTable":    "Ningun dato disponible en esta lista",
                        "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":   "",
                        "sSearch":        "Buscar:",
                        "sUrl":           "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":    "Último",
                            "sNext":    "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                });
               
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
}
var chart;
function graficoBarras(data){
    chart = Highcharts.chart('cultivos_kilogramos', {
        chart: {
            style: {
                fontSize: '0.9em'
            },
            type: 'column',
            height:  '85%'
        },
        title: {
            text: 'Cultivo por Kilogramos',
            style: {
                fontSize: '0.9em'
            }
        },
        subtitle: {
            text: 'Fuente: MINAGRI'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '0.9em',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            labels: {
                style: {
                    fontSize: '0.9em'
                }
            },
            min: 0,
            title: {
                text: 'Kilogramos'
            }
        },
        legend: {
            itemStyle: {
                fontSize: '0.9em'
            },
            enabled: false
        },
        tooltip: {
            pointFormat: 'Kilogramos: <b>{point.y:.1f}</b>',
            style: {
                fontSize: '0.9em'
            }
        },
        series: [{
            name: 'Population',
            data: data,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}


function number_format(amount, decimals) {
    amount += '';
    amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
    decimals = decimals || 0;
    if (isNaN(amount) || amount === 0)
            return parseFloat(0).toFixed(decimals);
    amount = '' + amount.toFixed(decimals);
    var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
    while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
    return amount_parts.join('.');
}


/*leyenda de kilogramos*/

var legend_kilogramos = L.control({position: 'bottomleft'});
legend_kilogramos.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'col-md-12');
    var html = "";
    html += `<div class="card">
				<div class="card-block">
					<h4 class="card-title">Leyenda</h4>
					<p class="card-text"><span class='fas fa-map-marker text-info fa-1x'></span> 1 a 5 kilogramos</p>
					<p class="card-text"><span class='fas fa-map-marker text-warning fa-1x'></span> 5 a 10 kilogramos</p>
					<p class="card-text"><span class='fas fa-map-marker text-danger fa-1x'></span> 10 a mas kilogramos</p>
                    <div class="mayorista"></div>
                    <div class="minorista"></div>
                    <div class="supermercados"></div>
                    <div class="mercados_itinerantes"></div>
				</div>
			</div>`;
    div.innerHTML = html;

    return div;
};
legend_kilogramos.addTo(map);

/* Login */

//agregar

$('body').on('click', '.btn-login', function (e) {
    e.preventDefault();
    $('#modal').modal({backdrop: 'static', keyboard: false})

    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/usuario/login');
    $('#modal').modal('show');
});

$('body').on('click', '.btn-iniciar-sesion', function (e) {
    e.preventDefault();
    var form = $('#formLogin');
    var formData = $('#formLogin').serializeArray();

    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        success: function (results) {
            if(results.success){
                location.reload();
            }else{
                $('.alerta_login').html(`<div class="toast-example" aria-live="polite" role="alert">
                                            <div class="toast toast-just-text toast-danger">
                                                <button type="button" class="toast-close-button" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                                </button>
                                                <div class="toast-message">Alerta</div>
                                                <div class="toast-message">El usuario o contraseña es incorrecto.</div>
                                            </div>
                                        </div>`);
            }
        },
    });
});

function pad_with_zeroes(number, length) {

    var my_string = '' + number;
    while (my_string.length < length) {
        my_string = '0' + my_string;
    }

    return my_string;

}

Highcharts.setOptions({
    chart: {
        style: {
            fontSize: '11px'
        }
    }
});


</script>