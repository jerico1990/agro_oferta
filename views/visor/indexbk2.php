<?php

/* @var $this yii\web\View */

$this->title = 'Agro - Ofertas';
use yii\bootstrap\ActiveForm;
?>
<style>
.container{
    max-width:100%;
}
.jumbotron{
    padding:2rem 1rem;
}
</style>
<div class="site-index">
        <div class="col-md-12 col-lg-12" style="padding-right:2px;padding-left:2px">
            <div class="card">
                <div class="card-header">
                     <a class="navbar-brand" href="#"><img src="<?= \Yii::$app->request->BaseUrl ?>/img/LogoMinagri.png" width="200" height="30" alt=""></a> Agro Oferta - Visor de tus cultivos
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="region">Región</label>
                                        <select class="form-control" id="region" name="region_id">
                                            <option value>Seleccionar región</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="provincia">Provincia</label>
                                        <select class="form-control" id="provincia" name="provincia_id">
                                            <option value>Seleccionar provincia</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="distrito">Distrito</label>
                                        <select class="form-control" id="distrito" name="distrito_id">
                                            <option value>Seleccionar distrito</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="cultivo">Cultivo</label>
                                        <select class="form-control cultivo_id" id="cultivo_id" name="cultivo_id">
                                            <option value>Seleccionar cultivo</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="cultivo">Fecha inicio</label>
                                        <input type="date" id="fecha_inicio" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="cultivo">Fecha fin</label>
                                        <input type="date" id="fecha_fin" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <div class="text-uppercase text-center">Cultivo vs Tonelada</div>
                                        <div id="chart2"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-12">
                                    <div  id="map" class="map sidebar-map" style="height:100%;width:100%;height:500px"></div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class=" ofertas table table-striped table-bordered dt-responsive nowrap" style="width:100%;font-size:1rem">
                                        <thead>
                                            <th>Agricultor</th>
                                            <th>Cantidad (tn)</th>
                                            <th>Precio total (S/.)</th>
                                            <th>Fecha de cosecha</th>
                                            <th>Cultivo</th>
                                        </thead>
                                        <tbody>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card">
                                <div class="card-body">
                                    <div class="jumbotron jumbotron-fluid bg-light">
                                        <div class="container">
                                            <h5>Ofertas publicadas</h5>
                                            <h4 class=" cantidad_ofertas"></h4>
                                        </div>
                                    </div>
                                    <div class="jumbotron jumbotron-fluid bg-light">
                                        <div class="container">
                                            <h5>Cantidad total (tn)</h5>
                                            <h4 class=" toneladas_productos"></h4>
                                        </div>
                                    </div>
                                    <div class="jumbotron jumbotron-fluid bg-light">
                                        <div class="container">
                                            <h5>Monto total (S/.)</h5>
                                            <h4 class=" montos_productos"></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Understood</button>
            </div>
        </div>
    </div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var map = L.map('map', {
    zoomControl: false,
    scrollWheelZoom: true,
    fullscreenControl: false,
    dragging: true,
    layers:[],
    minZoom: 3,
    maxNativeZoom: 18,
    maxZoom:18,
    zoom:5,
    center: [-9.1899672, -75.015152],
});

/* Mapa Base */

L.esri.basemapLayer('Imagery').addTo(map);

var regiones = new L.esri.featureLayer({
            url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/0/',
            minZoom: 5,
            style : function (feature) {
                return { color: 'white', weight: 2 };
            },
        });

regiones.addTo(map);

var provincias;
var distritos;
var region;
var provincia;

/* Cargando valores a selector*/
var boundsRegiones = L.latLngBounds([]);
regiones.on("load", function(evt) {
    var regiones_options = "<option value>Seleccionar</option>";
    regiones.eachFeature(function (layer) {
        regiones_options = regiones_options + "<option value='" + layer.feature.properties.iddpto + "'>" + layer.feature.properties.nombdep + "</option>";
    })
    $('#region').html(regiones_options);

    boundsRegiones = L.latLngBounds([]);
    // loop through the features returned by the server
    regiones.eachFeature(function (layer) {
        // get the bounds of an individual feature
        var layerBounds = layer.getBounds();
        // extend the bounds of the collection to fit the bounds of the new feature
        boundsRegiones.extend(layerBounds);
    });

    // once we've looped through all the features, zoom the map to the extent of the collection
    //map.fitBounds(boundsRegiones);

});

var iddpto;
var boundsRegion = L.latLngBounds([]);
$('body').on('change', '#region', function (e) {
    iddpto = $(this).val();
    //Ofertas();
    ListaOfertas();
    //map.setView([-9.1899672, -75.015152],6);
    map.fitBounds(boundsRegiones);
    map.removeLayer(regiones);
    if (region != undefined) {
        map.removeLayer(region);
    }
    /*
    if(region){
        map.removeLayer(region);
    }*/
    
    if (provincias != undefined) {
        map.removeLayer(provincias);
        if(distritos){
            map.removeLayer(distritos);
        }
        

        var distritos_options = "<option value>Seleccionar</option>";
        $('#distrito').html(distritos_options);
        iddist='';
    }
    /*
    if(provincias){
        map.removeLayer(provincias);
    }

    if(distritos){
        map.removeLayer(distritos);
    }*/

    
    

    region = new L.esri.featureLayer({
        url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/0/',
        where: "iddpto='" + iddpto + "'",
        style : function (feature) {
            return { color: 'white', weight: 2 };
        },
    }).addTo(map);
    
    region.once('load', function (evt) {
        // create a new empty Leaflet bounds object
        boundsRegion = L.latLngBounds([]);
        // loop through the features returned by the server
        region.eachFeature(function (layer) {
            // get the bounds of an individual feature
            var layerBounds = layer.getBounds();
            // extend the bounds of the collection to fit the bounds of the new feature
            boundsRegion.extend(layerBounds);
        });

        // once we've looped through all the features, zoom the map to the extent of the collection
        map.fitBounds(boundsRegion);
    });

    provincias = new L.esri.featureLayer({
        url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/1/',
        where: "iddpto='" + iddpto + "'",
        style : function (feature) {
            return { color: 'white', weight: 2 };
        },
    }).addTo(map);

    provincias.on("load", function(evt) {
        var provincias_options = "<option value>Seleccionar</option>";
        provincias.eachFeature(function (layer) {
            provincias_options = provincias_options + "<option value='" + layer.feature.properties.idprov + "'>" + layer.feature.properties.nombprov + "</option>";
        })
        $('#provincia').html(provincias_options);
    });

});

var idprov;
var boundsProvincia = L.latLngBounds([]);

$('body').on('change', '#provincia', function (e) {
    idprov = $(this).val();
    //Ofertas();
    ListaOfertas();

    map.fitBounds(boundsRegion);
    map.removeLayer(region);
    map.removeLayer(provincias);
    if (distritos != undefined) {
        map.removeLayer(distritos);
        iddist='';
    }
        

    provincias = new L.esri.featureLayer({
        url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/1/',
        where: "idprov='" + idprov + "'",
        style : function (feature) {
            return { color: 'white', weight: 2 };
        },
    }).addTo(map);
    
    provincias.once('load', function (evt) {
        // create a new empty Leaflet bounds object
        boundsProvincia = L.latLngBounds([]);
        // loop through the features returned by the server
        provincias.eachFeature(function (layer) {
            // get the bounds of an individual feature
            var layerBounds = layer.getBounds();
            // extend the bounds of the collection to fit the bounds of the new feature
            boundsProvincia.extend(layerBounds);
        });

        // once we've looped through all the features, zoom the map to the extent of the collection
        map.fitBounds(boundsProvincia);
    });

    distritos = new L.esri.featureLayer({
        url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/2/',
        where: "idprov='" + idprov + "'",
        style : function (feature) {
            return { color: 'white', weight: 2 };
        },
    }).addTo(map);

    distritos.on("load", function(evt) {
        var distritos_options = "<option value>Seleccionar</option>";
        distritos.eachFeature(function (layer) {
            distritos_options = distritos_options + "<option value='" + layer.feature.properties.iddist + "'>" + layer.feature.properties.nombdist + "</option>";
        })
        $('#distrito').html(distritos_options);
    });
});

var iddist;
$('body').on('change', '#distrito', function (e) {
    iddist = $(this).val();
    Ofertas();
    ListaOfertas();
    map.fitBounds(boundsProvincia);

    map.removeLayer(provincias);
    map.removeLayer(distritos);
 

    distritos = new L.esri.featureLayer({
        url: 'http://georural.minagri.gob.pe/geoservicios/rest/services/public/Demarcacion_Territorial/MapServer/2/',
        where: "iddist='" + iddist + "'",
        style : function (feature) {
            return { color: 'white', weight: 2 };
        },
    }).addTo(map);
    
    distritos.once('load', function (evt) {
        // create a new empty Leaflet bounds object
        var bounds = L.latLngBounds([]);
        // loop through the features returned by the server
        distritos.eachFeature(function (layer) {
            // get the bounds of an individual feature
            var layerBounds = layer.getBounds();
            // extend the bounds of the collection to fit the bounds of the new feature
            bounds.extend(layerBounds);
        });

        // once we've looped through all the features, zoom the map to the extent of the collection
        
        map.fitBounds(bounds);
        
    });
    /*
    distritos.once('load', function (evt) {
        console.log(evt.bounds);
        var bounds = L.latLngBounds([]);
        bounds.extend(evt.bounds);

        map.fitBounds(bounds);
    });*/
});

var cultivo_id;
$('body').on('change', '#cultivo_id', function (e) {
    cultivo_id = $(this).val();
    Ofertas();
    ListaOfertas();
});

var fecha_inicio;
var fecha_fin;
$('body').on('change', '#fecha_inicio', function (e) {
    fecha_inicio = $(this).val();
    if(fecha_inicio && fecha_fin){
        Ofertas();
        ListaOfertas();
    }
    
});

$('body').on('change', '#fecha_fin', function (e) {
    fecha_fin = $(this).val();
    if(fecha_inicio && fecha_fin){
        Ofertas();
        ListaOfertas();
    }
});

var GeoJsonOfertas = [];
var GrupoOfertasGeneral = L.layerGroup();
var icon_oferta = L.divIcon({className: 'fas fa-map-marker text-success fa-2x'});
async function Ofertas(){
    GrupoOfertasGeneral.clearLayers();
    //"http://190.12.92.169:8081/geoserver/centro_poblado/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=centro_poblado:centro_poblado_padron_agricultores&CQL_FILTER=ubigeo='" + iddist +"'&outputFormat=application/json",

    console.log(iddist,idprov.substring(2, 4),iddist.substring(4, 6));
    var candera_cqfilter= "";
    if(iddpto){
        candera_cqfilter = candera_cqfilter + "cod_region='" + parseInt(iddpto) +"'";
    }

    if(idprov){
        candera_cqfilter = candera_cqfilter + " + AND + cod_prov='" + parseInt(idprov.substring(2, 4)) +"'";
    }

    if(iddist){
        candera_cqfilter = candera_cqfilter + " + AND + cod_dist='" + parseInt(iddist.substring(4, 6)) +"'";
    }

    if(cultivo_id){
        candera_cqfilter = candera_cqfilter + " + AND + cod_cultivo='" + cultivo_id +"'";
    }

    if(fecha_inicio && fecha_fin){
        candera_cqfilter = candera_cqfilter + " + AND + fec_cosecha + BETWEEN + '" + fecha_inicio +"' AND '" + fecha_fin +"'";
    }

    candera_cqfilter = candera_cqfilter + " + AND + fec_cosecha + >= + '" + moment().format("YYYY-MM-DD") +"'";

    
    urls =  [
        "http://190.12.92.169:8081/geoserver/oferta_cultivo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=oferta_cultivo:ofe_tcm_oferta&CQL_FILTER=" + candera_cqfilter + "&outputFormat=application/json",
    ];
    console.log(urls)
    /*
    if(iddist && cultivo_id){
        urls =  [
            "http://190.12.92.169:8081/geoserver/oferta_cultivo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=oferta_cultivo:oferta&CQL_FILTER=distrito_id=" + candera_cqfilter + "&outputFormat=application/json",
        ];
    }*/

    let results =await Promise.all(urls.map((url) => fetch(url).then((r) => r.json())));

    let lista_ofertas = "";
    jQuery.each(results, function(i, result) {
        GeoJsonOfertas[i] = L.geoJson(result, {
            onEachFeature: function (feature, layer) {

                layer.bindPopup(Object.keys(feature.properties).map(function(k) {
                    return k + ": " + feature.properties[k];
                }).join("<br />") , {
                    maxHeight: 200
                });
            },
            pointToLayer: function (feature, latlng) {
                if(feature.properties.cant_toneladas>=0 && feature.properties.cant_toneladas<5){
                    icon_oferta = L.divIcon({className: 'fas fa-map-marker text-secondary fa-2x'});
                } else if(feature.properties.cant_toneladas>=5 && feature.properties.cant_toneladas<10){
                    icon_oferta = L.divIcon({className: 'fas fa-map-marker text-warning fa-2x'});
                } else if(feature.properties.cant_toneladas>=10 && feature.properties.cant_toneladas<=500){
                    icon_oferta = L.divIcon({className: 'fas fa-map-marker text-info fa-2x'});
                }

                return L.marker(latlng, {
                    icon: icon_oferta,
                });
            }
        });
        GrupoOfertasGeneral.addLayer(GeoJsonOfertas[i]);
    });
    GrupoOfertasGeneral.addTo(map);
}


var cantidad_ofertas=0;
var toneladas_productos=0;
var montos_productos=0;
var data_pie_cultivo_peso = {"Maíz":0};
ListaOfertas();
function ListaOfertas(){
    data_pie_cultivo_peso = {"Maíz":0};
    $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/oferta/get-lista-ofertas',
        method: 'POST',
        data:{_csrf:csrf,region_id:iddpto,provincia_id:idprov,distrito_id:iddist,cultivo_id:cultivo_id,fecha_inicio:fecha_inicio,fecha_fin:fecha_fin},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                cantidad_ofertas = 0 ;
                toneladas_productos = 0 ;
                montos_productos = 0 ;
                
               
                var ofertas ="";
                $('.ofertas').DataTable().destroy();
                $.each(results.ofertas, function( index, value ) {
                    var cultivo = "";
                    if(value.cod_cultivo=="1"){
                        cultivo="Fresa";
                    }else if(value.cod_cultivo=="2"){
                        cultivo="Maíz";
                    }
                    var fecha_hoy = moment(new Date());
                    var fecha_cosecha = moment(value.fec_cosecha);
                    var dias = fecha_cosecha.diff(fecha_hoy, 'days');
                    var alerta="";
                    if(dias<=10){
                        alerta='<span class="badge badge-pill badge-danger">' + dias + ' d</span>';
                    }else if(dias<=20){
                        alerta='<span class="badge badge-pill badge-warning">' + dias + ' d</span>';
                    }else if(dias<=31){
                        alerta='<span class="badge badge-pill badge-success">' + dias + ' d</span>';
                    }

                    /*
                    if(data_pie_cultivo_peso[cultivo]==undefined){
                        data_pie_cultivo_peso.push({(value.cultivo_id):value.cantidad});
                    }*/

                    if (cultivo in data_pie_cultivo_peso){
                        data_pie_cultivo_peso[cultivo] = parseFloat(data_pie_cultivo_peso[cultivo]) + parseFloat(value.can_toneladas);
                    }else{
                        data_pie_cultivo_peso[cultivo] = parseFloat(value.can_toneladas);
                    }

                    
                    

                    var tonelada = ((value.can_toneladas)?value.can_toneladas:0);
                    var precio = ((value.imp_precio)?value.imp_precio:0);
                    ofertas = ofertas + "<tr>";
                        ofertas = ofertas + "<td>" + ((value.txt_nombres_apellidos)?value.txt_nombres_apellidos:'') + "</td>";
                        ofertas = ofertas + "<td>" + ((value.can_toneladas)?number_format(value.can_toneladas,2):'') + "</td>";
                        ofertas = ofertas + "<td>" + ((value.imp_precio)?number_format(value.imp_precio,2):'') + "</td>";
                        ofertas = ofertas + "<td>" + ((value.fecha)?value.fecha + ' ' + alerta:'') + "</td>";
                        ofertas = ofertas + "<td>" + ((cultivo)?cultivo:'') + "</td>";
                    ofertas = ofertas + "</tr>";
                    toneladas_productos = toneladas_productos + parseFloat(tonelada);
                    montos_productos = montos_productos + parseFloat(precio);

                    cantidad_ofertas++;
                });
                
                chart2.load({
                    columns: 
                        Object.entries(data_pie_cultivo_peso)
                    
                });

                $('.ofertas tbody').html(ofertas);
                $('.cantidad_ofertas').html(number_format(cantidad_ofertas,2));
                $('.toneladas_productos').html(number_format(toneladas_productos,2));
                $('.montos_productos').html(number_format(montos_productos,2));
               
                $('.ofertas').DataTable({
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "pageLength" : 7,
                    "language": {
                        "sProcessing":    "Procesando...",
                        "sLengthMenu":    "Mostrar _MENU_ registros",
                        "sZeroRecords":   "No se encontraron resultados",
                        "sEmptyTable":    "Ningun dato disponible en esta lista",
                        "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":   "",
                        "sSearch":        "Buscar:",
                        "sUrl":           "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":    "Último",
                            "sNext":    "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                });
               
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
}

Cultivos();
async function Cultivos(){

    urls =  [
        "http://fspreset.minagri.gob.pe:3000/cat_cultivos",
    ];
    let results =await Promise.all(urls.map((url) => fetch(url,{method:'POST'}).then((r) => r.json())));
    var cultivo_options = "<option value>Seleccionar Cultivo</option>";
    jQuery.each(results, function(i, result) {
        $.map( result, function( val, i ) {
            console.log(val);
            cultivo_options = cultivo_options + "<option value='" + val.COD + "'>" + val.NOMCUL + "</option>";
        });
    });
    $('.cultivo_id').html(cultivo_options);
    $('.cultivo_id').selectpicker();
}



function number_format(amount, decimals) {
    amount += '';
    amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
    decimals = decimals || 0;
    if (isNaN(amount) || amount === 0)
            return parseFloat(0).toFixed(decimals);
    amount = '' + amount.toFixed(decimals);
    var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
    while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
    return amount_parts.join('.');
}


/*leyenda de bancos*/

var legend_bancos = L.control({position: 'bottomleft'});
legend_bancos.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'info legend');
    var html = "";
    html += "<table class='table' style='background:white' >";
        html += "<thead>";
            html += "<th><img width='150px' src='<?= \Yii::$app->request->BaseUrl ?>/img/LogoMinagri.png'></th>";
        html += "</thead>";
        html+= "<tbody>";
            html += "<tr>";
                html += "<td> <span class='fas fa-map-marker text-secondary fa-2x'></span> 1 a 5 toneladas <br> <span class='fas fa-map-marker text-warning fa-2x'></span> 5 a 10 toneladas <br> <span class='fas fa-map-marker text-info fa-2x'></span> 10 a mas toneladas </td>";
            html += "</tr>";
        html += "</tbody>";
    html += "</table>";
    div.innerHTML = html;

    return div;
};
legend_bancos.addTo(map);


var chart2 = c3.generate({
    bindto: '#chart2',
    data: {
        // iris data from R
        columns: [
        ],
        type : 'pie',
        //onclick: function (d, i) { console.log("onclick", d, i); },
        //onmouseover: function (d, i) { console.log("onmouseover", d, i); },
        //onmouseout: function (d, i) { console.log("onmouseout", d, i); }
    },
    pie: {
        label: {
            format: function (value, ratio, id) {
                return value;
            }
        }
    },
    legend: {
        show: false
    }
});

/*
Demo();
function Demo(){
    $.ajax({
        url:'http://ofi5.mef.gob.pe/inviertePub/ConsultaPublica/traeListaProyectoConsultaAvanzada',
        method: 'POST',
        data:{"filters":"","ip":"","cboNom":"1","txtNom":"","cboDpto":"0","cboProv":"0","cboDist":"0","optUf":"*","cboGNSect":"*","cboGNPlie":"","cboGNUF":"","cboGR":"*","cboGRUf":"","optGL":"","cboGLDpto":"*","cboGLProv":"*","cboGLDist":"*","cboGLUf":"","cboGLManPlie":"*","cboGLManUf":"","cboSitu":"*","cboNivReqViab":"*","cboEstu":"*","cboEsta":"*","optFecha":"*","txtIni":"","txtFin":"","chkMonto":false,"txtMin":"","txtMax":"","tipo":"1","cboFunc":"0","chkInactivo":"0","cboDivision":"0","cboGrupo":"0","rbtnCadena":"T","isSearch":false,"PageSize":1000,"PageIndex":1,"sortField":"MontoAlternativa","sortOrder":"desc","chkFoniprel":""},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            console.log(results);
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
}*/
</script>