<div class="page-header">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Oferta</a></li>
        <li class="breadcrumb-item active">Lista de modificaciones</li>
    </ol>
</div>

<!-- /.content-header -->
<style>
.dataTables_filter {display: none;}

</style>
<!-- Main content -->
<div class="content psi-content">

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="card">
                    
                    <div class="card-header">
                        <div class="row">
                            <div class="col"><h3 class="card-title">Listado de ofertas por publicar</h3></div>
                            <div class="col-md-3 col-sm-12 d-flex align-items-center justify-content-md-end justify-content-sm-center">

                            </div>
                        </div>

                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        
                        <div class="table-responsive col-sm-12 col-sx-12">
                            <table class="table table-hover dataTable table-striped w-full ofertas" id="ofertas">
                                <thead>
                                    <tr>
                                        <th>Producto</th>
                                        <th>Precio (S/. x Kg)</th>
                                        <th>Cantidad total ofertada</th>
                                        <th>Fecha de cosecha</th>
                                        <th>Acción</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</div>
<div id="modal" class="fade modal" role="dialog"  tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
var csrf = "<?=Yii::$app->request->getCsrfToken() ?>";
var lista_cultivos =new Array();
Cultivos();
async function Cultivos(){

    urls =  [
        "http://fspreset.minagri.gob.pe:3000/cat_cultivos2",
    ];
    let results =await Promise.all(urls.map((url) => fetch(url,{method:'POST'}).then((r) => r.json())));
    var cultivo_options = "<option value>Seleccionar producto</option>";
    jQuery.each(results, function(i, result) {
        $.map( result, function( val, i ) {
            //console.log(val);
            let cod = parseInt(val.COD);
            lista_cultivos[cod]= val.NOMCUL;
            cultivo_options = cultivo_options + "<option value='" + val.COD + "'>" + val.NOMCUL + "</option>";
        });
    });

}

async function Cultivos2(){

    urls =  [
        "http://fspreset.minagri.gob.pe:3000/cat_cultivos2",
    ];
    let results =await Promise.all(urls.map((url) => fetch(url,{method:'POST'}).then((r) => r.json())));
    var cultivo_options = "<option value>Seleccionar producto</option>";
    jQuery.each(results, function(i, result) {
        $.map( result, function( val, i ) {
            cultivo_options = cultivo_options + "<option value='" + val.COD + "'>" + val.NOMCUL + "</option>";
        });
    });
    $('.oferta-cod_cultivo').html(cultivo_options);
    $('.oferta-cod_cultivo').selectpicker();

}

async function Oferta(oferta_id){
    await $.ajax({
        url:'<?= \Yii::$app->request->BaseUrl ?>/oferta/get-oferta',
        method: 'POST',
        data:{_csrf:csrf,oferta_id:oferta_id},
        dataType:'Json',
        beforeSend:function()
        {
            
        },
        success:function(results)
        {   
            if(results && results.success){
                console.log(results);
                $('#oferta-imp_precio').val(results.oferta.imp_precio);
                $('#oferta-can_toneladas').val(results.oferta.can_toneladas);
                $('#oferta-fec_cosecha').val(results.oferta.fec_cosecha);

                $('.oferta-cod_cultivo').val('0' + results.oferta.cod_cultivo);
                $('.oferta-cod_cultivo').selectpicker('refresh');
            }
        },
        error:function(){
            alert('Error al realizar el proceso.');
        }
    });
}


ListaOfertasCambios();
async function ListaOfertasCambios(){
   

   await $.ajax({
       url:'<?= \Yii::$app->request->BaseUrl ?>/oferta/get-lista-ofertas-cambios',
       method: 'POST',
       data:{_csrf:csrf},
       dataType:'Json',
       beforeSend:function()
       {
           
       },
       success:function(results)
       {   
           if(results && results.success){
               
               var ofertas ="";
               $('.ofertas').DataTable().destroy();
               $.each(results.ofertas, function( index, value ) {
                    var cultivo = "";
                    if(lista_cultivos[value.cod_cultivo]){
                        cultivo = lista_cultivos[value.cod_cultivo]
                    }
                    

                    var fecha_hoy = moment(new Date());
                    var fecha_cosecha = moment(value.fec_cosecha);
                    var dias = fecha_cosecha.diff(fecha_hoy, 'days');
                    var alerta = "";
                    
                    var estado = "";
                    if(value.flg_estado==0){
                        estado = "Pendiente";
                    }

                  
                   var tonelada = ((value.can_toneladas)?value.can_toneladas:0);
                   var precio = ((value.imp_precio)?value.imp_precio:0);
                   ofertas = ofertas + "<tr>";
                       ofertas = ofertas + "<td>" + ((cultivo)?cultivo:'') + "</td>";
                       ofertas = ofertas + "<td>" + ((value.imp_precio)?number_format(value.imp_precio,2):'') + "</td>";
                       ofertas = ofertas + "<td>" + ((value.can_toneladas)?number_format(value.can_toneladas,2):'') + "</td>";
                       ofertas = ofertas + "<td>" + ((value.fecha)?value.fecha:'') + "</td>";
                       ofertas = ofertas + "<td>" + ((value.accion)?value.accion:'') + "</td>";
                       ofertas = ofertas + "<td>" + estado + "</td>";
                       ofertas = ofertas + "<td>";
                           ofertas = ofertas + '<button type="button" data-id="' + value.ide_oferta_cambio + '" class="btn btn-success btn-sm btn-confirmar-oferta-pendiente"><i class="fas fa-check"></i></button> ';
                           ofertas = ofertas + '<button type="button" data-id="' + value.ide_oferta + '" class="btn btn-info btn-sm btn-ver-oferta-publicada"><i class="fas fa-eye"></i></button> ';
                           ofertas = ofertas + '<button type="button" data-id="' + value.ide_oferta_cambio + '" class="btn btn-danger btn-sm btn-eliminar-oferta-pendiente"><i class="fas fa-trash-alt"></i></button>';
                           
                       ofertas = ofertas + "</td>";
                   ofertas = ofertas + "</tr>";
               });

               $('.ofertas tbody').html(ofertas);
              
               $('.ofertas').DataTable({
                   "responsive": true,
                   "paging": true,
                   "lengthChange": true,
                   "searching": true,
                   "ordering": true,
                   "info": true,
                   "autoWidth": false,
                   "pageLength" : 3,
                   "language": {
                       "sProcessing":    "Procesando...",
                       "sLengthMenu":    "Mostrar _MENU_ registros",
                       "sZeroRecords":   "No se encontraron resultados",
                       "sEmptyTable":    "Ningun dato disponible en esta lista",
                       "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                       "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                       "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                       "sInfoPostFix":   "",
                       "sSearch":        "Buscar:",
                       "sUrl":           "",
                       "sInfoThousands":  ",",
                       "sLoadingRecords": "Cargando...",
                       "oPaginate": {
                           "sFirst":    "Primero",
                           "sLast":    "Último",
                           "sNext":    "Siguiente",
                           "sPrevious": "Anterior"
                       },
                       "oAria": {
                           "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                           "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                       }
                   },
               });
              
           }
       },
       error:function(){
           alert('Error al realizar el proceso.');
       }
   });
}

//ver

$('body').on('click', '.btn-ver-oferta-publicada', function (e) {
    e.preventDefault();
    var oferta_id = $(this).attr('data-id');
    $('#modal .modal-content').load('<?= \Yii::$app->request->BaseUrl ?>/oferta/view?oferta_id=' + oferta_id,function(){
        Cultivos2();
        Oferta(oferta_id);
    });

    $('#modal').modal('show');
});


$('body').on('click', '.btn-confirmar-oferta-pendiente', function (e) {
    e.preventDefault();
    var ide_oferta_cambio = $(this).attr('data-id');
    var r = confirm("¿Esta seguro de confirmar la modificación del productor ?");
    if (r == true) {
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/oferta/oferta-confirmar-pendiente',
            method: 'POST',
            data:{_csrf:csrf,ide_oferta_cambio:ide_oferta_cambio},
            dataType:'Json',
            beforeSend:function()
            {
                
            },
            success:function(results)
            {   
                if(results && results.success){
                    ListaOfertasCambios();
                }
            },
            error:function(){
                alert('Error al realizar el proceso.');
            }
        });
    }
});

$('body').on('click', '.btn-eliminar-oferta-pendiente', function (e) {
    e.preventDefault();
    var ide_oferta_cambio = $(this).attr('data-id');
    var r = confirm("¿Esta seguro de eliminar la modificación indicada por el productor ?");
    if (r == true) {
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/oferta/oferta-eliminar-pendiente',
            method: 'POST',
            data:{_csrf:csrf,ide_oferta_cambio:ide_oferta_cambio},
            dataType:'Json',
            beforeSend:function()
            {
                
            },
            success:function(results)
            {   
                if(results && results.success){
                    ListaOfertasCambios();
                }
            },
            error:function(){
                alert('Error al realizar el proceso.');
            }
        });
    }
});


function number_format(amount, decimals) {
    amount += '';
    amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
    decimals = decimals || 0;
    if (isNaN(amount) || amount === 0)
            return parseFloat(0).toFixed(decimals);
    amount = '' + amount.toFixed(decimals);
    var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
    while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
    return amount_parts.join('.');
}
</script>