<?php

namespace app\models;

use Yii;
use nanson\postgis\behaviors\GeometryBehavior;
/**
 * This is the model class for table "oferta".
 *
 * @property int $id
 * @property string|null $dni
 * @property string|null $nombres_apellidos
 * @property string|null $region_id
 * @property string|null $provincia_id
 * @property string|null $distrito_id
 * @property int|null $cultivo_id
 * @property float|null $precio
 * @property float|null $cantidad
 * @property int|null $estado
 * @property string|null $geom
 * @property string|null $fecha_cosecha
 */
class Oferta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public static function tableName()
    {
        return 'oferta';
    }

    public function behaviors()
    {
        return [
            [
                'class' => GeometryBehavior::className(),
                'type' => GeometryBehavior::GEOMETRY_POINT,
                'attribute' => 'geom',
                // explicitly set custom db connection if you do not want to use
                // static::getDb() or Yii::$app->getDb() connections
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cultivo_id', 'estado'], 'default', 'value' => null],
            [['cultivo_id', 'estado'], 'integer'],
            [['precio', 'cantidad'], 'number'],
            [['geom'], 'safe'],
            [['fecha_cosecha'], 'safe'],
            [['dni'], 'string', 'max' => 8],
            [['nombres_apellidos'], 'string', 'max' => 250],
            [['region_id'], 'string', 'max' => 2],
            [['provincia_id'], 'string', 'max' => 4],
            [['distrito_id'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dni' => 'Dni',
            'nombres_apellidos' => 'Nombres Apellidos',
            'region_id' => 'Region ID',
            'provincia_id' => 'Provincia ID',
            'distrito_id' => 'Distrito ID',
            'cultivo_id' => 'Cultivo ID',
            'precio' => 'Precio',
            'cantidad' => 'Cantidad',
            'estado' => 'Estado',
            'geom' => 'Geom',
            'fecha_cosecha' => 'Fecha Cosecha',
        ];
    }
}
