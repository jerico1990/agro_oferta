<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;
use app\models\Oferta;
use app\models\Agricultor;
use yii\helpers\Json;
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        $session->remove('dni');
        $model = new Usuario();
        if(Yii::$app->request->post()){
            $json = $model->validateDNI($_POST['dni']);
            if(!$json['ok']){
                return $this->render('index_validacion',['json'=>$json,'message'=>$json['bandera'],'dni'=>$_POST['dni']]);
            }
            //var_dump($json);die;
            $agricultor = Agricultor::find()->where(['txt_dni'=>$_POST['dni']]);
            if($agricultor->count() == 0){
                $agricultor = new Agricultor;
                $agricultor->txt_nombres    = $json["nombres"];
                $agricultor->txt_paterno    = $json["apellidos"];
                //$agricultor->ap_paterno     = $json["apPrimer"];
                //$agricultor->ap_materno     = $json["apSegundo"];
                $agricultor->txt_dni  = $_POST['dni'];
                $agricultor->save();
            }

            if(isset($_POST['actualizando']) && $_POST['actualizando']=="1"){
                $agricultor                     = $agricultor->one();
                $agricultor->txt_telefono       = (isset($_POST['telefono'])?$_POST['telefono']:$agricultor->telefono);
                $agricultor->txt_celular        = (isset($_POST['celular'])?$_POST['celular']:$agricultor->celular); 
                $agricultor->txt_email          = (isset($_POST['correo_electronico'])?$_POST['correo_electronico']:$agricultor->email);
                $agricultor->txt_direccion      = (isset($_POST['direccion'])?$_POST['direccion']:$agricultor->txt_direccion);
                $agricultor->txt_centro_poblado = (isset($_POST['centro_poblado'])?$_POST['centro_poblado']:$agricultor->txt_centro_poblado); 

                $agricultor->update();
                $session->remove('dni');
            }

            $session->set('dni', $_POST['dni']);
            return $this->render('index_validacion',['json'=>$json,'message'=>'2','dni'=>$_POST['dni']]);

        }
        return $this->render('index',['model'=>$model]);
    }

    public function actionInformar(){
        $model = new Usuario();
        if(Yii::$app->request->post()){
            $json = $model->validateDNI($_POST['dni']);
            if($json && $json['ok']){
                return $this->render('informar',['json'=>$json,'dni'=>$_POST['dni']]);
            }
        }

        return $this->redirect(['index']);
        
    }

    public function actionCreateOferta(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($_POST){
            $region_id          = $_POST['region_id'];
            $provincia_id       = $_POST['provincia_id'];
            $distrito_id        = $_POST['distrito_id'];
            $cultivo_id         = $_POST['cultivo_id'];
            $precio             = $_POST['precio'];
            $cantidad           = $_POST['cantidad'];
            $fecha_cosecha      = $_POST['fecha_cosecha'];
            $dni                = $_POST['dni'];
            $nombres_apellidos  = $_POST['nombres_apellidos'];
            $latitud            = $_POST['latitud'];
            $longitud           = $_POST['longitud'];
            //var_dump($latitud,$longitud,[$longitud,$latitud]);die;

            $agricultor = (new \yii\db\Query())
                ->select('*')
                ->from('ofe_tcm_agricultor')
                ->where('txt_dni=:txt_dni',[':txt_dni'=>$dni]);
            if($agricultor->count()>0){
                $agricultor = $agricultor->one();
                $model = new Oferta();
                $model->cod_region              = $region_id;
                $model->cod_prov                = substr($provincia_id,3,2);
                $model->cod_dist                = substr($distrito_id,5,2);
                $model->cod_cultivo             = $cultivo_id;
                $model->imp_precio              = $precio;
                $model->can_toneladas           = $cantidad;
                $model->fec_cosecha             = $fecha_cosecha;
                $model->cod_agri                = $agricultor['ide_agricultor'];
                $model->txt_nombres_apellidos   = $nombres_apellidos;
                $model->txt_dni                 = $dni;
                $model->flg_estado              = 1;
                $model->fec_registro            = date('Y-m-d H:i:s');
                $model->cod_geom                = [$longitud,$latitud];
                //$model->save();
                if($model->save()){
                    return ['success'=>true,'message'=>'1'];
                }
                return ['success'=>false,'message'=>'2']; 
            }
            return ['success'=>false,'message'=>'2']; 

            
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
