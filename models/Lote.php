<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lote".
 *
 * @property int $id
 * @property string|null $descripcion
 * @property int|null $estado
 * @property string|null $region_id
 * @property string|null $provincia_id
 * @property string|null $distrito_id
 * @property int|null $agricultor_id
 * @property string|null $geom
 */
class Lote extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lote';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['estado', 'agricultor_id'], 'default', 'value' => null],
            [['estado', 'agricultor_id'], 'integer'],
            [['geom'], 'string'],
            [['descripcion'], 'string', 'max' => 150],
            [['region_id'], 'string', 'max' => 2],
            [['provincia_id'], 'string', 'max' => 4],
            [['distrito_id'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
            'estado' => 'Estado',
            'region_id' => 'Region ID',
            'provincia_id' => 'Provincia ID',
            'distrito_id' => 'Distrito ID',
            'agricultor_id' => 'Agricultor ID',
            'geom' => 'Geom',
        ];
    }
}
