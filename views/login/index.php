<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="page-content">
    <div class="page-brand-info">
        <div class="brand">
            <img class="brand-img" src="<?= \Yii::$app->request->BaseUrl ?>/img/LogoMinagri.png" width="50%" alt="...">
            <h2 class="brand-text font-size-40">MINAGRI</h2>
        </div>
        <p class="font-size-20">SISAGRO - Sistema de Agro Ofertas.</p>
        </div>

        <div class="page-login-main animation-slide-right animation-duration-1">
        <div class="brand hidden-md-up">
            <img class="brand-img" src="<?= \Yii::$app->request->BaseUrl ?>/img/LogoMinagri.png" alt="...">
            <h3 class="brand-text font-size-40">MINAGRI</h3>
        </div>
        <h3 class="font-size-24">Iniciar sesión</h3>
        <p></p>
        <?php $form = ActiveForm::begin(['options' => ['id' => 'formLogin','class' => 'container psi-login__form-container']]); ?>
            <div class="form-group">
                <label class="sr-only" for="inputEmail">Usuario</label>
                <input type="text" class="form-control" id="username" name="LoginForm[username]" placeholder="Usuario">
            </div>
            <div class="form-group">
                <label class="sr-only" for="inputPassword">Clave</label>
                <input type="password" class="form-control" id="password" name="LoginForm[password]"placeholder="Password">
            </div>
            <button type="submit" class="btn btn-primary btn-block">Ingresar</button>
        <?php ActiveForm::end(); ?>


        <footer class="page-copyright">
            <p>MINAGRI</p>
            <p>© 2020.</p>
           
        </footer>
    </div>

</div>

<script>
$('body').on('click', '.btn-iniciar-sesion', function (e) {
    e.preventDefault();
    var form = $('#formLogin');
    var formData = $('#formLogin').serializeArray();

    if (form.find('.has-error').length) {
        return false;
    }
    
    $.ajax({
        url:form.attr("action"),
        type: form.attr("method"),
        data: formData,
        dataType: 'json',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            //$('.sidebar-mini').LoadingOverlay("hide", true);
        },
        success: function (results) {
            if(results.success){
                location.reload();
            }else{
                alert("El usuario o contraseña es incorrecto");
            }
        },
    });
});
</script>