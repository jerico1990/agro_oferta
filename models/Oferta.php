<?php

namespace app\models;

use Yii;
use nanson\postgis\behaviors\GeometryBehavior;
/**
 * This is the model class for table "ofe_tcm_oferta".
 *
 * @property int $ide_oferta
 * @property float|null $can_toneladas
 * @property float|null $imp_precio
 * @property string|null $fec_cosecha
 * @property string|null $fec_registro
 * @property int|null $flg_estado
 * @property int|null $cod_region
 * @property int|null $cod_prov
 * @property int|null $cod_dist
 * @property int|null $cod_agri
 * @property int|null $cod_cultivo
 * @property string|null $cod_geom
 */
class Oferta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $titulo;
    public static function tableName()
    {
        return 'ofe_tcm_oferta';
    }

    public function behaviors()
    {
        return [
            [
                'class' => GeometryBehavior::className(),
                'type' => GeometryBehavior::GEOMETRY_POINT,
                'attribute' => 'cod_geom',
                // explicitly set custom db connection if you do not want to use
                // static::getDb() or Yii::$app->getDb() connections
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['can_toneladas', 'imp_precio'], 'number'],
            [['fec_cosecha', 'fec_registro', 'cod_cultivo'], 'safe'],
            [['flg_estado', 'cod_region', 'cod_prov', 'cod_dist', 'cod_agri'], 'default', 'value' => null],
            [['flg_estado', 'cod_region', 'cod_prov', 'cod_dist', 'cod_agri'], 'integer'],
            [['geom'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ide_oferta' => 'Ide Oferta',
            'can_toneladas' => 'Can Toneladas',
            'imp_precio' => 'Imp Precio',
            'fec_cosecha' => 'Fec Cosecha',
            'fec_registro' => 'Fec Registro',
            'flg_estado' => 'Flg Estado',
            'cod_region' => 'Cod Region',
            'cod_prov' => 'Cod Prov',
            'cod_dist' => 'Cod Dist',
            'cod_agri' => 'Cod Agri',
            'cod_cultivo' => 'Cod Cultivo',
            'cod_geom' => 'Cod Geom',
        ];
    }
}
