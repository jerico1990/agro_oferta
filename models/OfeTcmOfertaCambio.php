<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ofe_tcm_oferta_cambio".
 *
 * @property int $ide_oferta_cambio
 * @property int|null $ide_oferta
 * @property string|null $accion
 * @property float|null $can_toneladas
 * @property float|null $imp_precio
 * @property string|null $fec_cosecha
 * @property int|null $cod_cultivo
 * @property int|null $cod_agri
 * @property string|null $fec_registro
 * @property int|null $flg_estado
 */
class OfeTcmOfertaCambio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ofe_tcm_oferta_cambio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ide_oferta', 'cod_cultivo', 'cod_agri', 'flg_estado'], 'default', 'value' => null],
            [['ide_oferta', 'cod_cultivo', 'cod_agri', 'flg_estado'], 'integer'],
            [['accion'], 'string'],
            [['can_toneladas', 'imp_precio'], 'number'],
            [['fec_cosecha', 'fec_registro'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ide_oferta_cambio' => 'Ide Oferta Cambio',
            'ide_oferta' => 'Ide Oferta',
            'accion' => 'Accion',
            'can_toneladas' => 'Can Toneladas',
            'imp_precio' => 'Imp Precio',
            'fec_cosecha' => 'Fec Cosecha',
            'cod_cultivo' => 'Cod Cultivo',
            'cod_agri' => 'Cod Agri',
            'fec_registro' => 'Fec Registro',
            'flg_estado' => 'Flg Estado',
        ];
    }
}
